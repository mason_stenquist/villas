let {theme} = require('tailwindcss/defaultConfig.js');
const customSpacing = {
  ...theme.spacing,
  '14': '3.4rem',
  '72': '18rem',
  '80': '20rem',
  '96': '24rem',
  '1200': '1200px',
  '1400': '1400px',
}
module.exports = {
    future: {
      purgeLayersByDefault: true,
    },
    purge: {
      content: [
        './resources/**/*.antlers.html',
        './resources/**/*.blade.php',
        './content/**/*.md'
      ]
    },
    important: true,
    theme: {
      minWidth: customSpacing,
      maxWidth: customSpacing,
      minHeight: customSpacing,
      maxHeight: customSpacing,
      spacing: customSpacing,
      fontFamily:{
        'open': ['Open Sans Condensed', 'sans-serif']
      },
      screens: {
        'sm': '800px',
        'md': '1000px',
        'lg': '1280px',
        'xl': '1400px',
        'xxl': '1600px'
      },
      extend: {
        colors:{
          'white-50': "rgb(255 255 255 / 0.5)",
          'black-50': "rgb(0 0 0 / 0.5)",
          'primary': "#0f1d22",
          "primary-light": "#22363e",
          "primary-dark": "#0f1719",
          'gold': '#c9ab81',
          'gold-dark': "#906c39"
        },        
      },
    },
    variants: {},
    plugins: [],
  }
