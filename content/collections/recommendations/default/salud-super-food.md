---
title: 'Salud Super Food'
main_photo: recommendations/salud/o.jpg
short_description: |-
  Healthy, vegetarian and vegan-friendly, smoothies, wraps, salads, sandwiches, breakfast all day, lunch offered after 11:30a.

  Vegetarian-Friendly, Vegan Options, and Gluten-Free Options

  Breakfast, Lunch, Brunch.  Takeout, Seating, Free Wifi, Delivery.

  Certificate of Excellence2015 - 2018 Winner
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Healthy, vegetarian and vegan-friendly, smoothies, wraps, salads, sandwiches, breakfast all day, lunch offered after 11:30a.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vegetarian-Friendly, Vegan Options, and Gluten-Free Options'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Breakfast, Lunch, Brunch.  Takeout, Seating, Free Wifi, Delivery.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Certificate of Excellence2015 - 2018 Winner'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="7ecIgcb6fJ7OyfLTpnXKyg" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=MrFWeweC_TllZ9GtiS5PmA" rel="nofollow noopener">David S.</a>''s <a href="https://www.yelp.com/biz/salud-super-food-puerto-vallarta-2?hrid=7ecIgcb6fJ7OyfLTpnXKyg" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/TsGh5a7VzbzjGdeUheBYdQ" rel="nofollow noopener">Salud Super Food</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/salud/o-(1).jpg
  - recommendations/salud/o-(2).jpg
  - recommendations/salud/o-(3).jpg
  - recommendations/salud/o-(4).jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1613940630
recommendation_types:
  - 660ed207-651d-48ee-9643-1b734433d682
  - c3155e30-bd0b-4b0d-a01c-6626545281ec
  - 1c278d9f-2782-41ce-baa9-9a288eb473c0
id: 00f822ec-24af-4752-856c-3b5662b99ff0
---
