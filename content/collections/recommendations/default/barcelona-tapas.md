---
title: 'Barcelona Tapas'
recommendation_types:
  - 660ed207-651d-48ee-9643-1b734433d682
  - d426a854-13c5-49ae-a72f-c27d576cd2fa
  - 93e80f8c-25ab-49ee-9243-25213f713d75
  - b8a2171a-8726-442d-86b7-e9a2d1646ffe
main_photo: recommendations/barcelona-tapas/o.jpg
short_description: |
  Barcelona Tapas is frequented by locals and tourists as well. Located in the center of Puerto Vallarta, 4 blocks from The Malecon. Barcelona Tapas is a casual restaurant with open cuisine, excellent bay view, warm and friendly environment and reasonable prices.
  The food is traditional and modern Spanish tapas homemade, fresh, great quality, with local and imported products. Our menu has a great variety of fish and seafood, meat, vegetarian dishes and options for kids.
  The sangria is the most popular drink, although, you will find a good selection of wines and a complete bar. All of our kitchen staff is well trained by the chef-owner William Carballo, who was years of experience and in his background he has worked in Spain and Café Iberico and Emilio’s in Chicago, as well as having accomplished some practice in the restaurant Jaleo in Washington DC
  Barcelona Tapas is open from lunch time until dinner during most of the year and it is a great option for families and couples.
  We do not accept Visa or Mastercard. You can pay in cash, pesos, dollars or American Express.
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Barcelona Tapas is frequented by locals and tourists as well. Located in the center of Puerto Vallarta, 4 blocks from The Malecon. Barcelona Tapas is a casual restaurant with open cuisine, excellent bay view, warm and friendly environment and reasonable prices.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The food is traditional and modern Spanish tapas homemade, fresh, great quality, with local and imported products. Our menu has a great variety of fish and seafood, meat, vegetarian dishes and options for kids.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The sangria is the most popular drink, although, you will find a good selection of wines and a complete bar. All of our kitchen staff is well trained by the chef-owner William Carballo, who was years of experience and in his background he has worked in Spain and Café Iberico and Emilio’s in Chicago, as well as having accomplished some practice in the restaurant Jaleo in Washington DC'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Barcelona Tapas is open from lunch time until dinner during most of the year and it is a great option for families and couples.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We do not accept Visa or Mastercard. You can pay in cash, pesos, dollars or American Express.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="Gs1Zn7DL7u26ju_mxgu-yw" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=-35xtBSrepGU1XaNRfZtWA" rel="nofollow noopener">Ryan A.</a>''s <a href="https://www.yelp.com/biz/barcelona-tapas-bar-puerto-vallarta?hrid=Gs1Zn7DL7u26ju_mxgu-yw" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/UVcfP_B0eXmVg9pZ5mPk_w" rel="nofollow noopener">Barcelona Tapas Bar</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/barcelona-tapas/o-(1).jpg
  - recommendations/barcelona-tapas/o-(2).jpg
  - recommendations/barcelona-tapas/o-(3).jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1614010913
id: e30eca5b-77b3-4725-b89d-61df29d204d8
---
