---
title: 'Red Cabbage Café'
recommendation_types:
  - 660ed207-651d-48ee-9643-1b734433d682
  - 9a9dfd41-5015-4cf5-b7bd-6167f73dc649
main_photo: recommendations/red-cabbage-cafe/o.jpg
short_description: 'One of vallarta''s most delightful dining experiences, the menu features dishes from all regions of Mexico. Easily accessible on the south side of the Rio Cuale, the Red Cabbage Cafe is located in Colonia Remance. You know you are someplace special when you step into the lighthearted collage of its decor. On the brightly painted walls is an eclectic display of art, music, theater, literature and film. Frida Khalo and Diego Rivera are featured on one wall. Informal but stylish, bohemian but sophisticated, delicious dining in a delightful setting await Puerto Vallarta''s visitors and residents who are looking for a great food experience.Prices are moderate and value outstanding.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'One of vallarta''s most delightful dining experiences, the menu features dishes from all regions of Mexico. Easily accessible on the south side of the Rio Cuale, the Red Cabbage Cafe is located in Colonia Remance. You know you are someplace special when you step into the lighthearted collage of its decor. On the brightly painted walls is an eclectic display of art, music, theater, literature and film. Frida Khalo and Diego Rivera are featured on one wall. Informal but stylish, bohemian but sophisticated, delicious dining in a delightful setting await Puerto Vallarta''s visitors and residents who are looking for a great food experience.Prices are moderate and value outstanding.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="12bIdJ7kBl6NY6RF7lfqfA" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=9fe41Vmw245Eho1PghHHKw" rel="nofollow noopener">Amy O.</a>''s <a href="https://www.yelp.com/biz/red-cabbage-caf%C3%A9-puerto-vallarta-2?hrid=12bIdJ7kBl6NY6RF7lfqfA" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/W29lg5n4r5mG1Slg4vu8og" rel="nofollow noopener">Red Cabbage Café</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/red-cabbage-cafe/o-(1).jpg
  - recommendations/red-cabbage-cafe/o-(2).jpg
  - recommendations/red-cabbage-cafe/o-(3).jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1614013082
id: 368427d1-c9c7-440d-ae0c-3a3e018f4acd
---
