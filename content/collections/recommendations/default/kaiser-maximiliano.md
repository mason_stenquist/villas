---
title: 'Kaiser Maximiliano'
recommendation_types:
  - 660ed207-651d-48ee-9643-1b734433d682
  - a544fae8-a9af-4c43-b86c-717d75124048
main_photo: recommendations/kaiser-maximilian/o.jpg
short_description: |
  If you enjoy European sidewalk cafés and fine continental cuisine, this award-winning eatery evoking Old Vienna at its heyday is sure to charm you.
  
  A longtime Vallarta favorite, Austrian host Andreas Rupprechter named the place in honor of a fellow countryman, Mexico's former emperor. And Maximilian himself would have loved dining here, perhaps choosing hearty comfort food like Wienerschnitzel with potatoes and austrian salad. Or delving into gourmet renditions of contemporary European fare, like Rack of lamb with horseradish mustard crust, polenta, green beans and bell pepper purée or duck breast, port, sauce, foie gras, black pepper & rum, caramelized pineapple. 
  
  And while the menu is constantly being tweaked, it's nice knowing that some things don't change, and for a dozen years we've been able to count on enjoying our favorite plates here. So whether dining al fresco with unparalleled people watching, or in an intimate, air-conditioned, Old World Milieu, when a fabulous meal or just a perfect pastry and a special coffee is what you crave, this is the place to be.
  
  Kaiser Maximilian Restaurant & Café 
  
  Open for breakfast, lunch and dinner from Monday to Saturday.
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'If you enjoy European sidewalk cafés and fine continental cuisine, this award-winning eatery evoking Old Vienna at its heyday is sure to charm you.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'A longtime Vallarta favorite, Austrian host Andreas Rupprechter named the place in honor of a fellow countryman, Mexico''s former emperor. And Maximilian himself would have loved dining here, perhaps choosing hearty comfort food like Wienerschnitzel with potatoes and austrian salad. Or delving into gourmet renditions of contemporary European fare, like Rack of lamb with horseradish mustard crust, polenta, green beans and bell pepper purée or duck breast, port, sauce, foie gras, black pepper & rum, caramelized pineapple. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'And while the menu is constantly being tweaked, it''s nice knowing that some things don''t change, and for a dozen years we''ve been able to count on enjoying our favorite plates here. So whether dining al fresco with unparalleled people watching, or in an intimate, air-conditioned, Old World Milieu, when a fabulous meal or just a perfect pastry and a special coffee is what you crave, this is the place to be.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Kaiser Maximilian Restaurant & Café '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Open for breakfast, lunch and dinner from Monday to Saturday.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="24bkcP-xFKdYpUtQi6VD9g" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=1KuCKtf02vc3xfm9RyNfSQ" rel="nofollow noopener">Jim L.</a>''s <a href="https://www.yelp.com/biz/kaiser-maximilian-puerto-vallarta-2?hrid=24bkcP-xFKdYpUtQi6VD9g" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/RPwNmwU9WvEJTmiBKKVXxw" rel="nofollow noopener">Kaiser Maximilian</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/kaiser-maximilian/o-(3).jpg
  - recommendations/kaiser-maximilian/o-(2).jpg
  - recommendations/kaiser-maximilian/o-(1).jpg
  - recommendations/kaiser-maximilian/o.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1614130608
id: 74d602ac-81f7-4051-8cbb-c03bd2256f4f
---
