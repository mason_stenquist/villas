---
title: Garbo
recommendation_types:
  - b8a2171a-8726-442d-86b7-e9a2d1646ffe
  - b72581e5-5778-4c3c-a2c7-1127896feb7d
  - 150dcbcc-29c9-407b-9da7-20299e0ba272
main_photo: recommendations/garbo/183259_194577910564497_3342171_n.jpg
short_description: 'Garbo is a chic little piano and jazz bar located a half of a block off of the famous Olas Altas on Pulpito, just steps from the Abbey Hotel.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Garbo is a chic little piano and jazz bar located a half of a block off of the famous Olas Altas on Pulpito, just steps from the Abbey Hotel.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="PkSMPfO7ORc_mKHdMyW80g" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=fAyU85V1gW1GKHCk9yrXXw" rel="nofollow noopener">Steven M.</a>''s <a href="https://www.yelp.com/biz/garbo-puerto-vallarta-2?hrid=PkSMPfO7ORc_mKHdMyW80g" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/4UOXSFjHyeaDXU9ae6QVzA" rel="nofollow noopener">Garbo</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/garbo/o-(1).jpg
  - recommendations/garbo/o-(2).jpg
  - recommendations/garbo/o.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1614129376
id: b7e4afa3-00da-4f8a-8614-14d164791723
---
