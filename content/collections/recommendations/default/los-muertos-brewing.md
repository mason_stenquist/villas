---
title: 'Los Muertos Brewing'
recommendation_types:
  - 660ed207-651d-48ee-9643-1b734433d682
  - b8a2171a-8726-442d-86b7-e9a2d1646ffe
short_description: |-
  PUERTO VALLARTA’S FIRST CRAFT BREWERY & BREW PUB

  SETTING THE STANDARD FOR CASUAL DINING IN PV
  Opening the doors November 2nd, 2012, also known as “Dia de Los Muertos”, our pub pushed past the status quo for pubs in Vallarta. Bringing craft brewing and combining those flavorful beers with world class pizza and pub fare Los Muertos quickly built a loyal following of regulars. At any given time our pub hosts a boisterous mix of locals and visitors alike. Whether you’re just joining us for a happy hour, dining, or catching the game you’ll always find a cool vibe here at the pub.
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'PUERTO VALLARTA’S FIRST CRAFT BREWERY & BREW PUB'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'SETTING THE STANDARD FOR CASUAL DINING IN PV'
      -
        type: hard_break
      -
        type: text
        text: 'Opening the doors November 2nd, 2012, also known as “Dia de Los Muertos”, our pub pushed past the status quo for pubs in Vallarta. Bringing craft brewing and combining those flavorful beers with world class pizza and pub fare Los Muertos quickly built a loyal following of regulars. At any given time our pub hosts a boisterous mix of locals and visitors alike. Whether you’re just joining us for a happy hour, dining, or catching the game you’ll always find a cool vibe here at the pub.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="IUBfJ2dJSZ6jgEAVBXwtGg" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=IRBAyDXJ7xQSWseZPi--Fg" rel="nofollow noopener">Jason C.</a>''s <a href="https://www.yelp.com/biz/los-muertos-brewing-puerto-vallarta?hrid=IUBfJ2dJSZ6jgEAVBXwtGg" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/7UeClyHIfxqErhoIk0YScQ" rel="nofollow noopener">Los Muertos Brewing</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/brewpub-losmuertos-photo-04-1000x700_c-1.jpg
  - recommendations/brewpub-losmuertos-photo-02-1000x700_c.jpg
  - recommendations/brewpub-losmuertos-photo-01-1000x700_c.jpg
  - recommendations/brewpub-pizza-photo-03-1000x700_c.jpg
  - recommendations/brewpub-food-photo-04-1000x700_c.jpg
  - recommendations/brewpub-food-photo-02-1000x700_c.jpg
  - recommendations/brewpub-food-photo-01-1000x700_c.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1613864069
main_photo: recommendations/los-muertos-brewing-logo.png
id: 83940e96-4c42-429d-8645-ebcfe9b5a487
---
