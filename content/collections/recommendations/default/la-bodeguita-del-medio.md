---
title: 'La Bodeguita del Medio'
recommendation_types:
  - 660ed207-651d-48ee-9643-1b734433d682
  - 749de478-c8c4-48f8-940b-88f474bfe3f6
main_photo: recommendations/la-bodeguita-del-medio/o.jpg
short_description: 'La Bodeguita del Medio was founded in Cuba in the 1940s as a corner store owned by Angel Martinez. The store became a meeting place for local artists who enjoyed getting together for an afternoon ''Mojito''. The place has preserved the ambiance of the romantic Caribbean lifestyle that flourished with Havana. Today the Bodeguita del Medio is famous world wide for Salsa music, fine cigars, its original Mojitos and having guests write their thoughts and comments on the walls of the restaurant.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'La Bodeguita del Medio was founded in Cuba in the 1940s as a corner store owned by Angel Martinez. The store became a meeting place for local artists who enjoyed getting together for an afternoon ''Mojito''. The place has preserved the ambiance of the romantic Caribbean lifestyle that flourished with Havana. Today the Bodeguita del Medio is famous world wide for Salsa music, fine cigars, its original Mojitos and having guests write their thoughts and comments on the walls of the restaurant.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="Adh_08eZlQVNNVaICTfkTg" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=lJ4ioFY5q6UQ9JtXDRiVFQ" rel="nofollow noopener">Jasmine H.</a>''s <a href="https://www.yelp.com/biz/la-bodeguita-del-medio-puerto-vallarta?hrid=Adh_08eZlQVNNVaICTfkTg" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/cysMtFdUSTAfDQ1QDsmtMw" rel="nofollow noopener">La Bodeguita del Medio</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/la-bodeguita-del-medio/o-(1).jpg
  - recommendations/la-bodeguita-del-medio/o-(2).jpg
  - recommendations/la-bodeguita-del-medio/o-(3).jpg
  - recommendations/la-bodeguita-del-medio/o-(4).jpg
  - recommendations/la-bodeguita-del-medio/o.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1614129808
id: cee902c0-313d-4ffe-a8ad-adf52628e2ce
---
