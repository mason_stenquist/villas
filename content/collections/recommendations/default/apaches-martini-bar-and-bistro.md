---
title: 'Apaches Martini Bar & Bistro'
recommendation_types:
  - b8a2171a-8726-442d-86b7-e9a2d1646ffe
  - ea4b04d4-d0da-42b8-91af-731f15c1a4bf
  - e8b7a8f6-c7c5-4a32-9e57-b8b1cc1e7780
main_photo: recommendations/apaches-martini-bar/Capture.png
short_description: 'Fun sidewalk bar on Olas Altas'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Fun sidewalk bar on Olas Altas'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: Reviews
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'http://www.tripadvisor.com.mx/Restaurant_Review-g150793-d1579915-Reviews-Apache_s_Martini_Bar_and_Bistro-Puerto_Vallarta.html'
              target: _blank
              rel: null
        text: 'Trip Advisor'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="q33Ce-z0AXeiAHm8PNOOTg" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=DDgBSb4Dtb15AAt4ydI5Ig" rel="nofollow noopener">Hillari S.</a>''s <a href="https://www.yelp.com/biz/apaches-martini-bar-puerto-vallarta?hrid=q33Ce-z0AXeiAHm8PNOOTg" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/JxbQC5-7FKCVhDuvjGaMUA" rel="nofollow noopener">Apaches Martini Bar</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/apaches-martini-bar/Capture.png
  - recommendations/apaches-martini-bar/Capture3.png
  - recommendations/apaches-martini-bar/o.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1614131508
id: f25827ca-d856-40cc-ae71-6f818dfecd8c
---
