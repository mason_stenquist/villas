---
title: 'Archie’s Wok'
main_photo: recommendations/archieswok.gif
short_description: 'Since 1986, Archie’s Wok has been legendary in the Banderas Bay for serving vibrantly original cuisine influenced by the exotic flavors of Thailand, the Philippines and the Pacific Rim. Archie’s helped establish the culinary foundation of Puerto Vallarta and continues to be one of the bay’s most beloved, longtime established restaurants.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Since 1986, Archie’s Wok has been legendary in the Banderas Bay for serving vibrantly original cuisine influenced by the exotic flavors of Thailand, the Philippines and the Pacific Rim. Archie’s helped establish the culinary foundation of Puerto Vallarta and continues to be one of the bay’s most beloved, longtime established restaurants.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="_cPjrCsoP8wzpEuAZ4-olA" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=B3xvWa3K-4U42KK5WIN9TQ" rel="nofollow noopener">Jo S.</a>''s <a href="https://www.yelp.com/biz/archies-wok-puerto-vallarta?hrid=_cPjrCsoP8wzpEuAZ4-olA" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/Y3rObwnQX5Zhy4XCRZiBJg" rel="nofollow noopener">Archie''s Wok</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1613862908
recommendation_types:
  - 047fedc8-db7f-4ea7-9738-44afd52bb969
  - 660ed207-651d-48ee-9643-1b734433d682
photos:
  - recommendations/archies-wok1.jpg
  - recommendations/archies-wok2.jpg
  - recommendations/archies-wok3.jpg
  - recommendations/archies-wok4.jpg
  - recommendations/archies-wok5.jpg
  - recommendations/archies-wok6.jpg
  - recommendations/archies-wok7.jpg
id: 06892b97-a9a2-49b8-aa60-8810abb21aed
---
