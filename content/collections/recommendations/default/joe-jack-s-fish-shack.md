---
title: 'Joe Jack’s Fish Shack'
main_photo: recommendations/joe-jacks-fish-shack/o.jpg
short_description: |
  Now entering our 8th year, Jack’s Fish Shack is a lively, hip seafood restaurant located in beautiful “Old Town” Puerto Vallarta. Chef-owner Joe Jack prepares a menu of dishes that combines the freshest in local seafood and produce with moderately priced comfort food favorites.
  
  The bar serves up an assortment of fun and innovative cocktails including our fruit flavored mojitos and of course, the kick @#$ mai-tai.
  
  Joe Jack’s has two levels: a warm welcoming Mexican style cantina bar downstairs, and a great rooftop dining area upstairs that is great for accommodating large parties. Joe Jack’s Fish Shack is also available to host or cater your Specials Events.
  
  So whether you’re in the mood for some fresh to order seafood, a tasty burger, or just a bucket of shrimp and a cold beer at the bar, Joe Jack’s is the place to satisfy all your cravings and then some.
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Now entering our 8th year, Jack’s Fish Shack is a lively, hip seafood restaurant located in beautiful “Old Town” Puerto Vallarta. Chef-owner Joe Jack prepares a menu of dishes that combines the freshest in local seafood and produce with moderately priced comfort food favorites.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The bar serves up an assortment of fun and innovative cocktails including our fruit flavored mojitos and of course, the kick @#$ mai-tai.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Joe Jack’s has two levels: a warm welcoming Mexican style cantina bar downstairs, and a great rooftop dining area upstairs that is great for accommodating large parties. Joe Jack’s Fish Shack is also available to host or cater your Specials Events.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'So whether you’re in the mood for some fresh to order seafood, a tasty burger, or just a bucket of shrimp and a cold beer at the bar, Joe Jack’s is the place to satisfy all your cravings and then some.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="dmCbgK-w1zO-6AtIouInNg" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=Hn6xgnc2ftKKjcEJQaSbEA" rel="nofollow noopener">Bob T.</a>''s <a href="https://www.yelp.com/biz/joe-jacks-fish-shack-puerto-vallarta?hrid=dmCbgK-w1zO-6AtIouInNg" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/ocen293yFXnutGnWSarR7A" rel="nofollow noopener">Joe Jack''s Fish Shack</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/joe-jacks-fish-shack/o-(3).jpg
  - recommendations/joe-jacks-fish-shack/o-(1).jpg
  - recommendations/joe-jacks-fish-shack/o-(2).jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1614011604
recommendation_types:
  - 660ed207-651d-48ee-9643-1b734433d682
  - d426a854-13c5-49ae-a72f-c27d576cd2fa
id: c4d46e3f-d458-4c66-98d6-c934790583f2
---
