---
title: 'Tony’s Please'
recommendation_types:
  - 660ed207-651d-48ee-9643-1b734433d682
short_description: |-
  Traditional Mexican Cuisine
  Certificate of Excellence2015 – 2018 Winner
bard:
  -
    type: paragraph
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Traditional Mexican Cuisine'
      -
        type: hard_break
      -
        type: text
        text: 'Certificate of Excellence2015 – 2018 Winner'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="DFiutLE0r2A9jUmcsUIBDQ" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=8XwCuXxLDsfOwZvwb-FtMw" rel="nofollow noopener">Billie E.</a>''s <a href="https://www.yelp.com/biz/tonys-please-puerto-vallarta?hrid=DFiutLE0r2A9jUmcsUIBDQ" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/BUImPEjC8wc8TSLFUtHYSQ" rel="nofollow noopener">Tony''s Please</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/tony-please1.jpg
  - recommendations/tony-please2.jpg
  - recommendations/tony-please3.jpg
  - recommendations/tony-please4.jpg
  - recommendations/tony-please5.jpg
  - recommendations/tony-please6.jpg
  - recommendations/tony-please7.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1613864102
main_photo: recommendations/tonys-please.png
id: b64c4d57-d259-446d-a1e5-62d971a84bca
---
