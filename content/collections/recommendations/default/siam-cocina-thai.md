---
title: 'Siam Cocina Thai'
type: restaurant
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Siam Cocina Thai, a unique Thai restaurant in Puerto Vallarta serving authentic Southeast Asian cuisine featuring traditional ingredients and flavors. Siam always brings the freshest local products to the table, with authentic Thai flavors and contemporary presentation in Puerto Vallarta – a new favorite in the heart of the Romantic Zone.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="_xh4NUDO_XF9PE7gmHadIQ" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=IR_geI7GdR8hzXSkKkX-hQ" rel="nofollow noopener">Amy J.</a>''s <a href="https://www.yelp.com/biz/siam-cocina-thai-puerto-vallarta?hrid=_xh4NUDO_XF9PE7gmHadIQ" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/6jHm1_geTPmw-ZH3Deb5eg" rel="nofollow noopener">Siam Cocina Thai</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1613864087
photo_gallery:
  - siam4.jpeg
  - siam1.jpeg
  - siamcocinathai.jpg
  - siam5.jpg
  - siamcocinathai-sm.jpg
recommendation_types:
  - 660ed207-651d-48ee-9643-1b734433d682
  - 2f8c7f0a-763c-437e-b94f-3275b76eabe6
photos:
  - recommendations/siam5-(2).jpg
  - recommendations/siam4.jpeg
  - recommendations/sim3.jpeg
  - recommendations/sima2.jpeg
  - recommendations/siam1-(1).jpeg
  - recommendations/siamcocinathai.jpg
short_description: 'Siam Cocina Thai, a unique Thai restaurant in Puerto Vallarta serving authentic Southeast Asian cuisine featuring traditional ingredients and flavors. Siam always brings the freshest local products to the table, with authentic Thai flavors and contemporary presentation in Puerto Vallarta – a new favorite in the heart of the Romantic Zone.'
main_photo: recommendations/siam5-(2).jpg
id: 88eb6389-b89a-42d2-b62d-8e366d3d2a44
---
