---
title: 'Bistro Teresa'
recommendation_types:
  - 660ed207-651d-48ee-9643-1b734433d682
  - d426a854-13c5-49ae-a72f-c27d576cd2fa
  - a8b78e1c-8b9f-4a7f-b5d3-913d40769f20
  - b8a2171a-8726-442d-86b7-e9a2d1646ffe
main_photo: recommendations/bistro-teresa/o.jpg
short_description: 'Charming Bistro that won Trip Advisor''s Travelers'' Choice award in 2014.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Charming Bistro that won Trip Advisor''s Travelers'' Choice award in 2014.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="APJriVrYukjMUgOaNNIkIw" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=3SfiqyTQO8gP8z6XA1ekpQ" rel="nofollow noopener">Garseng W.</a>''s <a href="https://www.yelp.com/biz/bistro-teresa-puerto-vallarta-3?hrid=APJriVrYukjMUgOaNNIkIw" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/oToGW8GsHAatO-mv0peAeQ" rel="nofollow noopener">Bistro Teresa</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/bistro-teresa/bistro-teresa4.jpg
  - recommendations/bistro-teresa/o-(1).jpg
  - recommendations/bistro-teresa/o-(2).jpg
  - recommendations/bistro-teresa/o.jpg
  - recommendations/bistro-teresa/thumbnail4.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1614010487
id: f1735db2-5cf6-4f55-b6ad-b8a63e5703f5
---
