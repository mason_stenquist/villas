---
title: 'River Café'
recommendation_types:
  - 660ed207-651d-48ee-9643-1b734433d682
  - 9a9dfd41-5015-4cf5-b7bd-6167f73dc649
  - 921e47e7-be0a-4d7e-b9a5-096b46a36a01
main_photo: recommendations/river-cafe/o.jpg
short_description: 'River Cafe, is located at the Rio Cuale Island, just minutes away from the Boardwalk and Puerto Vallarta’s downtown. Our sophisticated cuisine delights the most demanding palates; furthermore, you can enjoy the best Mexican and international wines and a plenty of cocktails.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'River Cafe, is located at the Rio Cuale Island, just minutes away from the Boardwalk and Puerto Vallarta’s downtown. Our sophisticated cuisine delights the most demanding palates; furthermore, you can enjoy the best Mexican and international wines and a plenty of cocktails.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'River Cafe is an ideal restaurant for dinner in Puerto Vallarta, but guests may also enjoy a delicious Breakfast or outstanding Lunch in a natural and friendly atmosphere. If you are looking for a restaurant for groups or special events in Vallarta, please do not hesitate to contact us.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Our warm and natural ambience creates the perfect flair for a romantic Dinner in Puerto Vallarta. We invite you to learn more about '
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'http://www.rivercafe.com.mx/en/about-us/'
              target: _blank
              rel: null
        text: 'River Cafe'
      -
        type: text
        text: .
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="b9eDMJKCp_ZHh3XJG1yCMw" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=oSxGsL1Z4Qmm09T9o6usOA" rel="nofollow noopener">Stephanie L.</a>''s <a href="https://www.yelp.com/biz/river-caf%C3%A9-puerto-vallarta?hrid=b9eDMJKCp_ZHh3XJG1yCMw" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/ssdBW_QsHSrN_DB-82YeqQ" rel="nofollow noopener">River Café</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/river-cafe/o-(1).jpg
  - recommendations/river-cafe/o-(2).jpg
  - recommendations/river-cafe/o-(3).jpg
  - recommendations/river-cafe/o-(4).jpg
  - recommendations/river-cafe/o.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1614011263
id: b0ff3297-4705-4833-945d-620924935d7d
---
