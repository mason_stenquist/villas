---
title: 'Café Des Artists'
recommendation_types:
  - 660ed207-651d-48ee-9643-1b734433d682
  - eb1e3ff0-28ca-45a0-bff9-8899b06f99fc
  - 9a9dfd41-5015-4cf5-b7bd-6167f73dc649
main_photo: recommendations/cafe-des-artistes/Thumbnail.png
short_description: |
  Located in an old house in the center of Puerto Vallarta, Café des Artistes, has over 23 years of experience, offering luxury, elegance and comfort in unique natural environments that are adorned by the exquisite menu, carefully created under the direction of acclaimed Chef Thierry Blouet
  
  RECOGNIZED AS THE BEST RESTAURANT IN MEXICO 2011-2012
  “At Café des Artistes, every dish is a masterpiece; and every masterpiece feeds the spirit…” Mexican writer Carlos Fuentes.
  
  After more than 23 years of history, Café des Artistes, proudly continues to give the opportunity of living the most traditional gourmet experience in Puerto Vallarta to Mexico and the world; At Café des Artistes every element is product of Chef Thierry Blouet’s love and delicacy for culinary pleasures. Thierry’s successful career is recognized for his magical touch on gourmet creations.
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Located in an old house in the center of Puerto Vallarta, Café des Artistes, has over 23 years of experience, offering luxury, elegance and comfort in unique natural environments that are adorned by the exquisite menu, carefully created under the direction of acclaimed Chef Thierry Blouet'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'RECOGNIZED AS THE BEST RESTAURANT IN MEXICO 2011-2012'
  -
    type: paragraph
    content:
      -
        type: text
        text: '“At Café des Artistes, every dish is a masterpiece; and every masterpiece feeds the spirit…” Mexican writer Carlos Fuentes.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'After more than 23 years of history, Café des Artistes, proudly continues to give the opportunity of living the most traditional gourmet experience in Puerto Vallarta to Mexico and the world; At Café des Artistes every element is product of Chef Thierry Blouet’s love and delicacy for culinary pleasures. Thierry’s successful career is recognized for his magical touch on gourmet creations.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="LBMIoVab8gRKz2gzcfkgTg" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=FnAND8HrMfjKcBk0tqz5AA" rel="nofollow noopener">Alex A.</a>''s <a href="https://www.yelp.com/biz/caf%C3%A9-des-artistes-puerto-vallarta-2?hrid=LBMIoVab8gRKz2gzcfkgTg" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/A2f4WVFmoGpJeJoHg4Ga7Q" rel="nofollow noopener">Café des Artistes</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/cafe-des-artistes/o-(1).jpg
  - recommendations/cafe-des-artistes/o-(2).jpg
  - recommendations/cafe-des-artistes/o-(3).jpg
  - recommendations/cafe-des-artistes/o-(4).jpg
  - recommendations/cafe-des-artistes/o.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1614131129
id: 5cd13623-1ab1-4caf-b7d9-62db1d7c76cd
---
