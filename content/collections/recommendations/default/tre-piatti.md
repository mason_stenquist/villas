---
title: 'Tre Piatti'
recommendation_types:
  - 660ed207-651d-48ee-9643-1b734433d682
  - 69c0da9a-cfd2-4f43-802c-f79fe69dbf33
main_photo: recommendations/tre-piatti/4.jpg
short_description: |-
  Tre Piatti is a regional Italian restaurant in the heart of the Romantic Zone of Puerto Vallarta. Inside the restaurant and around the tables there are 2 mango trees with more than 120 years of age. The dining room is full of beautiful works of art, a romantic bar with the selection of wines by Chef Chanan and the center can be seen working the delights of Italian cuisine offered by this romantic and friendly restaurant.

  Tre Piatti offers a menu that changes constantly, it consists of 3 times - that's where the name Tre Piatti is born - and its pasta is made at home with the best ingredients selected by the Chef and prepared by the restaurant staff.
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Tre Piatti is a regional Italian restaurant in the heart of the Romantic Zone of Puerto Vallarta. Inside the restaurant and around the tables there are 2 mango trees with more than 120 years of age. The dining room is full of beautiful works of art, a romantic bar with the selection of wines by Chef Chanan and the center can be seen working the delights of Italian cuisine offered by this romantic and friendly restaurant.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Tre Piatti offers a menu that changes constantly, it consists of 3 times - that''s where the name Tre Piatti is born - and its pasta is made at home with the best ingredients selected by the Chef and prepared by the restaurant staff.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="Eq1HGKVKQYwICHf6OMKWqA" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=WGUbZQQbPPJ_ZmSiLMt6EQ" rel="nofollow noopener">Hope S.</a>''s <a href="https://www.yelp.com/biz/tre-piatti-puerto-vallarta?hrid=Eq1HGKVKQYwICHf6OMKWqA" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/9JHX6oWp0QH-lD3KowQAWw" rel="nofollow noopener">Tre Piatti</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
photos:
  - recommendations/tre-piatti/1.jpg
  - recommendations/tre-piatti/10.jpg
  - recommendations/tre-piatti/2.jpg
  - recommendations/tre-piatti/3.jpg
  - recommendations/tre-piatti/4.jpg
  - recommendations/tre-piatti/5.jpg
  - recommendations/tre-piatti/6.jpg
  - recommendations/tre-piatti/7.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1613940293
id: cf167df0-4906-4262-95be-6db0ae190e91
---
