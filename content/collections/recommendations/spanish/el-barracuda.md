---
id: 43daaf5b-c685-4796-bee7-acb3e88a300a
origin: 47ef37b4-8919-4380-a193-19e082a46010
short_description: |-
  Mariscos a la “Mercadito”. En Playa Camarones (centro de Puerto Vallarta) desde 2007.

  Somos un local especializado en mariscos estilo mercadito.

  También ofrecemos varias opciones para los no amantes de los mariscos, increíbles atardeceres, avistamiento de ballenas durante la temporada, ambiente inmejorable y calidez y servicio personalizado.
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Mariscos a la “Mercadito”. En Playa Camarones (centro de Puerto Vallarta) desde 2007.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Somos un local especializado en mariscos estilo mercadito.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'También ofrecemos varias opciones para los no amantes de los mariscos, increíbles atardeceres, avistamiento de ballenas durante la temporada, ambiente inmejorable y calidez y servicio personalizado.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="UbTfDEBjn6FA11B2DVvilw" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=eZGoPMRfq2hw5LfwJg0uTA" rel="nofollow noopener">Stephen W.</a>''s <a href="https://www.yelp.com/biz/el-barracuda-puerto-vallarta?hrid=UbTfDEBjn6FA11B2DVvilw" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/qCcVFhr6fPufjhxkpfVNhw" rel="nofollow noopener">El Barracuda</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
seo:
  title: 'El Barracuda (ES)'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642878440
---
