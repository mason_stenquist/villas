---
id: ef38b055-3760-40b0-889a-b76439e73939
origin: 06892b97-a9a2-49b8-aa60-8810abb21aed
short_description: 'Desde 1986, Archie''s Wok ha sido legendario en la Bahía de Banderas por servir una cocina vibrantemente original influenciada por los sabores exóticos de Tailandia, Filipinas y la Cuenca del Pacífico. Archie''s ayudó a establecer la base culinaria de Puerto Vallarta y sigue siendo uno de los restaurantes más queridos y establecidos de la bahía.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Desde 1986, Archie''s Wok ha sido legendario en la Bahía de Banderas por servir una cocina vibrantemente original influenciada por los sabores exóticos de Tailandia, Filipinas y la Cuenca del Pacífico. Archie''s ayudó a establecer la base culinaria de Puerto Vallarta y sigue siendo uno de los restaurantes más queridos y establecidos de la bahía.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="_cPjrCsoP8wzpEuAZ4-olA" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=B3xvWa3K-4U42KK5WIN9TQ" rel="nofollow noopener">Jo S.</a>''s <a href="https://www.yelp.com/biz/archies-wok-puerto-vallarta?hrid=_cPjrCsoP8wzpEuAZ4-olA" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/Y3rObwnQX5Zhy4XCRZiBJg" rel="nofollow noopener">Archie''s Wok</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
seo:
  title: 'Archie’s Wok (ES)'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642878421
---
