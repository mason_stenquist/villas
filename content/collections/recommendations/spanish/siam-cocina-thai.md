---
id: c015d9f3-e717-4fe4-ae3c-982f17f8a8ea
origin: 88eb6389-b89a-42d2-b62d-8e366d3d2a44
short_description: 'Siam Cocina Thai, un restaurante tailandés único en Puerto Vallarta que sirve auténtica cocina del sudeste asiático con ingredientes y sabores tradicionales. Siam siempre trae los productos locales más frescos a la mesa, con auténticos sabores tailandeses y una presentación contemporánea en Puerto Vallarta, un nuevo favorito en el corazón de la Zona Romántica.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Siam Cocina Thai, un restaurante tailandés único en Puerto Vallarta que sirve auténtica cocina del sudeste asiático con ingredientes y sabores tradicionales. Siam siempre trae los productos locales más frescos a la mesa, con auténticos sabores tailandeses y una presentación contemporánea en Puerto Vallarta, un nuevo favorito en el corazón de la Zona Romántica.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="_xh4NUDO_XF9PE7gmHadIQ" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=IR_geI7GdR8hzXSkKkX-hQ" rel="nofollow noopener">Amy J.</a>''s <a href="https://www.yelp.com/biz/siam-cocina-thai-puerto-vallarta?hrid=_xh4NUDO_XF9PE7gmHadIQ" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/6jHm1_geTPmw-ZH3Deb5eg" rel="nofollow noopener">Siam Cocina Thai</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
seo:
  title: 'Siam Cocina Thai (ES)'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642878581
---
