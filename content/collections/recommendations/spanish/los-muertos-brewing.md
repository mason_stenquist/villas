---
id: 833c661a-840a-4c14-8f5f-e805d99300fb
origin: 83940e96-4c42-429d-8645-ebcfe9b5a487
short_description: |-
  PRIMERA CERVECERÍA CRAFT & BREW PUB DE PUERTO VALLARTA

  ESTABLECIENDO EL ESTÁNDAR PARA LAS COMIDAS CASUALES EN PV
  Abriendo las puertas el 2 de noviembre de 2012, también conocido como “Día de los Muertos”, nuestro pub superó el status quo de los pubs en Vallarta. Traer elaboración artesanal y combinar esas sabrosas cervezas con pizzas de clase mundial y comida de pub Los Muertos rápidamente creó un público fiel de clientes habituales. En cualquier momento, nuestro pub alberga una mezcla bulliciosa de lugareños y visitantes por igual. Ya sea que se una a nosotros para disfrutar de una hora feliz, cenar o ver el partido, siempre encontrará un ambiente genial aquí en el pub.
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'PRIMERA CERVECERÍA CRAFT & BREW PUB DE PUERTO VALLARTA'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'ESTABLECIENDO EL ESTÁNDAR PARA LAS COMIDAS CASUALES EN PV'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Abriendo las puertas el 2 de noviembre de 2012, también conocido como “Día de los Muertos”, nuestro pub superó el status quo de los pubs en Vallarta. Traer elaboración artesanal y combinar esas sabrosas cervezas con pizzas de clase mundial y comida de pub Los Muertos rápidamente creó un público fiel de clientes habituales. En cualquier momento, nuestro pub alberga una mezcla bulliciosa de lugareños y visitantes por igual. Ya sea que se una a nosotros para disfrutar de una hora feliz, cenar o ver el partido, siempre encontrará un ambiente genial aquí en el pub.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="IUBfJ2dJSZ6jgEAVBXwtGg" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=IRBAyDXJ7xQSWseZPi--Fg" rel="nofollow noopener">Jason C.</a>''s <a href="https://www.yelp.com/biz/los-muertos-brewing-puerto-vallarta?hrid=IUBfJ2dJSZ6jgEAVBXwtGg" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/7UeClyHIfxqErhoIk0YScQ" rel="nofollow noopener">Los Muertos Brewing</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
seo:
  title: 'Los Muertos Brewing (ES)'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642878600
---
