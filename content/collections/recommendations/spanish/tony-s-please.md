---
id: d2109938-add7-45ac-9e2a-eaffb9908a8e
origin: b64c4d57-d259-446d-a1e5-62d971a84bca
short_description: |-
  Cocina tradicional mexicana
  Certificado de excelenciaGanador de 2015-2018
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Cocina tradicional mexicana'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Certificado de excelenciaGanador de 2015-2018'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<span class="yelp-review" data-review-id="DFiutLE0r2A9jUmcsUIBDQ" data-hostname="www.yelp.com">Read <a href="https://www.yelp.com/user_details?userid=8XwCuXxLDsfOwZvwb-FtMw" rel="nofollow noopener">Billie E.</a>''s <a href="https://www.yelp.com/biz/tonys-please-puerto-vallarta?hrid=DFiutLE0r2A9jUmcsUIBDQ" rel="nofollow noopener">review</a> of <a href="https://www.yelp.com/biz/BUImPEjC8wc8TSLFUtHYSQ" rel="nofollow noopener">Tony''s Please</a> on <a href="https://www.yelp.com" rel="nofollow noopener">Yelp</a><script src="https://www.yelp.com/embed/widgets.js" type="text/javascript" async></script></span>'
seo:
  title: 'Tony’s Please (ES)'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642877023
---
