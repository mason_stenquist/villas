---
id: 9dbf5447-3fe9-4ea5-bd81-d1b6ed3fdc9d
title: 'What do you mean that it is "Open to the Ocean"?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Most of the living areas in QMC & CTV are "Open to the Ocean", meaning that there are no windows or doors. It is just open. Bedrooms do have doors and windows that close and allow for air conditioning to be turned on when needed.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The temperature in Puerto Vallarta rarely falls below 65 degrees and most houses in PV are open to the ocean.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1717007183
---
