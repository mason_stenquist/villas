---
id: 2961f610-383e-4bc8-8598-eedb2de9e9eb
title: 'Can you swim in front of QMC/CTV?'
answer:
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'It depends on what the waves are doing at the time you would like to swim. We have sandy beach directly in front of QMC/CTV. There are also rocks that are in the water. If the bay is calm, you can swim in front of the property, and even snorkel around the rocks. If the bay is rough, the waves crash on the rocks and it is dangerous to swim directly in front. '
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'You can walk on the beach less than 5 minutes to the north our south of us and find more sandy beaches with less rocks in the water.  '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Beach directly in front of Quinta Maria Cortez and Casa Tres Vidas'
  -
    type: paragraph
    content:
      -
        type: image
        attrs:
          src: 'asset::assets::photos/Beach-in-front-of-QMC_CTV-1.jpg'
          alt: ''
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Beach in front of Quinta Maria Cortez and Casa Tres Vidas looking north'
  -
    type: paragraph
    content:
      -
        type: image
        attrs:
          src: 'asset::assets::photos/Beach-in-front-looking-north-1.jpg'
          alt: null
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Beach in front of Quinta Maria Cortez and Casa Tres Vidas looking south'
  -
    type: paragraph
    content:
      -
        type: image
        attrs:
          src: 'asset::assets::photos/Beach-in-front-looking-south.jpg'
          alt: null
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Beach north of Quinta Maria Cortez and Casa Tres Vidas as you head to town'
  -
    type: paragraph
    content:
      -
        type: image
        attrs:
          src: 'asset::assets::photos/Beach-to-North-on-way-to-town-1.jpg'
          alt: null
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Beach south of Quinta Maria Cortez and Casa Tres Vidas that is within walking distance'
  -
    type: paragraph
    content:
      -
        type: image
        attrs:
          src: 'asset::assets::photos/Beach-to-South-1.jpg'
          alt: null
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1717691211
---
