---
id: e2d597d3-65cf-4865-b63a-8cb9f89a367a
title: 'Is breakfast included at Quinta Maria Cortez?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Yes, breakfast is included at Quinta Maria Cortez for all direct reservations. Breakfast is not included for reservations made through a third party.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We serve Breakfast in the Palapa dinning area off the main Sala (living room) one level below street level (between Ana Gabriela & Rafael)'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Breakfast is served from 9:00am to 10:00am.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1735235333
---
