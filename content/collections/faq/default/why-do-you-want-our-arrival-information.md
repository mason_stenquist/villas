---
id: fbc41acb-37cc-4ccb-8aa1-a9e0fcea9d10
title: 'Why do you want our arrival information?'
answer:
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'So we can await your arrival!'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We are a small, boutique property with only 7 suites and 3 villas. Our guests tend to stay 5 to 14 nights, so we don’t have a front desk. Our staff cover many jobs, so we ask for your arrival information because we expect your arrival at the property about 1 hour after the flight has landed. Staff can then plan to greet you and carry your luggage up the stairs to Maxi, down the stairs to Vida Mar, or to any of the other beautiful suites and villas.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1717007426
---
