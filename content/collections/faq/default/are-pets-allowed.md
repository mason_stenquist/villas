---
id: ef7d90f7-3021-4dd6-8d8e-5c0d3d8c0589
title: 'Are pets allowed?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Please contact us at reservations@villasinvallarta.com for more information.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1735238536
---
