---
id: 44fb5580-f3df-4915-bfac-f3249ea19555
title: 'Can you walk to town?'
answer:
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Not in heels!'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We recommend that you walk along the beach to get to town. It is about a 20 minute walk to Playa Los Muertos (the main beach in town) depending on how fast you walk. Right before you get to Playa Los Muertos you have to go up a path over the rocks about 75 to 100 feet about above the sea. If you are not in good shape we do not recommend walking and encourage you to take a taxi.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We do not recommend that you walk to town on the Beach at night. It is very dark and it is easy to trip on the rocks, etc.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We do not recommend walking on the main highway (Mexico 200). It is a very busy street without a shoulder or sidewalk to walk along.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1717006234
---
