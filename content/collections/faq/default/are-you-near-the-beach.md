---
id: 2b97aa20-7856-4a96-bd82-9b1266db6336
title: 'Are you near the beach?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Yes, we are on the beach!'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'You enter QMC & CTV from the street at the back of the properties about 5 stories above the beach. You can make your way down the stairs and go out on the beach from the big gate at QMC or the beach door at the bottom of the steps at CTV.'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1715466483
---
