---
id: b0beac92-917f-4bba-8339-4eda14f988ff
title: 'Do you have an elevator?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We do not, but we have lots of stairs!'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'QMC/CTV are 8 levels, but the equivalent of a 10 story building. There is NO ELEVATOR. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'QMC is a unique old building, built on the beach front cliff by an eccentric lady that liked black and painted most of the floors and stairs black. There are lots of steps and some with narrow doors and hall ways. There is basically 1 suite per level. You enter the property from the street and 2/3 of the building cascades down to the beach.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The best suites to minimize steps are Ana Gabriela and Jessica that are on street level one level above the Sala (living room) area.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The villas at CTV are much the same, just occupying more levels. Vida Alta is our penthouse villa on the top 3.5 floors. Vida Sol is our middle villa with 2.5 levels. Vida Mar is our beach villa at the bottom of the property with 2 levels.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1717006980
---
