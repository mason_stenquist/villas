---
id: 6b3ea1c8-2fb1-42a6-868d-167615eff0de
title: 'Do you have an airport shuttle?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We can offer to set up airport transfers for you. Please contact '
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'mailto:reservations@villasinvallarta.com'
              target: _blank
              rel: null
        text: reservations@villasinvallarta.com
      -
        type: text
        text: ' and our staff will be happy to arrange that for you.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Transfers are $60 for up to 3 people.'
  -
    type: set
    attrs:
      enabled: false
      values:
        type: asset
        label: 'Airport Transportation'
  -
    type: paragraph
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1735235881
---
