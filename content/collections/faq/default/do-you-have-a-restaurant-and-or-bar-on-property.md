---
id: a6d6ce17-1e75-4004-b9cd-f6223dbc0685
title: 'Do you have a restaurant and/or bar on property?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We are in process of adding Silver''s Beach Club and Dandelion Lounge and Restaruant. We hope to open them by the end of 2025. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'All suites at QMC have a refrigerator and/or a kitchenette. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Additional refreshments can be purchased at a store in town, or at the convenience store “OXXO” on the main highway just above QMC/CTV and within walking distance.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'There is a nice restaurant “La Playita” in the Lindo Mar Resort, just to the South. To get there go out the big gate on the beach and turn left. Go over the rocks and under the pool of the building next door. You will cross a nice crescent beach that the sand goes all the way into the water. At the far side of the beach you will find a wall with stairs leading up. Take those stairs and continue down the cobble stone dirt road that dead ends at the restaurant. They serve Breakfast, Lunch and Dinner seven days a week.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1735235139
---
