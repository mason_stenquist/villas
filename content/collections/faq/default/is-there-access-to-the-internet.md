---
id: 648e1f16-9624-4d1c-8607-379ce84212cb
title: 'Is there access to the internet?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Yes, we have good internet Wi-Fi access and the internet is free of charge.   At check in you will be given a card with the password for the Wi-Fi that is good thru out the property.  '
  -
    type: paragraph
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1717007055
---
