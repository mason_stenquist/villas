---
title: '¿Estan cerca de la playa?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Si, estamos en la playa!'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Se ingresa a QMC & CTV desde la calle por la parte trasera de las propiedades, a unos 5 pisos sobre la playa. Puede bajar las escaleras y salir a la playa desde la puerta grande en QMC o la puerta de la playa al pie de los escalones en CTV.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1616519435
id: 51214353-63ff-4280-b0cb-4f1f5b540c37
origin: 2b97aa20-7856-4a96-bd82-9b1266db6336
---
