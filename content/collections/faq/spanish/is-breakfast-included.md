---
title: '¿El desayuno está incluido?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Sí, el desayuno está incluido (que es una de las Bes en B&B)'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Servimos el desayuno en el área de comedor - la Palapa de la Sala principal, un nivel debajo del nivel de la calle (entre Ana Gabriela y Rafael)'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'El desayuno se sirve de 9:00 am a 10:00 am de lunes a sábado. No se sirve desayuno los domingos.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1616520292
id: cd52115b-d94f-4a60-98e6-fd9f8055af03
origin: e2d597d3-65cf-4865-b63a-8cb9f89a367a
---
