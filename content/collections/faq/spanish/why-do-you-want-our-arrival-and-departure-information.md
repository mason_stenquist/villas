---
title: '¿Para qué desea nuestra información de llegada y salida?'
answer:
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '¡Para poder estar al pendiente de su llegada!'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Con solo 7 suites y huéspedes que suelen quedarse de 5 a 14 noches, no tenemos recepción. Esperamos su llegada aproximadamente 1 hora después de que el vuelo haya aterrizado y tendremos a alguien disponible para llevar su equipaje por las escaleras a Maxi o por las escaleras a Guadalupe o en alguna de nuestras otras hermosas suites.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1617137341
id: 41ce02af-b887-4c00-9b96-9643e4a8feab
origin: fbc41acb-37cc-4ccb-8aa1-a9e0fcea9d10
---
