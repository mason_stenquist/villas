---
title: 'Cuantas escaleras hay?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: ¡Muchas!
  -
    type: paragraph
    content:
      -
        type: text
        text: 'QMC es un edificio de 8 niveles y el equivalente a un edificio de 10 pisos. NO HAY ELEVADOR. Básicamente hay 1 suite por nivel y se ingresa a la propiedad desde la calle en la parte trasera de la propiedad. 2/3 del edificio cae en cascada hasta la playa.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'QMC es un edificio antiguo muy singular construido frente a la playa en el lado del acantilado construido por una mujer excéntrica a la que le gustaba el negro y pintaba la mayoría de los pisos / escaleras de negro. Hay muchos escalones y algunos con puertas / pasillos estrechos.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Las mejores habitaciones para minimizar los escalones son Ana Gabriela y Jessica que están en el nivel de la calle un nivel por encima del área de Sala / Desayuno.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1616519604
id: 88966db3-7310-4bd1-aadd-7cb23bd938d0
origin: b0beac92-917f-4bba-8339-4eda14f988ff
---
