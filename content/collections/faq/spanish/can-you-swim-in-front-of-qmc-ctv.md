---
title: '¿Se puede nadar frente a QMC/CTV?'
answer:
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Sí y No: directamente en frente de QMC / CTV hay una franja de playa de arena con rocas que sobresalen del agua más allá de la playa. Si la bahía está tranquila, se puede nadar frente a la propiedad e incluso bucear alrededor de las rocas. Si el mar esta agitado, como las olas rompen contra las rocas es peligroso nadar directamente enfrente de la propiedad.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'A menos de 92 metros al sur se puede nadar cuando el mar está agitado. En ese lugar, la playa de arena continúa hasta el agua.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'También se puede nadar hacia el lado norte de la propiedad cuando el mar esta agitado.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1617126631
id: 406f9780-9488-481c-97d3-86730868e09f
origin: 2961f610-383e-4bc8-8598-eedb2de9e9eb
---
