---
title: '¿Qué quieres decir con que es "Abierto al mar"?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'La mayor parte de las áreas comunes  de QMC y CTV, están abiertas al océano, lo que significa que no hay ventanas ni puertas. Simplemente está abierto.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'La temperatura en Puerto Vallarta rara vez cae por debajo de los 65 grados y la mayoría de las casas en PV están abiertas al océano.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1616519193
id: 6ffdefbe-5aa1-4c1a-80cc-6c48333f9b12
origin: 9dbf5447-3fe9-4ea5-bd81-d1b6ed3fdc9d
---
