---
title: '¿Tiene un restaurante y / o bar en la propiedad?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'No'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Todas las suites de QMC tienen refrigerador y / o  cocineta. Los refrigeradores están surtidos con 2 Coca-Colas, Coca-Cola Lights, Sprite y cerveza para darles la bienvenida.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Se pueden comprar refrigerios adicionales en una tienda de la ciudad o en la tienda "OXXO" en la carretera principal cerca de QMC / CTV.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Hay un bonito restaurante llamado "La Playita" justo al sur de la propiedad. Para llegar, salga por la puerta grande de la playa y gire a la izquierda sobre las rocas y debajo de la piscina del edificio de al lado. Cruzará una bonita playa en forma de media luna en la que la arena llega hasta el agua. Al otro lado de la playa encontrará una pared con escaleras que conducen hacia arriba. Tome esas escaleras y continúe por el camino adoquinado hasta donde termina la calle justo frente al restaurante. Sirven desayuno, comida y cena los siete días de la semana y no aceptan reservaciones.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1616519394
id: be25f62e-d17a-4f83-9d87-0ac6ff40a975
origin: a6d6ce17-1e75-4004-b9cd-f6223dbc0685
---
