---
title: '¿Se puede caminar al centro?'
answer:
  -
    type: paragraph
    content:
      -
        type: text
        text: '¡No con tacones!'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Sí, puede caminar al pueblo por la playa o por la Calle Santa Bárbara o por la Carretera Principal.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Le recomendamos que camine por la playa para llegar al pueblo. Es una caminata de 10 a 20 minutos hasta Playa Los Muertos dependiendo de qué tan rápido camine. Justo antes de llegar a Playa Los Muertos hay que subir por un sendero sobre las rocas a unos 75 a 100 pies sobre el nivel del mar. Si no está en buena condicion fisica, no recomendamos caminar.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'No recomendamos que camine hasta la ciudad por la playa por la noche. Está muy oscuro y es fácil tropezar con las rocas, etc.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'No recomendamos caminar por la carretera principal (México 200) es una calle muy transitada y no hay acera y la cuneta es estrecha.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1617136799
id: afd2725e-406b-450e-82a2-19dcf8966329
origin: 44fb5580-f3df-4915-bfac-f3249ea19555
---
