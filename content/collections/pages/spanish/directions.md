---
id: 703d7875-b107-4136-8b86-1a68f7904e5a
origin: ac646619-4dde-4a7f-b9c7-e292a24da8c7
title: 'Como llegar...'
bard:
  -
    type: paragraph
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Después de pasar el centro de Puerto Vallarta, tome la carretera de la costa Sur (México 200) hacia Mismaloya por aproximadamente 1.5 millas hasta Conchas Chinas.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Esté atento al Mini-super “OXXO” y a la oficina de Bienes Raices Applegate del lado derecho de la carretera.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'En el extremo izquierdo de este edificio hay una calle llamada Calle Sagitario. Siga hacia abajo, en direccion a la playa/agua y siga la calle cuando de vuelta a la derecha (justo al llegar a Casa Septiembre). Quinta María Cortez y Casa Tres Vidas estarán ubicadas del lado izquierdo, a la mitad de la calle, unas cinco casas después de Casa Septiembre; nos identificara por el toldo a rayas blancas y negras de Quinta María Cortez.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'El número de Quinta María es #132 y el número de Casa Tres Vidas es #126. Tenga en cuenta que la Calle Sagitario es una calle en U, con dos entradas/salidas a la carretera principal (México 200) y no todos los números de las casas están en orden.'
  -
    type: set
    attrs:
      values:
        type: buttons
        alignment: justify-center
        buttons:
          -
            link: 'https://villasinvallarta.com/assets/directions/QMC-CTV-Map-Dir-and-Phone-2012.pdf'
            button_text: 'Click here to download directions PDF'
            button_classes: null
            open_in_new_tab: true
seo:
  description: 'Como llegar...'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1689791635
---
