---
id: 55ceaa07-1b11-4371-9058-7272c0c4a116
origin: 62136fa2-9e5c-4c38-a894-a2753f02f5ff
title: 'Sobre nosotros'
slider: {  }
features: {  }
bard:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quinta Maria Cortez & Casa Tres Vidas'
      -
        type: text
        text: " es un hotel boutique bellamente decorado ubicado directamente en la playa en el extremo sur de Puerto Vallarta. Ofrecemos alojamiento de lujo que van desde suites de un dormitorio hasta villas de tres y cuatro dormitorios que se pueden configurar para cumplir con los requisitos específicos de nuestros huéspedes. Nuestra propiedad única se compone de dos edificios conectados:\_"
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'https://villasinvallarta.com/es/villas/quinta-maria-cortez'
              target: _blank
              rel: null
          -
            type: bold
        text: 'Quinta Maria Cortez “QMC”'
      -
        type: text
        text: "\_y\_"
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'https://villasinvallarta.com/es/villas/casa-tres-vidas'
              target: _blank
              rel: null
          -
            type: bold
        text: 'Casa Tres Vidas “CTV”'
      -
        type: text
        text: ". QMC ofrece siete amplias suites al aire libre de 1 y 2 recámaras (con cocinetas) en ocho niveles diferentes y son perfectas para escapadas románticas, lunas de miel, etc. Servimos un delicioso desayuno seis días a la semana (excepto los domingos) el cual está incluido en el costo de la habitacion y es preparado al momento en un comedor frente al mar y situado bajo una palapa.\_"
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'https://villasinvallarta.com/es/villas/casa-tres-vidas'
              target: _blank
              rel: null
          -
            type: bold
        text: 'Casa Tres Vidas “CTV”'
      -
        type: text
        text: "\_ofrece tres villas frente al mar (de 3 y 4 habitaciones cada una) que se pueden combinar para crear villas de 6, 7 e incluso 10 habitaciones que son ideales para grupos de amigos, compañeros de trabajo o familias. Cada villa tiene su propia sala de estar, comedor, alberca privada y jacuzzi climatizado, ubicado en dos o tres niveles y cuenta con personal completo, con un chef gourmet, un mesero/cantinero y una camarista. El servicio de chef privado está incluido en la tarifa y preparará dos comidas por día para su grupo, se cobran solo los costos reales de la comida. Para grupos y eventos grandes, QMC y CTV se pueden combinar para hospedar cómodamente hasta 80 invitados para ocasiones especiales como bodas, aniversarios y cumpleaños importantes. Nuestras habitaciones se pueden configurar para alojar hasta 44 huéspedes a la vez."
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Hemos equipado nuestras villas / suites con todas las comodidades que harán que su estadía con nosotros sea lo más placentera posible: teléfonos con marcacion directa que le permiten mantenerse en contacto con el mundo exterior, ventiladores de techo en todas las habitaciones, aire acondicionado regulable en la mayoría de las suites, toallas grandes y esponjosas para un día divertido en la playa, jabones y champús importados. Las cómodas batas de algodón y los secadores de pelo eléctricos le ayudarán a prepararse para la cena con estilo; y las cajas fuertes en la habitación protegen su pasaporte y sus prendas de valor. (Por cierto, la electricidad en México es de 120 voltios, lo que significa que las computadoras portátiles y las afeitadoras eléctricas de Estados Unidos y Canada funcionan bien). WI-FI e Internet gratuitos están disponibles en toda la Villa.'
properties: {  }
seo:
  description: 'Quinta Maria Cortez & Casa Tres Vidas es un hotel boutique bellamente decorado ubicado directamente en la playa en el extremo sur de Puerto Vallarta. Ofrecemos alojamiento de lujo que van desde suites de un dormitorio hasta villas de tres y cuatro dormitorios que se pueden configurar para cumplir con los requisitos específicos de nuestros huéspedes.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1688058275
---
