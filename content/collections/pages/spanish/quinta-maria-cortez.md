---
id: cabc1f49-94bd-4d3a-9450-ac8f6c50557f
origin: 2b442bb6-711f-4a55-9a6e-6d4362003d26
bard:
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe class="rounded-xl" width="100%" height="450" src="https://www.youtube.com/embed/KyNrpv8l1jo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quinta Maria Cortez'
      -
        type: text
        text: ", al igual que nuestra propiedad contigua,\_"
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'http://villasinvallarta.com/villas/ctv/'
              target: null
              rel: null
          -
            type: bold
        text: 'Casa Tres Vidas'
      -
        type: text
        text: ' está ubicada en una espectacular playa a solo minutos al sur del destino internacional Puerto Vallarta, México. Este bed & breakfast es el destino favorito de los viajeros de todo el mundo que buscan una escapada relajante en un entorno único. El Bed and Breakfast Quinta Maria Cortez está cerca de muchos lugares destacados, vistas espectaculares y numerosas actividades. Ya sea que desee experimentarlo todo o no hacer nada más que leer junto a la piscina o en la playa, Quinta Maria Cortez puede satisfacer todos sus caprichos. Con nosotros, disfrutará de un sabor local único fusionado con comodidades modernas. Cada suite sofisticada está amueblada con hermosos muebles antiguos y tiene todas las comodidades para garantizar que se sienta cómodo, relajado y completamente satisfecho con su estadía. Los visitantes expresan agradecimiento y placer por su estadía una y otra vez, y muchos son huéspedes repetitivos de cada año.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Algunas de las características y comodidades del Bed and Breakfast Quinta Maria Cortez incluyen:'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Siete suites únicas frente al mar, todas finamente decoradas con muebles mexicanos clásicos y antiguos.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'A poca distancia de Puerto Vallarta, es uno de los destinos vacacionales internacionales considerado como "Top 10".'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Fácil acceso a recorridos cercanos por la jungla, galerías, restaurantes, tiendas, pesca, sitios históricos y kilómetros de playas de arena dorada.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Ideal para parejas y recién casados ​​(una propiedad para adultos, no apta para niños menores de 18 años).'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Delicioso desayuno preparado a diario que se sirve 6 días a la semana (con excepcion de los domingos).'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Tómese unos minutos y explore nuestra propiedad única y descubra Quinta Maria Cortez Bed & Breakfast, y escapese de lo rutinario.'
seo:
  title: 'Quinta Maria Cortez (ES)'
  description: "Quinta Maria Cortez, al igual que nuestra propiedad contigua,\_Casa Tres Vidas está ubicada en una espectacular playa a solo minutos al sur del destino internacional Puerto Vallarta, México."
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642876835
---
