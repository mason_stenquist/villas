---
id: 3c04865d-afd6-44c6-9204-8646eafe6366
origin: 3e96212a-fdc7-48e5-8dce-256811192ee3
title: Hospedaje
bard:
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe class="rounded-xl" width="100%" height="450" src="https://www.youtube.com/embed/HcVk4QTRuLw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Villas in Vallarta'
      -
        type: text
        text: "\_(ViV) es un hotel boutique bellamente decorado ubicado en la playa en el extremo sur de Puerto Vallarta. VIV ofrece alojamientos de lujo que van desde suites de un dormitorio hasta villas de tres y cuatro dormitorios que se pueden configurar según los requisitos específicos de nuestros huéspedes. Nuestra exclusiva propiedad se compone de dos edificios conectados: Quinta Maria Cortez \"QMC\" y Casa Tres Vidas \"CTV\". QMC Bed & Breakfast se compone de siete grandes suites al aire libre de 1 y 2 dormitorios (con cocineta) en ocho niveles diferentes. QMC es perfecto para escapadas románticas, lunas de miel o simplemente para pasar un tiempo fuera de casa. Un delicioso desayuno cocinado al momento se sirve seis días a la semana en la palapa frente al mar. Casa Tres Vidas “CTV” ofrece tres villas frente al mar (3 y 4 dormitorios cada una) que se pueden combinar para crear villas de 6, 7 e incluso 10 dormitorios para amigos, compañeros de trabajo o familias. Cada villa tiene su propia sala, comedor, alberca privada y jacuzzi con calefacción. Totalmente atendido con un chef personal, mesero y camarista incluidos en la tarifa. El chef preparará dos comidas al día, cobrando únicamente los gastos de víveres. Para grupos grandes y eventos, QMC y CTV se pueden combinar para albergar cómodamente hasta 75 invitados en ocasiones especiales como bodas, aniversarios y cumpleaños importantes. Las habitaciones de la propiedad se pueden configurar para alojar hasta 42 huéspedes a la vez."
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1689094189
---
