---
id: 440b1457-4ba2-45b9-9994-a9806c1b57b9
origin: home
title: Inicio
slider:
  -
    title: 'Bienvenidos a Puerto Vallarta'
    subtitle: 'DIVINAMENTE LO MEJOR'
    assets: home/OceanView.jpg
    type: slide
    enabled: true
    image: home/OceanView.jpg
  -
    title: 'La Belleza de ser Boutique'
    subtitle: 'La Vista desde la Suite Maria en QMC'
    assets: home/Patio.jpg
    type: slide
    enabled: true
    image: home/Patio.jpg
  -
    title: 'Con espacio para relajarse'
    subtitle: 'Alberca Privada y la Sala en Vida Mar de CTV'
    image: home/CTV_Mar_3.jpg
    type: slide
    enabled: true
featured_content_title: 'Villas In Vallarta - Quinta Maria Cortez y Casa Tres Vidas'
features:
  -
    image: Eduardo-1A.jpeg
    heading: 'COVID en la Actualidad'
    sub_heading: 'Estamos haciendo todo lo posible para asegurarnos de que se sienta seguro y cómodo durante su estadía con nosotros. La “nueva normalidad” está cambiando continuamente, sin embargo, estamos totalmente comprometidos a seguir manteniendo los más altos estándares en cuanto a distanciamiento social, desinfección, higiene, saneamiento, etc. En respuesta a las recientes regulaciones de los CDC para el reingreso a los EE. UU., Canadá y otros países, tratamos de hacerlo todo muy fácil y sin preocupaciones. Las pruebas pueden ser administradas convenientemente en la propiedad por un profesional certificado, para que pueda seguir disfrutando de sus vacaciones con tranquilidad. Al finalizar la prueba, los huespedes recibirán resultados electrónicos y también obtendrán una copia impresa.'
    type: feature
    enabled: false
  -
    image: IMG_9009.jpg
    heading: 'La Playa'
    sub_heading: 'Un enclave mexicano privado, lujoso y finamente decorado, la combinación perfecta para una escapada relajante en un entorno único donde se dormira y despertará con los relajantes sonidos del océano justo afuera de su ventana. El complejo QMC / CTV es una de las únicas propiedades directamente en la playa. A todos nuestros huespedes se les proporcionan toallas de playa, camastros, sillas, mesas y sombrillas. Desde todas las habitaciones se puede disfrutar de vistas despejadas al amanecer y al atardecer.'
    type: feature
    enabled: true
  -
    image: header-images/staff.jpg
    heading: 'El Personal'
    sub_heading: 'Nuestra propiedad es única y es operada por un personal amable, leal, dedicado, con entusiasmo y que está orgulloso de su hospitalidad profesional. Además de proporcionar una experiencia superior a nuestros huéspedes, estamos comprometidos a asegurarnos de que se satisfagan sus necesidades y que su estadía con nosotros sea memorable.'
    type: feature
    enabled: true
bard:
  -
    type: set
    attrs:
      values:
        type: text_columns
        number_of_columns: 2
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Bienvenido a Villas en Vallarta, el secreto mejor guardado de Puerto Vallarta. Nuestro atractivo resort boutique cuenta con dos propiedades bellamente distintas, cada una de las cuales ofrece la elegancia del viejo mundo en un entorno contemporáneo. Somos una propiedad boutique ubicada en la playa más exclusiva de Puerto Vallarta, sin embargo, el centro de la ciudad está a solo una corta distancia caminando. Nuestro dedicado personal está capacitado para garantizar que su estadía con nosotros supere todas las expectativas.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Nuestra propiedad única cuenta con tres niveles de hospitalidad distintiva. Cada piso contiene su propia piscina privada, jacuzzi y una espectacular arquitectura al aire libre. Podemos configurar fácilmente la distribucion de cada planta para adaptarla a sus necesidades específicas al ofrecer entre una y diez habitaciones para su disfrute. Todas nuestras habitaciones son cómodas, acogedoras y cuentan con ropa de cama, colchas y toallas de lujo. Nuestro Chef Privado puede prepararle dos comidas completas diariamente a partir de un menú que usted diseñe y solicite. (Solo paga el costo de los alimentos). Nuestro equipo profesional también incluye un conserje experto y seguridad las 24 horas.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Nuestros huéspedes siempre son tratados como familia y son recibidos con una margarita de cortesía y una botana de inspiración local a su llegada. Una estadía en Villas en Vallarta es algo para recordar. Además de nuestras comodidades internas, podemos organizar excursiones de un día y recorridos que van desde safaris en jeep hasta caminatas impresionantes. Para los amantes del deporte, paseos a caballo, buceo, parasailing y golf de clase mundial son solo algunas de las actividades que le esperan. ¡Esperamos encontrarnos contigo pronto!'
          -
            type: paragraph
  -
    type: set
    attrs:
      values:
        type: call_to_action
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: '¿Tiene preguntas? Anímate y llámanos hoy sin cargo al '
              -
                type: text
                marks:
                  -
                    type: link
                    attrs:
                      href: 'tel:8886408100'
                      target: null
                      rel: null
                text: '(888) 640-8100'
              -
                type: text
                text: ' o '
              -
                type: text
                marks:
                  -
                    type: link
                    attrs:
                      href: 'tel:8012639500'
                      target: null
                      rel: null
                text: '(801) 263-9500'
              -
                type: text
                text: ', o envíanos un correo electrónico a '
              -
                type: text
                marks:
                  -
                    type: link
                    attrs:
                      href: 'mailto:info@villasinvallarta.com'
                      target: null
                      rel: null
                text: info@villasinvallarta.com
        button_text: 'Contactanos hoy'
        button_color: '#4ED4E0'
        link: 'entry::62136fa2-9e5c-4c38-a894-a2753f02f5ff'
properties:
  -
    property_title: 'Quinta Maria Cortez'
    property_image: 2H5A2696.jpg
    property_description: 'QMC está ubicado en una espectacular playa a solo minutos al sur del destino internacional Puerto Vallarta, México. Este bed & breakfast es el destino favorito de los viajeros de todo el mundo que buscan una escapada relajante en un entorno único.'
    type: property
    enabled: true
    property_link: 'entry::2b442bb6-711f-4a55-9a6e-6d4362003d26'
  -
    property_title: 'Casa Tres Vidas'
    property_image: 2H5A2567.jpg
    property_link: 'entry::86314aca-7793-4aa4-b988-c5826ce9f353'
    property_description: 'Ven a descubrir un enclave de 3 villas frente al mar ubicadas en el corazón de la Rivera del Pacífico de México, Casa Tres Vidas. Imagínese despertar en su lujosa villa mexicana privada y finamente decorada con los relajantes sonidos del océano justo afuera de su ventana.'
    type: property
    enabled: true
seo:
  description: 'Bienvenido a Villas en Vallarta, el secreto mejor guardado de Puerto Vallarta.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1687886240
---
