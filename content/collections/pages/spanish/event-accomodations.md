---
id: e439efe2-b511-4ec0-a2cf-46def4d62bc5
origin: fd2574b6-7700-4561-8585-865524f9b8b1
title: 'Hospedaje para eventos'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: "Nuestras propiedades contiguas, Quinta Maria Cortez y Casa Tres Vidas, están ubicadas en una de las playas más hermosas de Puerto Vallarta, México. Cada propiedad es única y le ofrece una variedad de hospedaje de lujo y oportunidades de escapadas inolvidables para la boda de sus sueños. Una vez que se confirma su boda, nos comunicamos y coordinamos los depósitos, las llegadas y las salidas directamente con sus invitados, lo que le permite completar todos los demás elementos de su lista de tareas pendientes.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "Quinta Maria Cortez, es un hotel boutique de 7 habitaciones que combina magistralmente hermosos muebles antiguos y un diseño espectacular con todas las comodidades modernas para garantizar que se sienta cómodo, todo creando un ambiente único... un poco de antigüedad tropical caprichosa junto al mar.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "Justo al lado, Casa Tres Vidas es un enclave de villas frente al mar de 3 o 4 dormitorios que, cuando se combina con Quinta Maria Cortez, crea un complejo con capacidad de 42 huéspedes por noche.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Lo invitamos a explorar nuestras propiedades, las cuales son cálida gracias al sol tropical mexicano.'
seo:
  description: 'Nuestras propiedades contiguas, Quinta María Cortez y Casa Tres Vidas, están ubicadas en una de las playas más hermosas de Puerto Vallarta, México, a solo unos pasos de su refugio personal.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1689789676
---
