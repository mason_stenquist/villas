---
id: 27346963-1d50-46cd-8088-0a8c9bd8cdd2
origin: eef4b1d8-2cdd-41de-90ec-094d6da1b41c
title: Tarifas
bard:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Información sobre las tarifas Quinta Maria Cortez:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Todos los precios son en dólares americanos'
  -
    type: set
    attrs:
      values:
        type: table
        table:
          -
            cells:
              - ''
              - '**VERANO** (05/01 - 10/31)'
              - '**INVIERNO** (11/01 - 04/30)'
              - '**DÍA FESTIVO** (12/15 - 01/06)'
          -
            cells:
              - '[Maximilliano](https://villasinvallarta.com/es/villas/maximiliano)'
              - $280
              - $340
              - $405
          -
            cells:
              - '[Maria](https://villasinvallarta.com/es/villas/maria)'
              - $280
              - $335
              - $400
          -
            cells:
              - '[Jessica](https://villasinvallarta.com/es/villas/jessica)'
              - $185
              - $205
              - $240
          -
            cells:
              - '[Ana Gabriel](https://villasinvallarta.com/es/villas/ana-gabriela)'
              - $265
              - $305
              - $375
          -
            cells:
              - '[Rafael](https://villasinvallarta.com/es/villas/rafael)'
              - $265
              - $305
              - $375
          -
            cells:
              - '[Alejandro](https://villasinvallarta.com/es/villas/alejandro)'
              - $240
              - $320
              - $390
          -
            cells:
              - '[Guadalupe](https://villasinvallarta.com/es/villas/guadalupe)'
              - $275
              - $320
              - $395
          -
            cells:
              - '**Villa entera**'
              - '$1,790'
              - '$2,130'
              - '$2,580'
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: buttons
        alignment: justify-center
        buttons:
          -
            link: 'http://reservation.worldweb.com/hotel/1183/?location_id=288'
            button_text: 'BOOK NOW'
            open_in_new_tab: false
            button_classes: null
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: italic
        text: '  Verifique sus fechas a través del botón Reservar ahora para ver ofertas especiales '
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '(español) Tarifas por noche en dólares americanos'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '• Más 19 % de impuestos (16 % de IVA y 3 % de impuestos por hospedaje)'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '$25USD por persona, por noche para terceras o cuartas personas'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Mínimo de 2 a 3 noches en verano (temporada baja), mínimo de 4 a 5 noches en invierno (temporada alta) y mínimo de 7 noches en días festivos (puede variar, sujeto a disponibilidad y época del año).'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Desayuno completo cocinado al momento servido 6 días a la semana (no hay desayuno los domingos)'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Tenga en cuenta: el QMC no es adecuado para niños menores de 18 años.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Pagos:'
      -
        type: text
        text: " depósito de una noche al momento de la reserva. El pago total se debe 30 días antes de la llegada.\_"
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Política de cancelación:'
      -
        type: text
        text: " sin cargo por cancelaciones 31 días o más antes de la llegada. Cargo de 1 noche por cancelaciones 30 días o menos antes de la fecha de llegada. Las tarifas de cancelación pueden variar según la época del año, la demanda y el tiempo de espera de la reserva.\_"
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Depósito durante temporada de vacaciones y política de cancelación: '
      -
        type: text
        text: 'El 50% del depósito total de la estancia se debe pagar al momento de hacer la reservación. El pago total se debe 90 días antes de la llegada. Todos los depósitos y pagos de reservaciones durante temporada de vacaciones no son reembolsables, incluidas las cancelaciones y no-shows. No se puede otorgar crédito por llegadas tardías o salidas anticipadas.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Depósitos de reserva de grupo y políticas de cancelación: '
      -
        type: text
        text: 'Se debe pagar un depósito no reembolsable del 50 % al momento de hacer la reservación. El pago final vence 90 días antes de la fecha de llegada. Todos los depósitos y pagos finales no son reembolsables, incluidas las cancelaciones y los no-shows. No se puede otorgar crédito por llegadas tardías o salidas anticipadas.'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Información bajo las tarifas Casa Tres Vidas'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Todos los precios son en dólares americanos'
  -
    type: set
    attrs:
      values:
        type: table
        table:
          -
            cells:
              - ''
              - '**# DE HABITACIONES**'
              - '**VERANO** (05/01 - 10/31)'
              - '**INVIERNO** (11/01 - 04/30)'
              - '**DÍA FESTIVO** (12/15 - 01/06)'
          -
            cells:
              - '[Vida Alta](https://villasinvallarta.com/es/villas/vida-alta)'
              - '3'
              - $815
              - '$1,015'
              - '$1,225'
          -
            cells:
              - '[Vida Alta & Sol](https://villasinvallarta.com/es/villas/vida-alta-and-sol-at-ctv)'
              - '6'
              - '$1,530'
              - '$1,930'
              - '$2,290'
          -
            cells:
              - '[Vida Sol](https://villasinvallarta.com/es/villas/vida-sol)'
              - '3'
              - $715
              - $915
              - '$1,105'
          -
            cells:
              - '[Vida Sol & Mar](https://villasinvallarta.com/es/villas/vida-sol-and-mar-at-ctv)'
              - '7'
              - '$1,650'
              - '$2,050'
              - '$2,500'
          -
            cells:
              - '[Vida Mar](https://villasinvallarta.com/es/villas/vida-mar)'
              - '4'
              - $935
              - '$1,135'
              - '$1,335'
          -
            cells:
              - '[**Todo de Casa Tres Vidas**](https://villasinvallarta.com/es/villas/casa-tres-vidas)'
              - '10'
              - '$2,465'
              - '$3,065'
              - '$3,656'
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: buttons
        alignment: justify-center
        buttons:
          -
            link: 'http://reservation.worldweb.com/hotel/1183/?location_id=289'
            button_text: 'BOOK NOW'
            open_in_new_tab: false
            button_classes: null
  -
    type: paragraph
    content:
      -
        type: text
        text: '*** '
      -
        type: text
        marks:
          -
            type: bold
        text: 'Consulte sus fechas a través del botón Reservar Ahora para ver las ofertas especiales (tarifas semanales, último minuto, etc.)'
      -
        type: text
        text: ' ****'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Tarifas por noche en dólares americanos'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Más 19% de impuestos (16 % de IVA y 3 % de impuestos por hospedaje)'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Las tarifas especiales de 1 o 2 habitaciones están disponibles durante la temporada baja y dentro de los 60 días posteriores a la llegada. Por favor, pregunte sobre tarifas y disponibilidad.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Las tarifas incluyen el servicio bilingüe de camarista, chef y mesero/encargado de villa. El costo de alimentos y bebidas no está incluido.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Estadía mínima: mínimo 2 a 3 noches en verano (temporada baja), mínimo de 4 a 5 noches en invierno (temporada alta) y mínimo de 7 noches en días festivos (la estadía mínima puede variar según la época del año y los patrones de reserva). '
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Vida Sol tiene capacidad para 10 personas, Vida Mar tiene capacidad para 8 personas y Vida Alta tiene capacidad para 6 personas.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Distribución de las Villas:'
      -
        type: text
        text: ' Vida Alta & Sol se pueden combinar para hospedar a 16 personas. Vida Sol & Mar se pueden combinar para hospedar a 18 personas. Las 3 villas se pueden combinar para crear su propio recinto privado y se pueden hospedar hasta 24 personas. Si necesita alojamiento para hospedar a más de 24 personas, hay habitaciones adicionales disponibles en nuestra propiedad hermana contigua, Quinta Maria Cortez. Para ver las lujosas suites y amenidades, visite nuestra galería. Póngase en contacto con nosotros directamente para obtener tarifas especiales al combinar habitaciones en ambas propiedades.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Pagos:'
      -
        type: text
        text: ' Casa Tres Vidas requiere un depósito del 25% al momento de hacer la reservación (si es más de 90 días antes de la llegada). Segundo pago del 25% con vencimiento 90 días antes. El saldo debe pagarse 30 días antes de la llegada y 90 días antes de la llegada si es una reservación durante temporada vacacional.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Política de cancelación/reembolso:'
      -
        type: text
        text: ' Sin cargo por cancelaciones con más de 60 días antes de la fecha de llegada. Cargo del 50% de 31 a 59 días antes de la llegada. No reembolsable en cualquier cancelación 30 días o menos antes de la llegada. No se otorgan créditos ni reembolsos por llegadas tardías o salidas anticipadas.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Reservaciones de grupo:'
      -
        type: text
        text: ' se debe hacer un depósito no reembolsable del 50 % al momento de hacer la reservación. El pago final vence 90 días antes de la fecha de llegada. Todos los depósitos y pagos finales no son reembolsables, incluidas las cancelaciones y los no-shows. No se puede otorgar crédito por llegadas tardías o salidas anticipadas.'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Información sobre tarifas Casa P:'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Todos los precios son en dólares americanos'
  -
    type: set
    attrs:
      values:
        type: table
        table:
          -
            cells:
              - null
              - ''
              - '**4 DORMITORIOS**'
          -
            cells:
              - Verano
              - 'May - Oct.'
              - $695
          -
            cells:
              - Invierno
              - 'Nov. - Apr.'
              - $835
          -
            cells:
              - 'Día festivo'
              - '16.Dec - 5.Enr'
              - '$1,095'
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: buttons
        alignment: justify-center
        buttons:
          -
            link: 'http://reservation.worldweb.com/hotel/1183/?location_id=290'
            button_text: 'RESERVE NOW'
            open_in_new_tab: false
            button_classes: null
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: italic
        text: ' '
      -
        type: text
        marks:
          -
            type: bold
        text: '*** Consulte sus fechas a través del botón Reservar ahora para ver las ofertas especiales (tarifas semanales, último minuto, etc.) ***'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Tarifas por noche en dólares americanos'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Más 19 % de impuestos (16 % de IVA y 3 % de impuestos por hospedaje)'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Las tarifas incluyen el servicio de camarista 6 días a la semana (domingo descanso del personal)'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Estancia Mínima: 3 a 4 noches mínimo durante el Verano (temporada baja), 4 a 5 noches mínimo durante el invierno (temporada alta) y 7 noches mínimo durante temporada de vacaciones. '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Pagos:'
      -
        type: text
        text: ' Casa Perezoso requiere un depósito del 25% al momento de hacer la reservación (si es más de 90 días antes de la llegada). Segundo pago del 25% con vencimiento 90 días antes de la llegada. El saldo debe pagarse 30 días antes de la llegada y 90 días antes de la llegada en reservaciones durante temporada de vacaciones.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Política de cancelación/reembolso'
      -
        type: text
        text: ": Sin cargo por cancelaciones con más de 60 días antes de la fecha de llegada. Cargo del 50% \_por cancelación 31 a 59 días antes de la llegada. No reembolsable por cualquier cancelación 30 días o menos antes de la llegada. No se otorgan créditos ni reembolsos por llegadas tardías o salidas anticipadas."
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Los precios están sujetos a cambios. Consulte nuestra '
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'https://staging.villasinvallarta.com/assets/docs/2021-POLITICA-DE-PRIVACIDAD---Villas-in-Vallarta.pdf'
              target: _blank
              rel: null
        text: 'Política de Privacidad'
      -
        type: text
        text: ' para obtener más información. '
seo:
  description: 'Villa Rates (ES).'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1690301712
---
