---
id: 6cddad67-3a15-4d43-a915-093f95d5c1bb
origin: 9c00bc08-b42d-40ca-9f36-b8761a2a02e6
title: 'Paquetes de boda'
bard:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'COSTO POR EVENTO'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'El costo por evento es de: $3,000 USD para hasta 50 invitados. Contáctenos si su boda es más pequeña o mayor a 50 invitados.'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'El Costo por Evento Incluye:'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Todos los empleados del evento: cantineros, meseros, chef y ayudante de cocina.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Renta de equipo: mesas, sillas, fundas para sillas, mantelería, cubiertos'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Uso de nuestro sistema de sonido existente en los 2 salones adyacentes del nivel medio (2 receptores Sony de 200 vatios con 5 juegos de bocinas conectadas a una computadora con iTunes y una biblioteca de aproximadamente 4000 canciones)'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'https://staging.villasinvallarta.com/assets/pdfs/Guia-para-bodas-QMC-CTV-WGL-2023.pdf'
              target: _blank
              rel: null
        text: 'Use este enlace para ver e imprimir nuestra guia de bodas'
      -
        type: hard_break
      -
        type: text
        marks:
          -
            type: italic
        text: '*El costo de alimentos y bebidas es adicional.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Para ayudar a que su día especial sea completo, trabajamos con una variedad de los mejores planificadores de bodas de Puerto Vallarta. En función de sus necesidades, podemos sugerirle un planificador de bodas que creemos que se adapta mejor a su personalidad y presupuesto. Algunos de nuestros organizadores de bodas locales favoritos son:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Tamara Bradley Morales de Conceptos elegantes. Ella puede ser contactada en:\_"
              -
                type: text
                marks:
                  -
                    type: link
                    attrs:
                      href: 'https://www.facebook.com/ChicConcepts'
                      target: null
                      rel: null
                text: ' Chic Concepts | Facebook'
              -
                type: text
                text: ' '
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Eva Corona o Michael Huhndorf de Vallarta Weddings. Los localizan en:\_"
              -
                type: text
                marks:
                  -
                    type: link
                    attrs:
                      href: 'http://www.vallartaweddings.com/'
                      target: _blank
                      rel: null
                text: www.vallartaweddings.com
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1689099577
---
