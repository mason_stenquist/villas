---
id: 48b14073-f4e3-4562-a7fd-2f7f5c626b83
origin: 95af035f-af31-4d2c-b3e8-f870715054e3
published: false
title: 'Menús de boda'
bard:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: Appetizers
  -
    type: set
    attrs:
      enabled: false
      values:
        type: table
        table:
          -
            cells:
              - Item
              - Price
          -
            cells:
              - 'Antipasta plate of imported meats, cheese and grapes'
              - $9.50
          -
            cells:
              - "Asparagus wrapped in Serrano with balsamic glaze\t"
              - $10.00
          -
            cells:
              - "Chicken Satays Grilled and Glazed\t"
              - $9.00
          -
            cells:
              - "Chorizo Stuffed Mushrooms\t"
              - $7.50
          -
            cells:
              - "Coconut Shrimp with Mango Chutney\t"
              - $13.75
          -
            cells:
              - "Crab Cakes\t"
              - $12.00
          -
            cells:
              - "Italian Bruchetta, Tomatoes, Basil & Pine nuts\t"
              - $9.00
          -
            cells:
              - "Marlin Tostadas Mazatlan style\t"
              - $9.00
          -
            cells:
              - "Mexican assortment of chips, salsa, guacamole, Taquitos and quesadillas\t"
              - $9.50
          -
            cells:
              - "Bruchetta, Cream Cheese, Capers, Dill and Smoked Salmon\t"
              - $11.00
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: "Soup &\_Salads"
  -
    type: set
    attrs:
      enabled: false
      values:
        type: table
        table:
          -
            cells:
              - Item
              - Price
          -
            cells:
              - 'French Onion Soup'
              - $7.00
          -
            cells:
              - 'Tortilla soup with chili pasilla and avocado'
              - $7.00
          -
            cells:
              - 'Ceasar Salad'
              - $9.00
          -
            cells:
              - 'Hot Spinach Salad with Bacon Vinaigrette'
              - $9.00
          -
            cells:
              - 'Baby Mescalin, fresh basil, pine nuts, fresh parmesan with Dijon Vinaigrette'
              - $10.00
          -
            cells:
              - 'Baby Mixed Greens, Arugula, Baby Tomatoes with Oriental Vinaigrette'
              - $10.00
          -
            cells:
              - 'Avocado Boats stuffed with Jumbo Shrimp in Aurora Sauce'
              - $13.50
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Main Courses'
  -
    type: set
    attrs:
      enabled: false
      values:
        type: table
        table:
          -
            cells:
              - Item
              - Price
          -
            cells:
              - 'Fresh Fish Veracruz Style'
              - $39.00
          -
            cells:
              - 'Fresh Fish sautéed in Garlic Sauce'
              - $39.00
          -
            cells:
              - 'Jumbo Shrimp Casa Tres Vidas: Broiled Bacon Wrapped'
              - $46.00
          -
            cells:
              - 'Filet & Shrimp'
              - $54.00
          -
            cells:
              - 'Filet Mignon Grilled with either Bernaise Sauce or Fresh Mushroom Ragout'
              - $45.00
          -
            cells:
              - 'Chicken Cordon Blue – Cheese & Spinach with Cream Sauce'
              - $32.00
          -
            cells:
              - 'Fettuccine with a Jumbo Shrimp Crown'
              - $36.00
          -
            cells:
              - 'Mexican Plate of Chili Relleno, Chicken Enchilada, Taquitos, Spanish Rice, Refried Beans, Guacamole'
              - $31.00
          -
            cells:
              - 'Mexican Traditional Buffet'
              - $36.00
          -
            cells:
              - 'Vegetarian Stuffed Portobello Mushroom'
              - $26.00
          -
            cells:
              - 'Lobster or Filet Mignon & Lobster'
              - 'On Request'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: '* If you want to offer your guest a choice of 2 entrees the higher price entrée will prevail'
      -
        type: hard_break
        marks:
          -
            type: italic
      -
        type: text
        marks:
          -
            type: italic
        text: '* All Main Courses are served with fresh baby vegetables in season along with either wild & white rice or oven roasted and fresh herbed potatoes'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: Desserts
  -
    type: set
    attrs:
      enabled: false
      values:
        type: table
        table:
          -
            cells:
              - Item
              - Price
          -
            cells:
              - 'New York Style Cheese Cake with Raspberry coulisse'
              - $9.00
          -
            cells:
              - 'Chocolate Decadance'
              - $9.00
          -
            cells:
              - 'Tres Leches Cake'
              - $8.00
          -
            cells:
              - Tiramasu
              - $9.00
          -
            cells:
              - Flan
              - $7.00
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: Beverages
  -
    type: table
    content:
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 2
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Mexican Beer & Margarita Bar:'
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 1
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: Margaritas
                  -
                    type: hard_break
                  -
                    type: text
                    text: 'Mexican Bottled Beer'
                  -
                    type: hard_break
                  -
                    type: text
                    text: 'Bottled Water'
                  -
                    type: hard_break
                  -
                    type: text
                    text: 'Soft Drinks'
          -
            type: table_cell
            attrs:
              colspan: 1
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: '$14.00 p/person for the 1st hour.'
                  -
                    type: hard_break
                  -
                    type: text
                    text: '$11.00 p/person each additional hour.'
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 2
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 2
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Open Domestic Bar, Including:'
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 1
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: Tequila
                  -
                    type: hard_break
                  -
                    type: text
                    text: 'Rum – Barcardi'
                  -
                    type: hard_break
                  -
                    type: text
                    text: 'Vodka – Absolute'
                  -
                    type: hard_break
                  -
                    type: text
                    text: Margaritas
                  -
                    type: hard_break
                  -
                    type: text
                    text: 'White & Red Chilean Wine'
                  -
                    type: hard_break
                  -
                    type: text
                    text: 'Mexican Bottled Beer'
                  -
                    type: hard_break
                  -
                    type: text
                    text: 'Mixers & soft drinks'
                  -
                    type: hard_break
                  -
                    type: text
                    text: 'Bottled Water'
          -
            type: table_cell
            attrs:
              colspan: 1
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: '$18.00 p/person for the 1st hour.'
                  -
                    type: hard_break
                  -
                    type: text
                    text: '$15.00 p/person each additional hour.'
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 2
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 2
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Champagne Toast:'
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 2
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: '$7.50 per person with Freixenet Champagne.'
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 2
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: hard_break
                    marks:
                      -
                        type: bold
                  -
                    type: text
                    marks:
                      -
                        type: bold
                      -
                        type: underline
                    text: 'Upgraded wine selections:'
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 1
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Casillero del Diablo'
                  -
                    type: hard_break
                  -
                    type: text
                    text: 'Cabernet or chardonnay'
          -
            type: table_cell
            attrs:
              colspan: 1
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: '$3.00 / person'
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 1
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Los Vascos'
                  -
                    type: hard_break
                  -
                    type: text
                    text: 'Cabernet or chardonnay'
          -
            type: table_cell
            attrs:
              colspan: 1
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: '$4.00 / person'
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 1
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: Errazuriz
                  -
                    type: hard_break
                  -
                    type: text
                    text: 'Merlot, Cabernet or chardonnay'
          -
            type: table_cell
            attrs:
              colspan: 1
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: '$6.00 / person'
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 2
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 2
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Champagne and/or wine with dinner can be quoted'
      -
        type: table_row
        content:
          -
            type: table_cell
            attrs:
              colspan: 2
              rowspan: 1
              colwidth: null
              background: null
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: '* 1 Hour minimum.'
                  -
                    type: hard_break
                  -
                    type: text
                    text: '* Beverage Packages Exclude 16% Tax & 15% Gratuities'
seo:
  description: 'Menús de boda'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1689790087
---
