---
id: c96e1965-986a-466c-924e-2db872db0f96
origin: 86314aca-7793-4aa4-b988-c5826ce9f353
bard:
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe class="rounded-xl" width="100%" height="450" src="https://www.youtube.com/embed/HcVk4QTRuLw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Ven a descubrir un enclave de 3 villas frente al mar ubicadas en el corazón de la Rivera del Pacífico de México - Casa Tres Vidas. Casa Tres Vidas (La Casa de 3 Vidas) son villas de 3 o 4 habitaciones que se pueden alquilar solas o combinadas para crear un complejo de 6, 7 o 10 habitaciones ubicado a pocos minutos al sur del centro de Puerto Vallarta, México.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Imagínese despertar en su lujosa villa mexicana privada y finamente decorada con los relajantes sonidos del océano justo afuera de su ventana. Después de disfrutar de un desayuno preparado a su gusto, la única decisión que debe tomar durante el día es ir de compras, descansar en su piscina privada, dar un paseo por la playa o disfrutar de una de las muchas excursiones de aventura disponibles para usted. Estas son sus vacaciones: pase los días como desee. Casa Tres Vidas permite a los viajeros experimentar México como deseen mientras disfrutan de la privacidad en un entorno tranquilo y lujoso.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Casa Tres Vidas, que aparece en el Architectural Digest, ofrece la flexibilidad para familias y grupos de varios tamaños en un entorno lujoso, lo que lo convierte en una opción única para su escapada de vacaciones.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'No se fíe sólo de nuestra palabra: los numerosos comentarios de nuestros invitados lo dicen todo:'
  -
    type: paragraph
    content:
      -
        type: text
        text: '"Esta fue una experiencia de vacaciones increíblemente fabulosa e inigualable ..." Vancouver BC, Canadá'
  -
    type: paragraph
    content:
      -
        type: text
        text: '"Desde el momento en que entramos por la puerta, nos trataron como a la realeza". Cedar Rapids, IA, Estados Unidos'
  -
    type: paragraph
    content:
      -
        type: text
        text: '“Estoy abrumado por lo maravillosa que fue nuestra estadía. Desde la hermosa villa con impresionantes vistas del océano hasta las comidas gourmet ... y la terraza en la azotea ... Casa Tres Vidas es el lugar para quedarse con amigos o familiares ". Seattle, WA, Estados Unidos'
seo:
  title: 'Casa Tres Vidas (ES)'
  description: 'Ven a descubrir un enclave de 3 villas frente al mar ubicadas en el corazón de la Rivera del Pacífico de México - Casa Tres Vidas. Casa Tres Vidas (La Casa de 3 Vidas) son villas de 3 o 4 habitaciones que se pueden alquilar solas o combinadas para crear un complejo de 6, 7 o 10 habitaciones ubicado a pocos minutos al sur del centro de Puerto Vallarta, México.'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642876749
---
