---
id: 58b4694c-dd4d-4095-a93e-523172df9b99
origin: bfae1bc7-bd68-4ba0-a6c8-bf177f1f59da
title: 'Guías para bodas'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Las reservaciones para bodas son diferentes a nuestras reservaciones comunes; normalmente operamos Quinta Maria Cortez "QMC" como un Bed & Breakfast boutique (incluimos el desayuno con el precio de la habitación, no ofrecemos otro servicio de bar o comida y no permitimos niños). Casa Tres Vidas “CTV” normalmente opera en el tradicional “Concepto de Villa” (alquilar las villas con personal de cocina y los huéspedes pagan la comida).'
  -
    type: paragraph
    content:
      -
        type: text
        text: "\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Para una reservación de bodas, ambas propiedades se reservan y operan de manera combinada, atendiendo a sus necesidades. Ofrecemos desayuno buffet de cortesía a todos los huéspedes, permitimos niños en ambas propiedades y otros servicios de alimentos y bebidas se contratan con anticipación a través de nuestra oficina en los Estados Unidos.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Guia de hospedaje:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Se requiere un período mínimo de renta de 4 noches (del 1 de mayo al 17 de diciembre) para ambas propiedades, Quinta Maria Cortez y Casa Tres Vidas, para asegurar el bloqueo de habitaciones para su boda.'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Durante este período podemos aceptar bodas más pequeñas utilizando solo Casa Tres Vidas, llámenos para obtener más detalles.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Se requiere un período mínimo de renta de 5 noches (del 6 de enero al 30 de abril) para ambas propiedades, Quinta María Cortez y Casa Tres Vidas, para asegurar el bloqueo de habitaciones para su boda.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Las vacaciones de Navidad / Año Nuevo están excluidas de reservaciones para bodas.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Se aplican las siguientes tarifas (la tarifa es para 4 o 5 noches):'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Tarifa económica de temporada baja por 4 noches (1 mayo a 31 octubre) - $20,254.00 USD'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Tarifa de temporada alta por 4 noches (1 NOV a 17 DIC) - $24,728.00 USD'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Tarifa Temporada alta por 4 noches (06 enero a 30 abril) solo llegadas en sábado - $30,910.00 USD'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Hay un total de 19 habitaciones con 25 camas y 1 sofá-cama tamaño Queen que se detalla a continuación para las 2 propiedades:'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'CTV – 3 villas (Vida Alta, Vida Sol y Vida Mar) con un total de 10 dormitorios'
                  -
                    type: bullet_list
                    content:
                      -
                        type: list_item
                        content:
                          -
                            type: paragraph
                            content:
                              -
                                type: text
                                text: '11 camas King y 1 cama Queen más un sofá cama Queen.'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'QMC – 7 suites con un total de 9 habitaciones'
                  -
                    type: bullet_list
                    content:
                      -
                        type: list_item
                        content:
                          -
                            type: paragraph
                            content:
                              -
                                type: text
                                text: '5 camas tamaño King, 4 camas tamaño Queen y 4 camas tamaño individual que se usan como sofás.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'La ocupación máxima combinando ambas propiedades es para 42 huéspedes.'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'A cualquier invitado adicional se le cobrará $ 25.00 USD por persona/por noche (incluye desayuno).'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Cobranza (para los invitados):'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Este es un servicio que ofrecemos, con los siguientes elementos:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Deberá proporcionarnos una '
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'lista completa '
              -
                type: text
                text: 'con habitaciones asignadas a cada invitado, al menos 120 días antes de la llegada con '
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'nombres completos, direcciones de correo electrónico y números de teléfono'
              -
                type: text
                text: .
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Las tarjetas de crédito individuales se procesarán entre 45 y 60 días antes de la llegada para el total adeudado si el pago se realiza en una sola exhibición. Para la mayoría de las bodas, cobramos un depósito de una noche de 2 a 6 meses antes de la fecha de la boda y el pago final 45 días antes de la llegada.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Solo una tarjeta de crédito por habitación, por favor.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Costo por el evento de boda:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'La tarifa del evento de boda es de $ 3,000 USD para hasta 50 invitados. Esta tarifa de evento cubre el uso de nuestra propiedad y playa para su evento de boda. Esta tarifa es aplicable para eventos de lunes a sábado. Los eventos dominicales o festivos incurren en cargos adicionales.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Invitados adicionales a los 50, tienen un costo extra de $50 USD por persona. Múltiples eventos incurrirán en cargos adicionales. La tarifa del evento también incluye:'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Todos los empleados para el evento: Meseros, cantineros, chef y ayudantes de cocina'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Renta de equipo: mesas, sillas, fundas para sillas, mantelería, cubiertos'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Se aplicará una tarifa adicional de $25 USD por persona para cualquier evento de cena formal en la playa o en cualquiera de las terrazas.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Depósitos y Pagos:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Para el hospedaje (bloque completo de habitaciones) y costo del evento'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: '25% en el momento de confirmar la reservación, para reservas realizadas más de 9 meses antes de la llegada'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: '25% - 9 meses antes de la llegada o el 50% del total'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: '25% - 4 meses antes de la llegada o 75% del total'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Pago final 30 días antes de la llegada.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Alimentos y Bebidas'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Los costos de alimentos y bebidas vencen 30 días antes de su evento'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Las garantías de alimentos y bebidas vencen 14 días antes de la llegada de su grupo'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Cancelaciones:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Hospedaje – Bloqueo completo de habitaciones'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'El depósito del 25% no es reembolsable'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Un total del 50% de la tarifa de alojamiento no es reembolsable por cancelaciones realizadas entre 9 meses y 120 días antes de la fecha de llegada'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Un total del 75% de la tarifa de alojamiento no es reembolsable para cancelaciones realizadas entre 120 y 31 días antes de la fecha de llegada'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Reservaciones canceladas 30 días o menos antes de la llegada no son reembolsables en absoluto'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Si hubiera un problema/evento improbable/imprevisible, como un incendio en la propiedad, un terremoto, un huracán en o alrededor de Puerto Vallarta que nos impida celebrar su evento de boda en nuestra propiedad, le ofreceremos un reembolso completo por las noches de habitación no utilizadas, alimentos y bebidas, así como el costo del evento.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Coordinadora de bodas:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Se debe contratar a una coordinadora de bodas profesional para supervisar los detalles de la boda que Villas en Vallarta no incluye en el paquete. Le invitamos a elegir cualquier planificador de bodas local'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Nosotros le recomendamos a:'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Tamara Bradley Morales, Chic Concepts, puede ponerse en contacto con ella al correo electrónico: '
                      -
                        type: text
                        marks:
                          -
                            type: link
                            attrs:
                              href: 'mailto:Weddingspv@yahoo.com'
                              target: null
                              rel: null
                        text: Weddingspv@yahoo.com
                      -
                        type: text
                        text: ' Su página de internet es '
                      -
                        type: text
                        marks:
                          -
                            type: link
                            attrs:
                              href: 'http://www.chicconcepts.com.mx'
                              target: null
                              rel: null
                        text: www.chicconcepts.com.mx
                      -
                        type: text
                        text: ' y su teléfono es (52) 322 141-5121'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Visita a las instalaciones'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Ofrecemos una estadía de 3 noches en cortesía para reservaciones confirmadas, en caso de que desee ver la selección completa de nuestras habitaciones, espacios públicos y el resto de la propiedad.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Si desea hacer una inspección de la propiedad, antes de hacer el depósito en su reservación, le cobraremos por su estadía y se le acreditara cuando haga la reservación de su boda.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Opciones en la propiedad:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Surtimos bebidas por un cargo fijo de $300 USD, abasteceremos los refrigeradores antes de su llegada en las suites y villas con:'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'CTV - En Vida Alta, Vida Sol y Vida Mar - 12 Coca-Colas, 12 cocas de dieta, 12 Sprite y 12 cervezas junto con agua embotellada'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'QMC: en cada una de las 7 suites, 6 Coca-Colas, 6 cocas de dieta, 6 Sprite y 6 cervezas junto con agua embotellada'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Ofrecemos un desayuno de cortesía 6 días a la semana para hasta 42 invitados, servido durante 1 hora cada día\_\_\_\_\_\_\_ (9:00 a. m. a 10:00 a. m.)"
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'No ofrecemos desayuno los domingos'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Alimentos y bebidas:'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Le proporcionaremos un orden de evento de banquete "F&B" de alimentos y bebidas "BEO" mucho antes de su llegada. Todas las comidas preparadas en la propiedad durante su evento de 4 o 5 días deben pedirse con 30 días de anticipación y anotarse en el F&B BEO.'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Cualquier artículo adicional, como botanas (papas fritas, salsas, etc.) y bebidas que se soliciten en el lugar, debe hacerse con 24 horas de anticipación. Nuestro personal va de compras todos los días a las 11:00 a. m. para el siguiente período de 24 horas. Las solicitudes deben hacerse antes de las 10:30 am para esa tarde y el día siguiente. Las solicitudes deben realizarse antes de las 10:30 am del sábado para el sábado por la tarde, el domingo y el lunes por la mañana. La mayoría del personal tiene el domingo libre. Cualquier artículo adicional solicitado en la propiedad, debe pagarse en efectivo al momento de que le entreguen la cuenta.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Miscelaneos:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Niños - los niños menores de 2 años no tienen que pagar. Los niños menores de 18 años están exentos de los cargos por servicio de bar.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'El día de su boda, le solicitamos que se abstenga de utilizar la sala y la piscina de Vida Sol para darle a nuestro personal la oportunidad de preparar la recepción y la cena de la boda.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Personal: El domingo es el día libre del personal. No ofrecemos desayuno ni otro servicio de comidas los domingos. Tampoco contamos con servicio de limpieza los domingos.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Lluvia: como saben, somos una propiedad única y no tenemos un salón de fiestas como respaldo. En caso de lluvia se vuelve más difícil atender a más de 55 invitados.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Música: ofrecemos el uso de nuestro sistema de sonido existente en QMC y CTV (nivel medio en las salas/áreas de estar contiguas) que consta de una computadora con iTunes (alrededor de 5000 canciones), 2 receptores Sony (200 vatios), un CD con reproductor para 5 CDs con 8 a 10 bocinas y conexiones para iPod. Su planificador de bodas local también tiene muchas opciones de música en vivo disponibles para usted.'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Si planea usar un iPod, necesita asignar un DJ o alguien para manejar la música.'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Recuerde, estamos en un área residencial y no podemos tener música alta después de las 11:00 p.m.'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Respete nuestro equipo y no reproduzca música a un volumen alto que pueda hacer estallar las bocinas o el sistema de sonido. Favor de no mover nuestro equipo sin la aprobación previa de la gerente.'
seo:
  description: 'Wedding reservations are different than our typical operations; normally we operate Quinta Maria Cortez “QMC” as a boutique Bed & Breakfast (were we include breakfast with the room rate, do not offer other bar or meal service and do not allow children).'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1689100123
---
