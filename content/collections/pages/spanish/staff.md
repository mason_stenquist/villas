---
id: 18a3105c-654e-4d30-a01d-8e8c66cab8d5
origin: fcd2ca16-093c-41fd-9af5-72da6c41bf59
title: Personal
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Sus anfitriones Margaret Parrish y Jon Christopher Jones le dan la bienvenida a la escapada perfecta frente a la playa en el soleado y cálido Puerto Vallarta, México.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Margaret, que ha vivido en todo el mundo porque su padre estaba en la armada americana, se enamoró por primera vez de Vallarta durante su luna de miel a mediados de los 80. Después de vender un pequeño pero  próspero cafe que tenia en Salt Lake City, se aburrió rápidamente. Sus sueños siempre la llevaron de regreso a Vallarta, el lugar del que se enamoró desde el momento en que llegó.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Jon era amigo de Margaret de Salt Lake City, donde era dueño de una exitosa agencia de viajes: Travel Zone. Desde un viaje de vacaciones de primavera a México cuando estaba en la secundaria, el objetivo de Jon era mudarse allí algún día. La amplia experiencia de Jon en hotelería como Director del programa Hospitalidad Global de Coca-Cola para los Juegos Olímpicos del 2002 y muchos otros proyectos para los Juegos Olímpicos y Paralímpicos, fue el campo de entrenamiento perfecto como copropietario de un par de hoteles.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Juntos, en 1997, rescataron un diamante en bruto en la playa perfecta en Vallarta y Quinta María Cortez se convirtió en una realidad después de meses de renovaciones y mejoras. Solo tres años después, la propiedad vecina estuvo disponible y Casa Tres Vidas se unió al enclave que conforma Villas en Vallarta.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Margaret, Jon, Sara González, su Gerente General bilingüe que se unió al grupo en 2018 y todo el equipo de Villas en Vallarta, ¡esperan darte la bienvenida!'
seo:
  description: 'Sus anfitriones Margaret Parrish y Jon Christopher Jones le dan la bienvenida a la escapada perfecta frente a la playa en el soleado y cálido Puerto Vallarta, México.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1687887283
---
