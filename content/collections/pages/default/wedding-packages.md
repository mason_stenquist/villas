---
id: 9c00bc08-b42d-40ca-9f36-b8761a2a02e6
blueprint: page
title: 'Wedding Packages'
parent: 1d183432-707d-461e-add2-4d8a30eddf08
template: pages/wedding
header_image: header-images/vallarta-weddings-0035.jpg
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1735239187
bard:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Traditional Mexican Plated Dinner'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "One Hour Reception, including: Mexican Beer,\_Bottled water, Soft Drinks and Margaritas"
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Chips, Fresh Mexican Salsa, Guacamole,\_Chicken Taquitos"
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Traditional Mexican Plated Dinner of Chili Rellenos,\_Chicken Enchiladas"
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Spanish Rice, Refried Beans'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Cold Black Bean & Corn Salad with Cilantro Vinaigrette'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Flour and Corn Tortillas'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Mexican Salsa & Guacamole'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Red & White Wine served with dinner'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Coffee & tea service'
  -
    type: paragraph
    content:
      -
        type: text
        text: '$75.00 per person plus 16% tax & 15% gratuity'
  -
    type: paragraph
    content:
      -
        type: text
        text: "$15.00 per person, per additional hour for Bar\_as mentioned above plus 16% tax & 15% gratuity"
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Fresh Catch of the Day'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'One Hour open domestic bar'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Fresh Asparagus wrapped in Serrano\_and Stuffed Chorizo Mushrooms"
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Fresh Baby Spinach Salad with warm Bacon\_Vinaigrette Dressing"
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Choice of Fresh Catch of the Day or\_Stuffed Chicken Breast"
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Baby Vegetables in Season & Sautéed Herbed\_Baby Red Potatoes"
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Fresh Baked Bolillo (Mexican French Bread)\_with butter"
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Red & White Wine Served with dinner'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Coffee & tea service'
  -
    type: paragraph
    content:
      -
        type: text
        text: '$84.00 per person plus 16% tax & 15% gratuity'
  -
    type: paragraph
    content:
      -
        type: text
        text: "$16.00 per person, per additional hour for Bar\_as mentioned above 16% tax & 15% gratuity"
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Surf & Turf'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'One Hour open domestic bar'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Chorizo Stuffed Mushrooms, Bruchetta of\_Fresh Tomato, Fresh Basil and Cheese, Cerviche"
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Baby Green Salad with parmesean cheese, pinenuts and vinegrette'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Surf & Turf: Grilled Tenderloin of Beef\_with Port Wine Reduction and"
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Garlic Sautéed Jumbo Shrimp'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Baby vegetables in season & Sautéed Herbed\_Baby Red Potatoes"
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Baked Mini Bolillos (Mexican French Bread)\_with butter"
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Red & White Chilean Wine served with Dinner'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Coffee & tea service'
  -
    type: paragraph
    content:
      -
        type: text
        text: '$110.00 per person plus 16% tax & 15% gratuity'
  -
    type: paragraph
    content:
      -
        type: text
        text: '$17.00 per person, per additional hour for Bar'
      -
        type: hard_break
      -
        type: text
        text: 'as mentioned above plus 16% tax & 15% gratuity'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Welcome Party'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Sunset Welcome reception on the terrace of Quinta Maria Cortez and Casa Tres Vidas.'
  -
    type: paragraph
    content:
      -
        type: text
        text: '2 Hour Reception, including: Mexican Beer, Margaritas, Bottled water and Soft Drinks.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Heavy Hors d’ oeuvres – Chips, Fresh Mexican Salsa, Guacamole, Taquitos, Quesadillas, Chicken satay, Sopas and Flan.'
  -
    type: paragraph
    content:
      -
        type: text
        text: '$55.00 per person plus 16% tax & 15% gratuity'
  -
    type: paragraph
    content:
      -
        type: text
        text: '$15.00 per person per additional hour for Bar as mentioned above plus 16% tax & 15% gratuity'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: '*Prices valid through 2024'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'EVENT FEE'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Event fee: $3,000 USD for up to 50 guests. Contact us for a bid on small weddings or for weddings with over 50 guests.'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Event fee includes:'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'All event employees – bartenders, wait staff, chef and kitchen help'
      -
        type: hard_break
      -
        type: text
        text: 'Rental Equipment – tables, chairs, chair covers, linens, place settings'
      -
        type: hard_break
      -
        type: text
        text: 'Use of our existing Sound system in the 2 adjacent mid-level salons (2 Sony 200 watt receivers with 5 sets of speakers connected to a computer with ITunes and a library of approximately 4000 songs)'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'statamic://entry::bfae1bc7-bd68-4ba0-a6c8-bf177f1f59da'
              target: null
              rel: null
        text: 'Click here to view and print our wedding guidelines'
      -
        type: hard_break
      -
        type: text
        marks:
          -
            type: italic
        text: '*Food and beverage costs are additional.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'To help make your special day complete, we ask that you hire a wedding planner. We recommend:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Rocio Quintal, +52 322 429 7498'
---
