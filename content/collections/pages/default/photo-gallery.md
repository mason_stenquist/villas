---
id: 90afad3e-2887-43cd-bdf5-865c19a864be
blueprint: page
title: 'Photo Gallery'
bard:
  -
    type: set
    attrs:
      values:
        type: gallery
        photos:
          - villas/ctv/alta/2H5A2124.jpg
          - villas/ctv/mar/2H5A6659.jpg
          - villas/ctv/mar/IMG_6566.jpg
          - villas/ctv/sol/2H5A6548.jpg
          - villas/ctv/sol/2H5A6711.jpg
          - villas/qmc/alex/IMG-4390-CR07octt3f-P160x200.jpg
          - villas/qmc/ana_gab/IMG-8570-CR4t-P160x200.jpg
          - villas/qmc/guadalupe/QMC-B-Ter-W-4217a.jpg
          - villas/qmc/guadalupe/IMG_6475.jpg
          - villas/qmc/jessica/IMG_5082.jpg
          - villas/qmc/jessica/QMC-Jes-Sala-4429-P160x200.jpg
          - villas/qmc/maria/IMG-6010-CR18t.jpg
          - villas/qmc/maxi/QMC-Max--4377a.jpg
          - villas/qmc/rafael/IMG-8622-CR4t.jpg
  -
    type: paragraph
template: pages/show
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642877946
seo:
  description: '@seo:title'
---
