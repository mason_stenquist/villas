---
title: "Quinta Maria Cortez"
template: villas/index
parent: 3e96212a-fdc7-48e5-8dce-256811192ee3
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1616268664
blueprint: villa_overview
bard:
    - type: set
      attrs:
          values:
              type: embed_html
              html: '<iframe class="rounded-xl" width="100%" height="450" src="https://www.youtube.com/embed/KyNrpv8l1jo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
    - type: paragraph
      content:
          - type: text
            marks:
                - type: bold
            text: "Quinta Maria Cortez"
          - type: text
            text: ", like our adjoining property\_"
          - type: text
            marks:
                - type: link
                  attrs:
                      href: "http://villasinvallarta.com/villas/ctv/"
                      target: null
                      rel: null
                - type: bold
            text: "Casa Tres Vidas"
          - type: text
            text: ", is located on a spectacular beach just minutes south of the international destination Puerto Vallarta, Mexico. This bed & breakfast is a favorite destination for travelers from around the world searching for a relaxing getaway in a unique setting. The Quinta Maria Cortez Bed and Breakfast is close to many notable landmarks, spectacular sights and numerous activities.\_ Whether you want to experience it all or do nothing but read by the pool or on the beach, Quinta Maria Cortez can satisfy your every whim. \_With us, you’ll enjoy a unique local flavor fused with modern comforts. Each sophisticated suite is furnished with beautiful antique furniture and has all the amenities to ensure that you’ll be comfortable, relaxed, and fully satisfied with your stay. Visitors express their appreciation and pleasure with their stay time and time again, and many are repeat guests with us each year."
    - type: paragraph
      content:
          - type: text
            text: "Some of the Quinta Maria Cortez Bed and Breakfast features and amenities include:"
    - type: bullet_list
      content:
          - type: list_item
            content:
                - type: paragraph
                  content:
                      - type: text
                        text: "Seven unique oceanfront suites all finely decorated with classic and antique Mexican furniture."
          - type: list_item
            content:
                - type: paragraph
                  content:
                      - type: text
                        text: "Walking distance from Puerto Vallarta, one of the “Top 10” international vacation destinations."
          - type: list_item
            content:
                - type: paragraph
                  content:
                      - type: text
                        text: "Easy access to nearby jungle tours, galleries, restaurants, shopping, fishing adventures, historic sites and miles of golden-sand beaches."
          - type: list_item
            content:
                - type: paragraph
                  content:
                      - type: text
                        text: "Ideal for couples and honeymooners (An Adult Property – Not suitable for children under 18)."
          - type: list_item
            content:
                - type: paragraph
                  content:
                      - type: text
                        text: "Delicious made to order breakfast served 6 days a week (No Breakfast on Sundays)."
    - type: paragraph
      content:
          - type: hard_break
          - type: text
            text: "Take a few minutes and explore our unique property and discover Quinta Maria Cortez Bed & Breakfast,"
          - type: text
            marks:
                - type: bold
                - type: italic
            text: "an escape from the expected"
          - type: text
            marks:
                - type: italic
            text: .
villa_type: qmc
id: 2b442bb6-711f-4a55-9a6e-6d4362003d26
---
