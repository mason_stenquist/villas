---
id: fd2574b6-7700-4561-8585-865524f9b8b1
blueprint: page
title: 'Event Accomodations'
template: pages/show
parent: a1a93215-87c8-4247-bf1b-f1157f1b1b04
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1687890758
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: "Our adjoining properties,\_Quinta Maria Cortez\_and\_Casa Tres Vidas, are both located on one of Puerto Vallarta, Mexico’s most beautiful beaches. Each property is unique and offers you a variety of luxurious accommodations and unforgettable getaway opportunities for your dream wedding.\_ Once your wedding is confirmed, we communicate and coordinate the deposits, arrivals and departures directly with your guests, freeing you up to accomplish all of the other items on your to-do list."
  -
    type: paragraph
    content:
      -
        type: text
        text: "Quinta Maria Cortez, is a 7 bedroom boutique hotel that masterfully combines beautiful antique furniture and dramatic design with all the modern amenities to ensure that you’ll be comfortable, all creating a unique ambiance… a bit of whimsical tropical antiquity by the sea.\_ "
  -
    type: paragraph
    content:
      -
        type: text
        text: "Just next door, Casa Tres Vidas is an enclave of 3-or 4-bedroom beachfront villas that when combined with\_Quinta Maria Cortez, creates a compound that accommodates 42 overnight guests."
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We invite you to explore our properties, each warmed by the tropical Mexican sun.'
header_image: header-images/event-accommodations.jpg
seo:
  description: "Our adjoining properties,\_Quinta Maria Cortez\_and\_Casa Tres Vidas, are both located on one of Puerto Vallarta, Mexico’s most beautiful beaches, just steps from your personal retreat."
---
