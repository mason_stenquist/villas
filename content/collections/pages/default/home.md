---
id: home
blueprint: home
title: Home
template: home
layout: layout
features:
  -
    image: Eduardo-1A.jpeg
    heading: 'COVID Update'
    sub_heading: 'We are doing everything we can to ensure you feel safe and comfortable during your stay with us. The “new normal” is continually changing, however, we are totally committed to continue upholding the highest standards regarding social distancing, disinfection, hygiene, sanitation, etc. In response to the recent CDC regulations for re-entry into the US and Canada - and other countries - we are making it very easy and worry-free for you. Testing can be conveniently administered on-property by a certified professional, so you can continue to enjoy your vacation with peace of mind. Upon completion of testing, guests will receive electronic results and will also obtain a printed copy.'
    type: feature
    enabled: false
  -
    image: IMG_9009.jpg
    heading: 'Beach Front'
    sub_heading: 'A private, luxurious and finely appointed Mexican enclave, the perfect combination for a relaxing getaway in a unique setting where you will fall asleep and wake up to the calming sounds of the ocean just outside your window. The QMC/CTV compound is one of the only properties directly on the beach. Beach towels, loungers, chairs, tables and umbrellas are provided for all our guests.  Unobstructed views of the sunrise and sunset can be enjoyed from all rooms.'
    type: feature
    enabled: true
  -
    image: header-images/staff.jpg
    heading: Staff
    sub_heading: 'Our unique property is operated by friendly, loyal and dedicated staff that is enthusiastic and proud about their professional hospitality.  In addition to a superior guest experience, we are committed to making sure your needs are met and your stay with us is memorable.'
    type: feature
    enabled: false
bard:
  -
    type: set
    attrs:
      values:
        type: text_columns
        number_of_columns: 2
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Welcome to Villas in Vallarta, Quinta Maria Cortez & Casa Tres Vidas, Puerto Vallarta’s best kept secret. Our inviting boutique hotel features two beautifully distinct estates, each offering old world elegance in a contemporary setting. We are a boutique property located on Puerto Vallarta’s most exclusive beach, yet the city center is only a short walk away. Our dedicated staff is trained to ensure your stay with us will exceed all expectations.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We offer accommodations ranging from beautiful suites at Quinta Maria Cortez, to private 3, 4, 6, 7 & 10 bedroom villas at Casa Tres Vidas. '
  -
    type: set
    attrs:
      values:
        type: call_to_action
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Have questions? Go ahead and give us a call today at '
              -
                type: text
                marks:
                  -
                    type: link
                    attrs:
                      href: 'tel:8012639500'
                      target: null
                      rel: null
                text: '(801) 263-9500'
              -
                type: text
                text: ', or shoot us an email at '
              -
                type: text
                marks:
                  -
                    type: link
                    attrs:
                      href: 'mailto:reservations@villasinvallarta.com'
                      target: _blank
                      rel: null
                text: reservations@villasinvallarta.com
        button_text: 'Contact Us Today'
        button_color: '#4FD1C5'
        link: 'entry::e6bafa7f-0b95-49f9-ba9a-b69b71999917'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe class="rounded-xl" width="100%"  height="500" src="https://www.youtube.com/embed/HcVk4QTRuLw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
  -
    type: paragraph
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1735239973
featured_content_title: 'Villas In Vallarta - Quinta Maria Cortez & Casa Tres Vidas'
properties:
  -
    property_title: 'Quinta Maria Cortez'
    property_image: 2H5A2696.jpg
    property_description: 'QMC is located on a spectacular beach just minutes south of the international destination Puerto Vallarta, Mexico. This bed and breakfast is a favorite destination for travelers from around the world searching for a relaxing getaway in a unique setting.'
    type: property
    enabled: true
    property_link: 'entry::2b442bb6-711f-4a55-9a6e-6d4362003d26'
  -
    property_title: 'Casa Tres Vidas'
    property_image: 2H5A2567.jpg
    property_link: 'entry::86314aca-7793-4aa4-b988-c5826ce9f353'
    property_description: 'Come discover an enclave of 3 beachfront villas located in the heart of Mexico’s Pacific Rivera, Casa Tres Vidas. Imagine awakening in your private luxurious and finely appointed Mexican villa to the calming sounds of the ocean just outside your window.'
    type: property
    enabled: true
slider:
  -
    title: 'Welcome to Puerto Vallarta'
    subtitle: 'AT ITS HEAVENLY BEST'
    assets: home/OceanView.jpg
    type: slide
    enabled: false
    image: home/OceanView.jpg
  -
    title: 'The Beauty of Boutique'
    subtitle: 'View From Maria Suite at QMC'
    assets: home/Patio.jpg
    type: slide
    enabled: true
    image: home/Patio.jpg
  -
    title: 'Availability at Quinta Maria Cortez!'
    subtitle: 'Click Book Now to see dates and rates'
    image: home/Alta-Special-Offer.jpg
    type: slide
    enabled: true
  -
    title: 'Room to Relax'
    subtitle: 'Private Pool & Sala in Vida Mar at CTV'
    image: home/CTV_Mar_3.jpg
    type: slide
    enabled: true
  -
    title: 'Casa Tres Vidas Villas Available'
    subtitle: 'Click Book Now to see available dates and rates'
    image: home/Sol-Special-Offer.jpg
    type: slide
    enabled: true
  -
    title: 'Celebrate New Year''s in Puerto Vallarta'
    subtitle: |-
      Vida Alta 3 Bedroom Villa
      December 30, 2022 - January 7, 2023
      Book Now!
    image: home/NYE-Vida-Alta.jpg
    type: slide
    enabled: false
  -
    title: 'March Availability - Vida Mar 4 Bedroom Villa'
    subtitle: |-
      Mar 17 - Mar 26, 2023
      Click Book Now to view rates
    image: home/OceanView.jpg
    type: slide
    enabled: false
seo:
  description: 'Welcome to Villas in Vallarta, Quinta Maria Cortez & Casa Tres Vidas, Puerto Vallarta’s best kept secret.'
---
I see a bad-ass mother who don't take no crap off of nobody.
