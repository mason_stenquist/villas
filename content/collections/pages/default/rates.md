---
id: eef4b1d8-2cdd-41de-90ec-094d6da1b41c
blueprint: page
title: Rates
parent: 53a89af7-91ab-4676-a477-d0b6381df8b2
bard:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'QUINTA MARIA CORTEZ – SUITE RATES'
  -
    type: set
    attrs:
      values:
        type: table
        table:
          -
            cells:
              - ''
              - '**SUMMER** (05/01 - 10/31)'
              - '**WINTER** (11/01 - 04/30)'
              - '**HOLIDAY** (12/20 - 01/04)'
          -
            cells:
              - '[Maximilliano](/villas/maximiliano)'
              - $290
              - $360
              - $415
          -
            cells:
              - '[Maria](/villas/maria)'
              - $290
              - $355
              - $410
          -
            cells:
              - '[Jessica](/villas/jessica)'
              - $185
              - $230
              - $260
          -
            cells:
              - '[Ana Gabriela](/villas/ana-gabriela)'
              - $275
              - $325
              - $385
          -
            cells:
              - '[Rafael](/villas/rafael)'
              - $275
              - $325
              - $385
          -
            cells:
              - '[Alejandro](/villas/alejandro)'
              - $285
              - $340
              - $400
          -
            cells:
              - '[Guadalupe](/villas/guadalupe)'
              - $285
              - $340
              - $405
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: buttons
        alignment: justify-center
        buttons:
          -
            link: 'https://secure.webrez.com/hotel/1183/calendar'
            button_text: 'BOOK NOW'
            open_in_new_tab: true
            button_classes: null
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
          -
            type: italic
        text: ' *** Check your dates via the Book Now Button '
      -
        type: text
        marks:
          -
            type: bold
        text: '***'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Rates in USD Per Night'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Plus 19% tax (16% VAT & 3% bed tax)'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '$50 per-person, per night for a 3rd or 4th person'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Cooked to Order Full Breakfast served daily from 8:30a to 10:30a included with your direct booking.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Please note: The QMC is an adult only property and not suitable for children under the age of 18.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Payments: '
      -
        type: text
        text: 'One night deposit due at time of booking. Full payment is due 30 days prior to arrival. '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Cancellation Policy:'
      -
        type: text
        text: "\_"
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: underline
        text: 'Summer Season'
      -
        type: text
        text: ': No fee for cancellations until 15 days prior to arrival.  No refund for cancellations within 14 days of arrival.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: underline
        text: 'Winter:'
      -
        type: text
        text: ' No fee for cancellations until 15 days prior to arrival.  No refund for cancellations within 14 days of arrival.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: underline
        text: 'Holiday Season'
      -
        type: text
        text: ': No Fee for cancellations until 30 days prior to arrival.  1 night charge for cancellations within 29 days prior to arrival.  '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Cancellation fees may very depending on the time of year, demand and reservation lead time. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'No credit or refund will be given for late arrivals or early departures. '
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'CASA TRES VIDAS – VILLA RATES'
  -
    type: set
    attrs:
      values:
        type: table
        table:
          -
            cells:
              - ''
              - '**\# OF ROOMS**'
              - '**SUMMER** (05/01 - 10/31)'
              - '**WINTER** (11/01 - 04/30)'
              - '**HOLIDAY** (12/20 - 01/04)'
          -
            cells:
              - '[Vida Alta](/villas/vida-alta)'
              - '3'
              - $845
              - '$1,045'
              - '$1,255'
          -
            cells:
              - '[Vida Alta & Sol](/villas/vida-alta-and-sol-at-ctv)'
              - '6'
              - '$1,590'
              - $1990
              - '$2,350'
          -
            cells:
              - '[Vida Sol](/villas/vida-sol)'
              - '3'
              - $745
              - $945
              - '$1,135'
          -
            cells:
              - '[Vida Sol & Mar](/villas/vida-sol-and-mar-at-ctv)'
              - '7'
              - '$1,720'
              - '$2,120'
              - '$2,570'
          -
            cells:
              - '[Vida Mar](/villas/vida-mar)'
              - '4'
              - $975
              - '$1,175'
              - '$1,375'
          -
            cells:
              - '[**All of Casa Tres Vidas**](/villas/casa-tres-vidas)'
              - '10'
              - '$2,565'
              - '$3,165'
              - '$3,765'
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: buttons
        alignment: justify-center
        buttons:
          -
            link: 'https://secure.webrez.com/hotel/1183/calendar'
            button_text: 'BOOK NOW'
            open_in_new_tab: true
            button_classes: null
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: '*** Check your dates via the Book Now Button ***'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Rates in USD Per Night'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Plus 19% tax (16% VAT & 3% bed tax)'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Special 1-2 bedroom rates are available. Please inquire regarding rates and availability.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Rates include the service of daily housekeeper'
              -
                type: hard_break
              -
                type: text
                marks:
                  -
                    type: underline
                text: 'Cost of food and beverages is not included'
              -
                type: text
                text: .
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Minimum Night Stay:\_3-4 night minimum Summer Season, 4 to 5 night minimum Winter Season, and 7 night minimum Holidays Season. (Minimum Stay may very depending on the time of year and booking patterns). "
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Vida Sol accommodates 10 people, Vida Mar accommodates 8 people and Vida Alta accommodates 6 people.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Villa Arrangement:'
      -
        type: text
        text: "\_"
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Vida Alta & Sol can be combined to accommodate 16 people. '
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Vida Sol & Mar can be combined to accommodate 18 people. '
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'All 3 villas can be combined to create your own private compound and can accommodate up to 24 people. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'If you require sleeping accommodations for more than 24 people, additional rooms are available at our adjoining sister property, Quinta Maria Cortez. To preview the luxurious suites and amenities, visit our '
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'https://villasinvallarta.com/villas/qmc'
              target: _blank
              rel: null
        text: Accommodations
      -
        type: text
        text: ' page. Please contact us directly for special rates when combining rooms at both properties.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Payments:'
      -
        type: text
        text: "\_Casa Tres Vidas requires a 25% deposit due at the time of booking. An additional 25% at 90 days prior to arrival (or 50% of total within 90 days of arrival). The balance is due 30 days prior to arrival."
  -
    type: paragraph
    content:
      -
        type: hard_break
      -
        type: text
        marks:
          -
            type: bold
        text: 'Cancellation/Refund Policy:'
      -
        type: text
        text: ' '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: underline
        text: 'Summer & Winter Seasons'
      -
        type: text
        text: ': No fee for cancellations more than 60 days prior to arrival. 50% cancellation fee for 30 to 60 days prior to arrival. Non-refundable for any cancellation within 30 days prior to arrival. '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: underline
        text: 'Holiday Season'
      -
        type: text
        text: ': No fee for cancellations more than 120 days prior to arrival. 25% cancellation fee for 120 to 61 Days prior. 50% cancellation fee for 60 to 31 days prior to arrival. Non-refundable for any cancellation 30 days prior to arrival.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Cancellation fees may very depending on the time of year, demand and reservation lead time. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'No credits or refunds are given for late arrivals or early departures. '
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1735240794
seo:
  description: 'Villa Rates.'
---
