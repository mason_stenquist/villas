---
id: 9aabe92a-0f9a-4992-bd22-f7277a761057
blueprint: page
title: 'Wedding Gallery'
parent: 1d183432-707d-461e-add2-4d8a30eddf08
template: pages/wedding
header_image: header-images/vallarta-weddings-0035.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1626646546
bard:
  -
    type: set
    attrs:
      values:
        type: gallery
        photos:
          - pages/vallarta-weddings-0013.jpg
          - pages/vallarta-weddings-0014.jpg
          - pages/vallarta-weddings-0015.jpg
          - pages/vallarta-weddings-0016.jpg
          - pages/vallarta-weddings-0017.jpg
          - pages/vallarta-weddings-0018.jpg
          - pages/vallarta-weddings-0019.jpg
          - pages/vallarta-weddings-0020.jpg
          - pages/vallarta-weddings-0021.jpg
          - pages/vallarta-weddings-0022.jpg
          - pages/vallarta-weddings-0023.jpg
          - pages/vallarta-weddings-0024.jpg
          - pages/vallarta-weddings-0025.jpg
          - pages/vallarta-weddings-0026.jpg
          - pages/vallarta-weddings-0027.jpg
          - pages/vallarta-weddings-0028.jpg
          - pages/vallarta-weddings-0029.jpg
          - pages/vallarta-weddings-0030.jpg
          - pages/vallarta-weddings-0031.jpg
          - pages/vallarta-weddings-0032.jpg
          - pages/vallarta-weddings-0033.jpg
          - pages/vallarta-weddings-0034.jpg
          - pages/vallarta-weddings-0035.jpg
          - pages/vallarta-weddings-0036.jpg
          - pages/vallarta-weddings-0037.jpg
          - pages/vallarta-weddings-0038.jpg
          - pages/vallarta-weddings-0039.jpg
          - pages/vallarta-weddings-0040.jpg
          - pages/vallarta-weddings-0041.jpg
          - pages/vallarta-weddings-0042.jpg
          - pages/vallarta-weddings-0043.jpg
          - pages/vallarta-weddings-0044.jpg
          - pages/vallarta-weddings-0045.jpg
          - pages/vallarta-weddings-0046.jpg
          - pages/vallarta-weddings-0047.jpg
          - pages/vallarta-weddings-0048.jpg
          - pages/vallarta-weddings-0049.jpg
          - pages/vallarta-weddings-0050.jpg
          - pages/vallarta-weddings-0051.jpg
          - pages/puerto-vallarta-wedding-01.jpg
          - pages/puerto-vallarta-wedding-02.jpg
          - pages/puerto-vallarta-wedding-03.jpg
          - pages/puerto-vallarta-wedding-04.jpg
          - pages/puerto-vallarta-wedding-05.jpg
          - pages/vallarta-weddings-0001.jpg
          - pages/vallarta-weddings-0002.jpg
          - pages/vallarta-weddings-0003.jpg
          - pages/vallarta-weddings-0004.jpg
          - pages/vallarta-weddings-0005.jpg
          - pages/vallarta-weddings-0006.jpg
          - pages/vallarta-weddings-0007.jpg
          - pages/vallarta-weddings-0008.jpg
          - pages/vallarta-weddings-0009.jpg
          - pages/vallarta-weddings-0010.jpg
          - pages/vallarta-weddings-0011.jpg
          - pages/vallarta-weddings-0012.jpg
  -
    type: paragraph
---
