---
id: 1d183432-707d-461e-add2-4d8a30eddf08
blueprint: page
title: Weddings
template: pages/wedding
parent: a1a93215-87c8-4247-bf1b-f1157f1b1b04
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Puerto Vallarta is the perfect tropical location for your dream wedding. This international destination was listed in "Brides Magazine" as a top 10 venue world-wide favorite. According to the 2010 issue of Destination Weddings & Honeymoon Magazine, the secluded beach at Quinta Maria Cortez and Casa Tres Vidas was the “best choice in Vallarta to say I do”.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We offer wedding packages utilizing the unique architecture and beachfront setting of Casa Tres Vidas and Quinta Maria Cortez providing you the perfect backdrop for your memorable day. We also have relationships with the top wedding planners in the area to make your whole event as personalized, as special and as effortless as possible.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'With overnight accommodations for up to 42 guests combined with the private, intimate setting to is able to provide visitors with a memorable and magical wedding experience for up to 75 people. We can also customize packages for smaller, intimate weddings or commitment ceremonies at Casa Tres Vidas; contact us for a personalized plan.'
header_image: header-images/vallarta-weddings-0035.jpg
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1735238852
---
