---
id: 53a89af7-91ab-4676-a477-d0b6381df8b2
blueprint: page
title: Recommendations
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642877550
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: EN
template: recommendations/index
seo:
  description: Recommendations
---
