---
id: fcd2ca16-093c-41fd-9af5-72da6c41bf59
published: false
blueprint: page
title: Staff
parent: 62136fa2-9e5c-4c38-a894-a2753f02f5ff
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1734049286
header_image: header-images/staff.jpg
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Your hosts Margaret Parrish and Jon Christopher Jones welcome you to their perfect beachfront escape in sunny and warm Puerto Vallarta, Mexico!'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Margaret, an army brat that has lived all over the world, first fell in love with Vallarta during her honeymoon in the mid 80s. After selling a thriving small café in Salt Lake City, she quickly became bored. Her dreams always brought her back to Vallarta, the place that she fell in love with the minute she arrived.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Jon was a friend of Margaret’s from Salt Lake City where he owned a successful travel agency – the Travel Zone. Ever since a spring break trip to Mexico in high school, Jon’s goal was to one day move there. Jon’s extensive background in hospitality as the Director of Coca-Cola’s Global Hospitality program for the 2002 Olympic Games and many other projects for the Olympic and Paralympic Games, was the perfect training ground as part owner of a pair of hotels.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Together, in 1997, they rescued a diamond in the rough on the perfect beach in Vallarta and Quinta Maria Cortez became a reality after months of renovations and improvements. Just three years later, the neighboring property became available, and Casa Tres Vidas joined the enclave that makes up Villas in Vallarta.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Margaret, Jon, Sara Gonzalez, their bilingual General Manager who joined the group in 2018 and the entire Villas in Vallarta team, look forward to welcoming you!'
template: staff/index
---
