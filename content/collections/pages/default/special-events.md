---
id: a1a93215-87c8-4247-bf1b-f1157f1b1b04
blueprint: page
title: 'Special Events'
template: pages/show
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1735237616
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Villas in Vallarta specializes in hosting special events. From weddings and corporate events to milestone birthdays and retirement celebrations, we will make your special event an occasion to remember. We have overnight accommodations for up to 42 guests and can host additional guests for your event. Please email us at reservations@villasinvallarta.com and we can start to plan your event.'
header_image: header-images/special-event.jpg
---
