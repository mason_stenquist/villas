---
id: ac646619-4dde-4a7f-b9c7-e292a24da8c7
blueprint: page
title: Directions
parent: 62136fa2-9e5c-4c38-a894-a2753f02f5ff
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642877137
template: pages/directions
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'After driving through downtown Puerto Vallarta, take the coast highway South (Mexico 200 toward Mismaloya) for approximately 1.5 miles to Conchas Chinas.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Watch for the Mini-Market “OXXO” and the Applegate Realty Building on the right.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'At the far left side of this building is a street named Calle Sagitario. Follow it down toward the beach/water and around (it doubles back past Casa Septiembre). Quinta Maria Cortez and Casa Tres Vidas will be located on the left, midway down the street, about five houses past Casa Septiembre, you will see the black and white striped awning of Quinta Maria Cortez.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'House number on Quinta Maria is #132 and House number on Casa Tres Vidas is #126. Keep in mind that Calle Sagitario is a circular street with two entrances/exits to the main road (Mexico 200) and that not all the house numbers are in order!'
  -
    type: set
    attrs:
      values:
        type: buttons
        alignment: justify-center
        buttons:
          -
            link: /assets/directions/QMC-CTV-Map-Dir-and-Phone-2012.pdf
            button_text: 'Click here to download directions PDF'
            button_classes: null
            open_in_new_tab: true
---
