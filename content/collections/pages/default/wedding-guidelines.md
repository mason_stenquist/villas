---
id: bfae1bc7-bd68-4ba0-a6c8-bf177f1f59da
blueprint: page
title: 'Wedding Guidelines'
parent: 1d183432-707d-461e-add2-4d8a30eddf08
template: pages/wedding
header_image: header-images/vallarta-weddings-0035.jpg
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1735239351
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: "Wedding reservations are different than our typical operations; normally we operate Quinta Maria Cortez “QMC” as a boutique Bed & Breakfast (were we include breakfast with the room rate, do not offer other bar or meal service and do not allow children).\_ Casa Tres Vidas “CTV” is normally operated in the traditional “Villa Concept” (renting the villas with kitchen staff and the guests pay for the food).\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "\_With a Wedding reservation, both properties are reserved and operated in a combined way, catering to your needs.\_ We offer complimentary buffet breakfast to all guests, we allow children at both properties and other food & beverage services are contracted in advance via our office in the United States."
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Accommodation Guidelines:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'A minimum rental period of 4 nights is required (1st of May thru the 20th of December) for both properties, Quinta Maria Cortez and Casa Tres Vidas, to secure your wedding room block.'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'During this period we can accommodate smaller weddings utilizing only Casa Tres Vidas, please call us for further details.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'A minimum rental period of 5 nights is required (6th of January thru the 30th of April) for both properties, Quinta Maria Cortez and Casa Tres Vidas, to secure your wedding room block.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Christmas / New Year Holiday is excluded from Wedding Bookings'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The following rates apply (Rate is total for 4 or 5 nights):'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Value Season Rate for 4 nights (1MAY24 to 31OCT24) - $21,230.USD'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'High Season Rate for 4 nights (1NOV24 to 20DEC24) - $25,537.USD'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'High Season Rate for 5 nights (06JAN24 to 30APR24) Saturday arrivals only - $31,922.USD'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'There are a total of 19 bedrooms with 25 beds and 1 queen-size hide-a-bed detailed as following for the 2 properties:'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'CTV – 3 villas (Vida Alta, Vida Sol & Vida Mar) with a total of 10 bedrooms'
                  -
                    type: bullet_list
                    content:
                      -
                        type: list_item
                        content:
                          -
                            type: paragraph
                            content:
                              -
                                type: text
                                text: '11 King-size beds and 1 queen-size bed plus one queen-size hide-a-bed.'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'QMC – 7 suites with a total of 9 bedrooms'
                  -
                    type: bullet_list
                    content:
                      -
                        type: list_item
                        content:
                          -
                            type: paragraph
                            content:
                              -
                                type: text
                                text: '5 King-size beds, 4 queen-size beds and 4 twin-size day beds.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The Maximum occupancy combining both properties is 42 guests.'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Any additional guests will be charged $25. per person/per night (including breakfast).'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Collecting funds from Guests:'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'This is a service that we offer, with the following items:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "You will need to provide us with a\_"
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'complete rooming'
              -
                type: text
                text: "\_list at least 120 days prior to arrival with\_"
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'full names,  e-mail addresses and phone numbers.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Individual credit cards will be processed 45-60 days prior to arrival for total due if only paying one payment. For most wedding parties we collect a one night deposit 2 to 6 months prior to the room block dates and final payment 45 days prior to arrival.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'One form of payment (credit card) per room, please.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Wedding Event Fee:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The Wedding Event Fee is $3,000 for up to 50 guests. This event fee covers the use of our property and beach for your Wedding Event. This fee is applicable for events Monday through Saturday. Sunday or Holiday events incur additional charges.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Additional Wedding Guests above 50 are $50 per person. Multiple Events will incur additional charges. The event fee also includes:'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'All event employees – bartenders, wait staff, chef & kitchen help'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Rental Equipment – tables, chairs, chair covers, linens, place settings'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'An additional fee of $25 per person will apply to an event that includes a full sit- down meal on the beach or roof-top terraces.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Deposits & Payments:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'For Accommodations (Entire Room Block) & Event Fee'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: '25% at time of booking for bookings made more than 9 months prior to arrival.'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: '25% - 9 months prior to arrival or 50% of the total.'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: '25% - 4 months prior to arrival or 75% of the total.'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Final payment 30 days prior to arrival.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Food & Beverage'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Food & Beverage costs are due 30 days prior to your event.'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Food & Beverage guarantees are due 14 days prior to your group arrival.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Cancellations:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Accommodations – Entire Room Block'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: '25% deposit is non-refundable.'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'A total of 50% of the accommodation fee is non-refundable for cancellations made 9 months to 120 days prior to arrival.'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'A total of 75% of the accommodation fee is non-refundable for cancellations made 120 days to 31 days prior to arrival.'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Bookings cancelled 30 days or less prior to arrival are totally non-refundable.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'If there is an unlikely/unforeseeable issue/event such as fire on the property, earthquake, hurricane in or around Puerto Vallarta that would prevent us from hosting your wedding event at our property, we would offer you a full refund for any unused room nights, Food & Beverage and Event Fee.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Wedding Co-coordinator:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'A professional Wedding Coordinator must be contracted to oversee the wedding details not offered by Villas in Vallarta. You are welcome to choose any local wedding planner.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We recommend:'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Rocio Quintal, +52 322 429 7498'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Site Inspections'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We offer a complimentary 3-night stay on deposited bookings if you would like to come preview our complete selection of rooms, public spaces and grounds.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'If you would like a site inspection prior to a deposit being made on your booking, we will charge for your stay and credit the charge toward your wedding reservation.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'On-site Options:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Beverage Stocking Service – for a flat $400 charge, we will stock the refrigerators prior to your arrival in the suites and villas. Drinks - 10 cases of a mix of soft drinks.  Choose from Coke, Diet Coke, and Sprite. 4 cases of beer. Choose from Modelo (can), Pacifico (bottle), and Corona (bottle). 4 cases of water.'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'CTV – will stock all villa fridges with 6 of each type of drink.'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'QMC – will stock each suite fridge with 2 of each type of drink'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Remaining drinks will be put in Vida Sol for all guests to access'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We offer a complimentary breakfast 6 days a week for up to 42 guests served for up to 1 hour each day (9:00am to 10:00am)'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'We do not offer Breakfast on Sundays'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Food & Beverage:'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We will supply you with a Food & Beverage “F&B” Banquet Event Order “BEO” well before your arrival. All meals done on property during your 4 or 5-day event must be ordered 30 days in advance and noted on the F&B BEO.'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Any additional items such as snacks (chips & salsas, etc.) and beverages which are requested on-site, must be done so 24 hours in advance. Our staff goes shopping each day at 11:00am for the next 24-hour period. Requests must be made by 10:30am for that afternoon and the following day. Requests must be made by 10:30am on Saturday for Saturday afternoon, Sunday and Monday morning. Most of the staff have Sunday off.\_These additional items ordered on-site are to be paid for in cash on-site."
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Miscellaneous:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Children — Children under 2 are free. Children under 18 are exempt from bar charges.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'On the day of your wedding, we request that you refrain from using the Vida Sol sala and pool to give our staff the opportunity to set up for the wedding reception and dinner.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Staff — Sunday is the staff’s day off. We do not offer breakfast or other meal service on Sunday. We also do not have maid service on Sundays.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Rain — As you know, we are a unique property and do not have a ballroom as back-up. In the case of rain it becomes more difficult to handle over 50 guests.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Music — We offer the use of our existing sound system at QMC & CTV (mid level in the adjoining Salas/living areas) consisting of a Computer with iTunes (about 5000 songs) and 2 Sony receivers (200 watt) and a 5 disk CD player with 8 to 10 speakers. IPod hook ups. Your local wedding planner also has many live options available to you.'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'If you plan to use an iPod, you need to assign a DJ or someone to handle the music.'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Please remember, we are in a residential area and cannot have loud music after 11:00pm'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Please respect our equipment by not playing music to loud with the potential to blow the speakers or sound system. And do not move our equipment with out prior approval from the manager.'
---
