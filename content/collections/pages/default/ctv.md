---
id: 86314aca-7793-4aa4-b988-c5826ce9f353
blueprint: villa_overview
title: 'Casa Tres Vidas'
parent: 3e96212a-fdc7-48e5-8dce-256811192ee3
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1735239538
template: villas/index
bard:
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe class="rounded-xl" width="100%" height="450" src="https://www.youtube.com/embed/HcVk4QTRuLw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Come discover an enclave of 3 beachfront villas located in the heart of Mexico’s Pacific Rivera, Casa Tres Vidas. Casa Tres Vidas (The House of 3 Lives) are 3-or 4-bedroom villas that can be rented alone or combined to create a 6, 7 or 10 bedroom compound located mere minutes south of downtown Puerto Vallarta, Mexico.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Imagine awakening in your private luxurious and finely appointed Mexican villa to the calming sounds of the ocean just outside your window. The only decision you need to make for the day is whether to go shopping, lay by your private pool, take a walk along the beach, or enjoy one of the many adventurous tours available to you. This is your vacation — spend the days as you desire. Casa Tres Vidas allows travelers to experience Mexico as they wish while enjoying privacy in peaceful and plush surroundings.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Casa Tres Vidas, featured in Architectural Digest, offers the flexibility for families and groups of multiple sizes in a luxurious setting making for a unique choice for your vacation getaway.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Don’t just take our word for it — our many guest raves say it all;'
  -
    type: blockquote
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: italic
            text: '“This was an unbelievably terrific and unmatched vacation experience…” Vancouver BC, Canada'
  -
    type: blockquote
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: italic
            text: '“From the moment we walked in the door, we were treated like royalty.” Cedar Rapids, IA, USA'
  -
    type: blockquote
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: italic
            text: '“I was overwhelmed at how wonderful our stay was. From the beautiful villa with breathtaking views of the ocean to the gourmet meals… and roof top deck… Casa'
          -
            type: text
            text: "\_"
          -
            type: text
            marks:
              -
                type: italic
            text: 'Tres Vidas is the place to stay with friends or family.” Seattle, WA, USA'
villa_type: ctv
---
TEST