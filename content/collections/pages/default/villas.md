---
id: 3e96212a-fdc7-48e5-8dce-256811192ee3
blueprint: villa_overview
title: Accommodations
template: villas/index
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1735239755
bard:
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe class="rounded-xl" width="100%" height="450" src="https://www.youtube.com/embed/HcVk4QTRuLw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Villas in Vallarta'
      -
        type: text
        text: "\_(VinV) is a beautifully appointed boutique hotel located on the beach at the South end of Puerto Vallarta. VinV offers luxurious accommodations ranging from one-bedroom suites to three and four-bedroom villas that can be configured to our guests specific requirements. Our unique property is comprised of two connected buildings:\_"
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quinta Maria Cortez “QMC”'
      -
        type: text
        text: "\_and\_"
      -
        type: text
        marks:
          -
            type: bold
        text: 'Casa Tres Vidas “CTV”'
      -
        type: text
        text: ".\_ QMC Bed & Breakfast is comprised of seven large 1 & 2 bedroom open-air suites (with kitchenettes) on eight different levels. QMC is perfect for romantic get-a-ways, honeymoons, or simply time away from home. A delicious cooked to order breakfast is served seven days a week in the ocean front palapa.\_"
      -
        type: text
        marks:
          -
            type: bold
        text: 'Casa Tres Vidas “CTV”'
      -
        type: text
        text: "\_offers three beachfront villas (3 & 4 bedrooms each) that can be combined to create 6, 7 and even 10 bedroom villas for friends, co-workers or families.\_ Each villa has its own living room, dining room, private pool and heated Jacuzzi. Fully staffed with a butler and maid included in the rate. For large groups and functions, QMC and CTV can be combined to comfortably host up to 75 guests for special occasions such as weddings, anniversaries and milestone birthdays. Our on-site room accommodations can be configured to lodge up to 42 guests at a time."
---
Test