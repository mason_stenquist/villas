---
title: 'About Us'
template: pages/directions
bard:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Quinta Maria Cortez & Casa Tres Vidas'
      -
        type: text
        text: " is a beautifully appointed boutique hotel located directly on the beach at the South end of Puerto Vallarta. We offer luxurious accommodations ranging from one-bedroom suites to three and four-bedroom villas that can be configured to conform to our guests specific requirements. Our unique property is comprised of two connected buildings:\_"
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'http://villasinvallarta.com/room-list/qmc/'
              target: null
              rel: null
          -
            type: bold
        text: 'Quinta Maria Cortez “QMC”'
      -
        type: text
        text: "\_and\_"
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'http://villasinvallarta.com/room-list/ctv/'
              target: null
              rel: null
          -
            type: bold
        text: 'Casa Tres Vidas “CTV”'
      -
        type: text
        text: ". QMC offers seven large 1 & 2 bedroom open-air suites (with kitchenettes) on eight different levels and is best utilized for romantic get-a-ways, honeymoons, etc. A delicious cooked to order breakfast in a palapa covered ocean front dining room is available six days a week and is included in the rate.\_"
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'http://villasinvallarta.com/room-list/ctv/'
              target: null
              rel: null
          -
            type: bold
        text: 'Casa Tres Vidas “CTV”'
      -
        type: text
        text: "\_offers three beachfront villas (3 & 4 bedrooms each) that can be combined to create 6, 7 and even 10 bedroom villas that are great for groups of friends, co-workers or families. Each villa has its own living room, dining room, private pool and heated Jacuzzi, located on either two or three levels and comes fully staffed with a gourmet chef, butler and maid. Your private chef is included in the rate and will prepare two meals per day for your group, charging only for the actual food costs. For large groups and functions, QMC and CTV can be combined to comfortably host up to 80 guests for special occasions such as weddings, anniversaries and milestone birthdays. Our on-site room accommodations can be configured to lodge up to 44 guests at a time."
  -
    type: paragraph
    content:
      -
        type: text
        text: "We have equipped our villas/suites with all the amenities that will make your stay with us as pleasant as possible: direct dial phones that let you stay in touch with the outside world, ceiling fans in all rooms, adjustable air conditioning in most suites, big fluffy towels for a fun day on the beach and imported soaps and shampoos. Comfortable cotton bathrobes and electric hairdryers help you get ready for dinner in style; and in-room safes protect your passport and crown jewels. (By the way, the electricity in Mexico is 120 volts, which means North American laptops and electric razors work just fine.) \_Free WI-FI and internet is available throughout the Villa."
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1612475205
id: 62136fa2-9e5c-4c38-a894-a2753f02f5ff
---
I'm just a kid living in the 90's, writing articles in his secret public journal wonder if someday, somewhere in the future, they will be read by someone.