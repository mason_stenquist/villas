---
id: b780e193-7262-49bd-a9bc-786c0be62534
title: 'Vida Alta 121'
room_description: 'This grand master bedroom in Vida Alta is air conditioned and it was recently remodeled. It features a  king-size bed, TV in the sitting area and a generous en suite bathroom with two sinks, shower and a claw feet tub. There is a private terrace with spectacular views of the ocean and surrounding jungle.'
room_photos:
  - villas/2H5A2827.jpg
  - villas/ctv/alta/121/2H5A2090.jpg
  - villas/2H5A8693.jpg
  - villas/2H5A8699_701_Int2.jpg
  - villas/2H5A8673_6_F-Int.jpg
  - villas/2H5A8685.jpg
  - villas/2H5A2835_7_Int2.jpg
  - villas/ctv/alta/121/IMG-9066-ABR20nov14-h3.jpg
  - villas/ctv/alta/121/IMG-9070-ABR20nov14-h3.jpg
  - villas/ctv/alta/121/IMG-9075-ABR20nov14-h3.jpg
  - villas/2H5A2853.jpg
  - villas/2H5A2859_60_Int2.jpg
  - villas/ctv/alta/121/IMG-9109-ABR20nov14-h3.jpg
  - villas/ctv/alta/121/IMG-9127-ABR20nov14-h2.jpg
  - villas/2H5A8703.jpg
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1701367992
---
