---
id: df7188bf-7119-48d0-97b9-265745a128ca
title: 'Vida Sol 136'
room_description: 'This air-conditioned bedroom, on the same level as #135, also has two king-sized beds, a large private bath with double vanities, and a balcony with ocean views that connects with the balcony to room #135.  It also has a mini-fridge and coffee maker.'
room_photos:
  - villas/2H5A8848.jpg
  - villas/2H5A8853.jpg
  - villas/2H5A8843.jpg
  - villas/ctv/sol/136/IMG_3273.jpg
  - villas/ctv/sol/136/IMG_3280.jpg
  - villas/2H5A8834.jpg
  - villas/2H5A8837.jpg
  - villas/2H5A8841.jpg
  - villas/2H5A8861.jpg
  - villas/ctv/sol/136/IMG_3252.jpg
  - villas/2H5A8803_4_Int2.jpg
  - villas/ctv/sol/136/IMG_3249.jpg
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1701369148
---
