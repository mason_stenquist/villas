---
id: 02ec060c-2736-4d45-8e1f-53551abce77d
title: 'Vida Sol 135'
room_description: 'This bedroom, located one level below the living area, offers two king-sized beds and a private bath. It also features air conditioning, a balcony to enjoy ocean views and a private side entrance down several levels to the beach.  It also has a mini-fridge and coffee maker.'
room_photos:
  - villas/2H5A8769.jpg
  - villas/ctv/sol/135/IMG_3226.jpg
  - villas/2H5A8792_4_Int2.jpg
  - villas/2H5A8748_53_55_Int2.jpg
  - villas/2H5A8757_9_Int2.jpg
  - villas/2H5A8766.jpg
  - villas/2H5A8777.jpg
  - villas/ctv/sol/135/IMG_3220.jpg
  - villas/ctv/sol/135/IMG_3240.jpg
  - villas/2H5A8782.jpg
  - villas/2H5A8785.jpg
  - villas/2H5A8789.jpg
  - villas/ctv/sol/135/IMG_3194.jpg
  - villas/ctv/sol/135/IMG_3196.jpg
  - villas/2H5A8865_8_Int2.jpg
  - villas/2H5A8820.jpg
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1701368880
---
