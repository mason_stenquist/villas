---
id: 73237ba5-92d4-4802-9db1-7fce7d7811fb
title: 'Vida Alta 122'
room_description: 'This bedroom, on the same level as room #121, offers a king-size bed, air-conditioning and newly remodeled bathroom with double-sinks and a large shower. It also features a large private terrace with views of the bay and north towards the lights of Puerto Vallarta.'
room_photos:
  - villas/2H5A8726.jpg
  - villas/2H5A8724.jpg
  - villas/2H5A8732.jpg
  - villas/2H5A2812_9_Int2.jpg
  - villas/2H5A8714_8_Int2.jpg
  - villas/ctv/alta/122/IMG-9040-ABR20nov14-h3.jpg
  - villas/ctv/alta/122/IMG-9045-ABR20nov14-h3.jpg
  - villas/ctv/alta/122/IMG-9054-ABR20nov14-h2.jpg
  - villas/ctv/alta/122/IMG-9056-ABR20nov14-h2.jpg
  - villas/2H5A8737_8_Int2.jpg
  - villas/2H5A8741.jpg
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1701368121
---
