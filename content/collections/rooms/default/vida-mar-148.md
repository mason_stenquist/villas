---
id: 49c4c59b-8bfd-4827-bada-6cae418b75f1
title: 'Vida Mar 148'
room_description: 'This bedroom, next to room #147, is similar in design with a king-sized bed, sitting area and private bath. This air-conditioned bedroom shares the same garden patio with room #147, offering peaceful views of the ocean.'
room_photos:
  - villas/ctv/mar/148/IMG_3386.jpg
  - villas/ctv/mar/148/IMG_3390.jpg
  - villas/ctv/mar/148/IMG_3395.jpg
  - villas/ctv/mar/148/IMG_3398.jpg
  - villas/ctv/mar/148/IMG_3399.jpg
  - villas/ctv/mar/148/IMG_3410.jpg
  - villas/2H5A2721.jpg
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1676474654
---
