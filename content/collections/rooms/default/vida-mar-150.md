---
id: 71380529-85d9-4b6c-966d-2684d1444b82
title: 'Vida Mar 150'
room_description: 'This air-conditioned bedroom with direct access to the swimming pool and Jacuzzi, is on the lower beach level of the villa. This room features a queen-sized bed and a sitting area. The spacious bathroom can be entered from the bedroom or from the kitchen/living area making it useful as a powder room for the Villa.'
room_photos:
  - villas/2H5A2734.jpg
  - villas/ctv/mar/150/2H5A6650-ATR14nov18-e2-ctvpano5.jpg
  - villas/ctv/mar/150/2H5A6650.jpg
  - villas/ctv/mar/150/IMG_5141.jpg
  - villas/ctv/mar/150/2H5A6646.jpg
  - villas/ctv/mar/150/IMG_5151.jpg
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1676474546
---
