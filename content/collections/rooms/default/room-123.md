---
title: 'Vida Alta 123'
room_description: 'This air-conditioned bedroom on the same level as the living, dining, kitchen and Jacuzzi area, is set behind the living area and features a king-size bed and private bathroom. Shuttered windows open for an ocean view thru the living area or can be closed for privacy.'
room_photos:
  - villas/ctv/alta/123/2H5A2095.jpg
  - villas/ctv/alta/123/2H5A2102.jpg
  - villas/ctv/alta/123/2H5A2106.jpg
  - villas/ctv/alta/123/IMG-6828-ABR21novt-h7.jpg
  - villas/ctv/alta/123/IMG-6835-ABR21novt-h13.jpg
  - villas/ctv/alta/123/IMG-6850-ABR21novt-h15.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1613926243
id: d2a73c93-7f01-4b79-a9dd-4821d2e29d11
---
