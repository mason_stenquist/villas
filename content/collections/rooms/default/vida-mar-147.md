---
id: bb827caf-dd2a-4c6c-939b-c87a77ab271e
title: 'Vida Mar 147'
room_description: 'This air-conditioned bedroom offers a private bath, a king- sized bed along with a single (Twin) Day bed. Sitting area with sofa, chairs and desk. The garden patio directly off this bedroom offers wonderful views of the ocean.  The Bathroom has been recently been updated.'
room_photos:
  - villas/ctv/mar/147/2H5A6638.jpg
  - villas/ctv/mar/147/2H5A6639.jpg
  - villas/ctv/mar/147/IMG_3328.jpg
  - villas/ctv/mar/147/IMG_3335.jpg
  - villas/ctv/mar/147/IMG_3346.jpg
  - villas/ctv/mar/147/IMG_3352.jpg
  - villas/ctv/mar/147/IMG_3367.jpg
  - villas/ctv/mar/147/IMG_3370.jpg
  - villas/ctv/mar/147/IMG_3377.jpg
  - villas/ctv/mar/147/IMG_3382.jpg
  - villas/2H5A2723.jpg
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1676474610
---
