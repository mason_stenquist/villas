---
title: 'Vida Mar 149'
room_description: 'The Master Bedroom of Vida Mar features a large private terrace directly on the water. The air-conditioned room offers a king-sized bed with ocean view along with a sitting area and private bath.'
room_photos:
  - villas/ctv/mar/149/CTV-M-149-4245a.jpg
  - villas/ctv/mar/149/CTV-M-149-Ent-5265a.jpg
  - villas/ctv/mar/149/CTV-M-149-Tub-5278a.jpg
  - villas/ctv/mar/149/IMG_5186.jpg
  - villas/ctv/mar/149/IMG_6574.jpg
  - villas/ctv/mar/149/IMG_6601.jpg
  - villas/ctv/mar/149/IMG_6594.jpg
  - villas/ctv/mar/149/IMG_6583.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1613926342
id: a5d1b547-0236-46ca-aed2-899b01479661
---
