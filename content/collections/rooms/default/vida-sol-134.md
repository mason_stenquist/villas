---
title: 'Vida Sol 134'
room_description: 'Tucked behind the Vida Sol living area, this air-conditioned bedroom includes a king-sized bed, private bath and television. It is the perfect choice for those that want to minimize stairs and be in the heart of the Villa.'
room_photos:
  - villas/ctv/sol/134/2H5A2211.jpg
  - villas/ctv/sol/134/2H5A2215.jpg
  - villas/ctv/sol/134/2H5A6569.jpg
  - villas/ctv/sol/134/2H5A6575.jpg
  - villas/ctv/sol/134/2H5A6577.jpg
  - villas/ctv/sol/134/2H5A6582.jpg
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1613926423
id: 2e329d58-8fc7-4ee7-ac50-f17ef9ce6920
---
