---
room_description: 'Escondida detrás de la sala de estar de Vida Sol, esta habitación con aire acondicionado incluye una cama King, baño privado y televisión. Es la elección perfecta para aquellos que quieren minimizar las escaleras y estar en el corazón de la Villa.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1616513699
id: 8d372915-80e6-4e1a-b16d-b750926bdde8
origin: 2e329d58-8fc7-4ee7-ac50-f17ef9ce6920
---
