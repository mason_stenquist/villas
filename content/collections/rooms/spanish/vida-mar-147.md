---
room_description: 'Esta habitación con aire acondicionado ofrece un baño privado, una cama king-size y un sofá cama individual (Twin). Área de descanso con sofá, sillas y escritorio. El patio del jardín esta directamente afuera de esta habitación y ofrece maravillosas vistas del océano. El baño ha sido remodelado recientemente.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1616513651
id: 7e7e186f-fcae-402f-a8a9-c4a5ad3ce015
origin: bb827caf-dd2a-4c6c-939b-c87a77ab271e
---
