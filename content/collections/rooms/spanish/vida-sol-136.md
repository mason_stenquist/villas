---
room_description: 'Esta habitación con aire acondicionado, en el mismo nivel que la #135, también tiene dos camas tamaño king, un gran baño privado con tocadores dobles y un balcón con vista al mar que conecta con el balcón a la habitación # 135. También tiene un pequeño refrigerador y cafetera.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1616513717
id: 29e082b4-832a-4c99-854c-150d2dc2d8e5
origin: df7188bf-7119-48d0-97b9-265745a128ca
---
