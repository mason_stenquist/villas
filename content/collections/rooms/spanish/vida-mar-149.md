---
room_description: 'El dormitorio principal de Vida Mar cuenta con una gran terraza privada directamente sobre el agua. La habitación con aire acondicionado ofrece una cama king-size con vista al mar junto con una sala de estar y baño privado.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1616513676
id: 34589ad6-54f7-4ac0-bd07-ed61b8d88316
origin: a5d1b547-0236-46ca-aed2-899b01479661
---
