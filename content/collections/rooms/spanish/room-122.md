---
room_description: 'Esta habitacion, en el mismo nivel que la habitación # 121, ofrece una cama king-size, aire acondicionado y un baño recién remodelado con dos lavabos y una gran ducha. También cuenta con una gran terraza privada con vistas a la bahía y al norte hacia las luces de Puerto Vallarta.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1616513626
id: 2986e185-15e5-4afd-ab30-ba7fd545be31
origin: 73237ba5-92d4-4802-9db1-7fce7d7811fb
---
