---
room_description: 'Esta habitación, ubicada un nivel abajo de la sala de estar, ofrece dos camas tamaño king y un baño privado. También cuenta con aire acondicionado, un balcón para disfrutar de la vista al mar y una entrada lateral privada, varios niveles arriba de la playa. También tiene un pequeño refrigerador y cafetera.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1616513708
id: b31b3d04-ee15-416f-ba00-f99894e82d4a
origin: 02ec060c-2736-4d45-8e1f-53551abce77d
---
