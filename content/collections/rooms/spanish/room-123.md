---
room_description: 'Esta habitación con aire acondicionado en el mismo nivel que la sala, el comedor, la cocina y el jacuzzi, se encuentra detrás de la sala de estar y cuenta con una cama king-size y baño privado. Las ventanas con contraventanas se abren para tener vista al mar a través de la sala de estar o se pueden cerrar para mayor privacidad.'
updated_by: ecbd048d-0f27-4739-adeb-944bce5e5579
updated_at: 1616513640
id: a9b55bfd-3d9b-4b1a-9ff4-9591d058db30
origin: d2a73c93-7f01-4b79-a9dd-4821d2e29d11
---
