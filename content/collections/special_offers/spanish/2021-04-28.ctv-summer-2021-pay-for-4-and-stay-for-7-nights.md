---
id: bdaa5712-5fde-4b07-afb6-608293294590
origin: 05964edf-4378-4982-bf1e-54cb8d5d621b
published: false
title: 'CTV - Verano 2024 - Pague 4 y Hospedese 7 Noches'
short_description: |-
  Pague 4 y quédese 7 noches con llegada/salida en sábado. 

  Válido del 1.JUN.2024 al 28.SEP.2024 en Casa Tres Vidas.
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Pague 4 y quédese 7 noches con llegada/salida en sábado. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Válido del 1.JUN.2024 al 28.SEP.2024 en Casa Tres Vidas.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1692720217
---
