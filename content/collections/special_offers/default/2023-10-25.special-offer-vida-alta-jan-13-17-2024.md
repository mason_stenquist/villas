---
id: e8fef28c-afaf-4420-8c3e-0cda2af149bb
published: false
title: 'SPECIAL OFFER: Vida Alta Jan 13-17 or Mar 23-28, 2024'
discount_amount: '35% discount!'
villa_types:
  - ctv
short_description: 'Enjoy a 4 or 5 night stay! 4 nights starting Saturday January 13, 2024, or 5 nights starting March 23, 2024,  in our 3 bedroom villa Vida Alta. Enjoy the rooftop terrace and pool, spectacular views of the Pacific and 35% off our standard rates during the high season.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Enjoy a 4 or 5 night stay! 4 nights starting Saturday January 13, 2024, or 5 nights starting March 23, 2024,  in our 3 bedroom villa Vida Alta. Enjoy the rooftop terrace and pool, spectacular views of the Pacific and 35% off our standard rates during the high season.'
main_photo: rooms/0411.jpg
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1698255157
---
