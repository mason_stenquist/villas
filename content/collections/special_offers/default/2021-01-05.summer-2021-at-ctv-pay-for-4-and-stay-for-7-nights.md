---
id: e5b51fa9-70a6-4cf9-8cc8-3d165aa00842
published: false
title: 'Summer 2023 at CTV: Pay for 4 & Stay for 7 Nights'
discount_amount: '3 FREE nights'
villa_types:
  - ctv
short_description: |-
  Summer 2023 Special – Pay for 4 nights & Stay for 7 Nights – 3 Nights free at Casa Tres Vidas. Valid for Saturday arrival/departures 27.MAY.2023 thru 30-SEP-2023.

  *Book now – This deal won’t last!
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Summer 2023 Special – Pay for 4 nights & Stay for 7 Nights – 3 Nights free at Casa Tres Vidas. Valid for Saturday arrival/departures 27.MAY.2023 thru 30-SEP-2023.'
  -
    type: paragraph
    content:
      -
        type: text
        text: '*Book now – This deal won’t last!'
main_photo: special-offers/pay-for-4-stay-for-7.jpg
deal_link: 'https://secure.webrez.com/hotel/1183?location_id=289'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1692711403
---
