---
id: 05964edf-4378-4982-bf1e-54cb8d5d621b
published: false
title: 'CTV - Summer 2025 - Pay for 4 & Stay for 7 Nights'
discount_amount: '3 Nights Free'
villa_types:
  - ctv
short_description: 'Pay for 4 & Stay for 7 Nights with a Saturday Arrival/Departure.   3 Night Free! Valid 31.MAY.2025 thru 27.SEP.2025 at Casa Tres Vidas.'
main_photo: villas/ctv/alta/IMG-9143-ABR20nov14-h3.jpg
deal_link: 'https://secure.webrez.com/hotel/1183/?package_id=150680'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1732301930
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Pay for 4 & Stay for 7 Nights with a Saturday Arrival/Departure.   3 Night Free! Valid 31.MAY.2025 thru 27.SEP.2025 at Casa Tres Vidas.'
---
