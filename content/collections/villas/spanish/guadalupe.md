---
id: 4557d622-1d32-4c3b-9cc8-466d1a097655
origin: e425ab4b-3c93-426a-9a6e-7b9f43e332d6
short_description: 'Esta elegante habitación está ubicada en el area más privilegiada justo frente al mar. Este espacio de dos niveles con cama tamaño king, sofá cama individual, sala, baño y cocineta es una gran habitación abierta con áreas definidas por múltiples niveles. Guadalupe es ideal para aquellos que desean pasar un gran cantidad de tiempo en la playa, ubicada justo afuera de la puerta de su habitacion y para aquellos que disfrutan de estar directamente en el agua con todas las vistas y sonidos de las olas justo afuera de su habitación.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Esta elegante habitación está ubicada en el area más privilegiada justo frente al mar. Este espacio de dos niveles con cama tamaño king, sofá cama individual, sala, baño y cocineta es una gran habitación abierta con áreas definidas por múltiples niveles. Guadalupe es ideal para aquellos que desean pasar un gran cantidad de tiempo en la playa, ubicada justo afuera de la puerta de su habitacion y para aquellos que disfrutan de estar directamente en el agua con todas las vistas y sonidos de las olas justo afuera de su habitación.'
seo:
  title: 'Guadalupe (ES)'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642877862
---
