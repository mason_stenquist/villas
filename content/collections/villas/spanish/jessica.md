---
id: 06dd779c-bbbe-4643-820c-c156ceea7100
origin: 94d994ae-10c7-43d3-bd01-1b4ac737682b
short_description: 'Jessica cuenta con su propia entrada privada a solo unos pasos de la entrada principal. Hay una cama tamaño queen en un tapanco sobre la cocina, cuenta con un sofá cama y una sala de estar debajo. La bañera / regadera hundida se encuentra junto a la sala de estar. Aunque Jessica ofrece una limitada vista del océano, es posible disfrutar del sonido de las olas.'
seo:
  title: 'Jessica (ES)'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642878178
---
