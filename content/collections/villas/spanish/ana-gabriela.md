---
id: 6f2030f6-d0d1-4f83-adc0-1a11bb516ad0
origin: a0a96206-3a95-4978-8f33-6cb6249ad37c
short_description: 'Las paredes de color rojo oscuro y una cama con dosel de tamaño king hacen de "Gabby" una escapada romántica perfecta junto al mar. Los azulejos negros antiguos resaltan la bañera / regadera hundidas y el baño contiguo con bidé. La sala de estar en el amplio balcón es un lugar ideal para relajarse y ver las olas. Gabby cuenta con un pequeño refrigerador y una estación de café.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Las paredes de color rojo oscuro y una cama con dosel de tamaño king hacen de "Gabby" una escapada romántica perfecta junto al mar. Los azulejos negros antiguos resaltan la bañera / regadera hundidas y el baño contiguo con bidé. La sala de estar en el amplio balcón es un lugar ideal para relajarse y ver las olas. Gabby cuenta con un pequeño refrigerador y una estación de café.'
seo:
  title: 'Ana Gabriela (ES)'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642878260
---
