---
id: 728ed926-13aa-4584-87bb-adaa88c3fc3e
origin: 5da81190-520f-4c7a-9d72-afb434567e1b
short_description: 'Con sus imponentes 18 pies de altura eta espaciosa sala de estar abiertay con una cúpula alta, Vida Sol refleja el encanto del México colonial tradicional y combina el lujoso entorno de la villa de 2 niveles y 3 habitaciones con la espectacular vista del Pacífico. Observe juguetones delfines y ballenas desde su terraza privada mientras disfruta de su café matutino o de ese cóctel perfecto al atardecer. Vida Sol ofrece una espectacular piscina privada y jacuzzi con calefaccion en un exuberante jardín tropical.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Con sus imponentes 18 pies de altura eta espaciosa sala de estar abiertay con una cúpula alta, Vida Sol refleja el encanto del México colonial tradicional y combina el lujoso entorno de la villa de 2 niveles y 3 habitaciones con la espectacular vista del Pacífico. Observe juguetones delfines y ballenas desde su terraza privada mientras disfruta de su café matutino o de ese cóctel perfecto al atardecer. Vida Sol ofrece una espectacular piscina privada y jacuzzi con calefaccion en un exuberante jardín tropical.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vida Sol se puede combinar con Vida Alta para tener 6 habitaciones o con Vida Mar para tener 7 habitaciones o con ambas Vida Mar y Vida Alta para un total de 10 habitaciones, alojando hasta 24 huespedes.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe width="1255" height="706" src="https://www.youtube.com/embed/PMmiuoyW5n4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
seo:
  title: 'Vida Sol (ES)'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642876223
---
