---
id: 70c4d412-8315-4cc7-a2c8-f6bf4bc0b88e
origin: fdec8603-40e1-429a-b7e1-50d8cb15afa4
short_description: 'Esta hermosa villa frente al mar está situada en Playa Conchas Chinas, una playa tranquila acentuada por palmeras que se balancean y que ofrece algunas de las mejores vistas de Puerto Vallarta. Esta villa al aire libre de 8 niveles con 10 habitaciones cuenta con una cúpula arquitectónica de 18 pies de altura que otorga el encanto del México colonial tradicional y ofrece una vista espectacular desde la villa. Casa Tres Vidas cuenta con 3 piscinas y 3 jacuzzis climatizados junto con 3 espaciosas áreas de estar. Ofrece la oportunidad de disfrutar de una cena o función grupal y además tener privacidad o un momento de tranquilidad. La villa cuenta con un encargado de la villa que habla inglés, servicio de limpieza diario, seguridad nocturna y un chef privado para preparar dos comidas al día, generalmente desayuno y cena.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Don’t just take our word for it — our many guest raves say it all;'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Imagínese despertar en su lujosa villa mexicana privada y finamente decorada con los relajantes sonidos del océano justo afuera de su ventana.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Después de disfrutar de un desayuno preparado a su gusto, la única decisión que debe tomar durante el día es ir de compras, descansar en su piscina privada, dar un paseo por la playa o disfrutar de uno de las muchas excursiones de aventura disponibles para usted. Estas son sus vacaciones: pase los días como desee. Casa Tres Vidas permite a los viajeros experimentar México como deseen mientras disfrutan de la privacidad en un entorno tranquilo y lujoso.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Casa Tres Vidas, que aparece en Architectural Digest, ofrece la flexibilidad para familias y grupos de varios tamaños en un entorno lujoso, lo que lo convierte en una opción única para su escapada de vacaciones.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'No se fíe sólo de nuestra palabra: los numerosos elogios de nuestros huespedes lo dicen todo;'
  -
    type: blockquote
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: '“Esta fue una experiencia de vacaciones increíblemente fabulosa e inigualable…” Vancouver BC, Canada'
  -
    type: blockquote
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: '“Desde el momento en que entramos por la puerta, nos trataron como a la realeza.” Cedar Rapids, IA, USA'
  -
    type: blockquote
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: '“Me sentí abrumado por lo maravillosa que fue nuestra estadia. Desde la hermosa villa con impresionantes vistas del océano hasta las comidas gourmet ... y la terraza en la azotea ... Casa Tres Vidas es el lugar para quedarse con amigos o familiares.” Seattle, WA, USA'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Informacion de la Villa:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Frente al mar en Playa Conchas Chinas'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '10 habitaciones con aire acondicionado con 11 camas king y 1 queen y baños privados - capacidad para 24'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '2 dormitorios tienen 2 camas king cada uno'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '3 salas de estar'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '3 comedores'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '3 cocinas'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'El personal incluye el administrador de la casa, el ama de llaves, el chef de la villa (prepara dos comidas al día)'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '3 piscinas y 3 jacuzzis climatizados (nivel de playa y nivel superior)'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Acceso a la playa privada - Mobiliario de playa'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'TV por cable / DVD / Sistema de sonido'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Acceso a computadora / Internet y acceso a línea DSL'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Casa Tres Vidas es el lugar perfecto para albergar su próxima celebración especial. Desde bodas, reuniones familiares o cumpleaños especiales, nuestra arquitectura única y nuestro entorno frente al mar le brindan el escenario perfecto para un día memorable. En nuestro enclave de 3 villas, Casa Tres Vidas puede ofrecer alojamiento durante la noche para hasta 24 personas. Combinado con nuestra propiedad hermana de al lado, el hotel boutique frente a la playa Quinta Maria Cortez, podemos recibir a 18 invitados adicionales, lo que hace que Casa Tres Vidas y Quinta Maria Cortez sean el lugar perfecto para alojar (hasta 42 invitados) y organizar su celebración especial de 20 a 80 personas. Si su grupo necesita alojamiento para por noche para más de 42 personas, podemos ofrecerle consejos y ayudarlos a acomodarlos en hoteles y villas cercanos con los que tenemos buena relacion.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe width="1255" height="706" src="https://www.youtube.com/embed/Gq5y3_yaaFg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
  -
    type: paragraph
seo:
  title: 'Casa Tres Vidas (ES)'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642878210
---
