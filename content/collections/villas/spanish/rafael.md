---
id: 2d1117f8-f6bd-4ca8-a313-8cfbb77391b7
origin: 53385dc5-20d4-422d-b7aa-dcb0a92ec7e0
short_description: 'Rafael, nuestra habitación de dos niveles, cuenta con una cama tamaño queen en el nivel de entrada y otra en el nivel inferior de la habitación. En el nivel de enmedio hay un llamativo baño de azulejos blancos y negros con regadera / bañera hundida. Justo afuera de la entrada de Rafael hay una terraza privada con vista a la alberca y al Océano Pacífico. Los cómodos sillones en la terraza son un gran lugar para terminar su novela favorita o para continuar soñando.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Rafael, nuestra habitación de dos niveles, cuenta con una cama tamaño queen en el nivel de entrada y otra en el nivel inferior de la habitación. En el nivel de enmedio hay un llamativo baño de azulejos blancos y negros con regadera / bañera hundida. Justo afuera de la entrada de Rafael hay una terraza privada con vista a la alberca y al Océano Pacífico. Los cómodos sillones en la terraza son un gran lugar para terminar su novela favorita o para continuar soñando.'
seo:
  title: 'Rafael (ES)'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642877669
---
