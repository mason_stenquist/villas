---
id: 1463ed89-df01-42e7-8e0c-dd16f9fe3241
origin: f6047bc0-c8c2-45a4-8098-08d9f22bfe7a
title: 'Vida Sol & Mar en CTV'
short_description: |-
  Villa de playa con 7 dormitorios en Playa Conchas Chinas, la combinación de Vida Mar y Sol cae en cascada hasta la playa en 4 niveles con 2 piscinas privadas y 2 jacuzzis climatizados con Wi-Fi a través de la propiedad.

  Las villas Vida Mar & Sol están ubicadas en una playa tranquila acentuada por palmeras, arena cálida y oleaje suave. Estas villas al aire libre de 4 niveles fusionan el confort moderno con el encanto colonial mexicano.
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vida Sol con sus imponentes 18 pies, donde la sala de estar y el comedor se ubican al aire libre bajo una cúpula alta es donde sus chefs privados prepararán 2 comidas al día para su grupo (6 días a la semana). Vida Mar & Sol reflejan el encanto del México colonial tradicional y combinan el lujoso entorno de la villa combinada de 4 niveles con la espectacular vista del Pacífico. Observe juguetones delfines y ballenas desde su terraza privada mientras disfruta de su café matutino o de ese cóctel perfecto al atardecer.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Lo más destacado de Vida Mar & Vida Sol @ Casa Tres Vidas:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Frente a la playa en Playa Conchas Chinas, a 10 minutos a pie de la playa del centro de Puerto Vallarta'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '7 habitaciones con aire acondicionado, 7.5 baños'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '(8 camas king, 1 cama queen y 1 cama individual)'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '2 salas de estar'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '2 Comedores y 1 comedor en la terraza'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '2 Cocinas'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Terraza en la playa y terraza frente al mar en Vida Sol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '2 albercas privadas'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '2 Jacuzzis climatizados'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Terraza solárium'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Wi-Fi gratis'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Televisores en el área común y 4 de las 7 habitaciones'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Teléfono de marcación directa'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Acceso a playa privada'
  -
    type: paragraph
    content:
      -
        type: text
        text: '   * Mobiliario de playa (Sillas, Mesas, Camastros y Sombrillas) para disfrutar de la tranquila playa.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe width="1255" height="706" src="https://www.youtube.com/embed/fQE5lAjr23Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
  -
    type: paragraph
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1627079722
---
