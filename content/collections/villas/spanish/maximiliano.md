---
id: 0fd6fe48-6251-426e-b797-f4aa3bdf97ab
origin: a7815b8c-3298-410e-87fe-0fa95351e1de
short_description: 'Esta espaciosa y lujosa suite tiene dos dormitorios diseñados individualmente: una cama tamaño king está abierta al aire fresco del océano, mientras que la otra habitación cuenta con una cama tamaño queen en un dormitorio contiguo independiente con aire acondicionado. La sala de estar, el comedor y la cocina se encuentran en una gran sala al aire libre decorada con antigüedades y obras de arte originales. El gran baño de azulejos verdes cuenta con una ducha / bañera hundida y tiene vista a las playas hacia la parte sur de la Bahía de Banderas.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Esta espaciosa y lujosa suite tiene dos dormitorios diseñados individualmente: una cama tamaño king está abierta al aire fresco del océano, mientras que la otra habitación cuenta con una cama tamaño queen en un dormitorio contiguo independiente con aire acondicionado.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'La sala de estar, el comedor y la cocina se encuentran en una gran sala al aire libre decorada con antigüedades y obras de arte originales. El gran baño de azulejos verdes cuenta con una ducha / bañera hundida y tiene vista a las playas hacia la parte sur de la Bahía de Banderas.'
seo:
  title: 'Maximiliano (ES)'
updated_by: 45933f4e-71ad-44ce-80e3-45bd622dae4f
updated_at: 1642876894
---
