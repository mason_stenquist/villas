---
id: d5dc6eaa-a27b-4515-9779-12896f926aa7
title: 'Vida Alta'
villa_type: ctv
short_description: 'This spectacular beachfront villa offers some of the most amazing views in Puerto Vallarta, including sweeping views of the bay and vivid sunsets. This beautiful 5,000 sq. foot (464 sq. meter) penthouse villa has 3 levels with 3 spacious bedrooms, 5 baths, plus a rooftop terrace with a private pool and bar. The open-air main living room/dining room area includes a heated Jacuzzi framed by hand-carved cantera columns and arches. Other amenities include air-conditioned bedrooms, daily maid service and 24-hour security.'
amenities:
  - full-kitchen
  - heated-jacuzzi
  - living-room
  - phone
  - private-beach-access
  - wifi-internet
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'This spectacular beachfront villa offers some of the most amazing views in Puerto Vallarta, including sweeping views of the bay and vivid sunsets. This beautiful 5,000 sq. foot (464 sq. meter) penthouse villa has 3 levels with 3 spacious bedrooms, 5 baths, plus a rooftop terrace with a private pool and bar. The open-air main living room/dining room area includes a heated Jacuzzi framed by hand-carved cantera columns and arches. Other amenities include air-conditioned bedrooms, daily maid service and 24-hour security.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vida Alta can be combined with Vida Sol to offer a total of 6 bedrooms or with Vida Sol and Vida Mar for a total of 10 bedrooms accommodating up to 24 guests.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe width="1255" height="706" src="https://www.youtube.com/embed/zT2vn_4qRas" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
villa_photos:
  - villas/ctv/alta/IMG-9143-ABR20nov14-h3.jpg
  - villas/IMG_8980.jpg
  - villas/IMG_9014.jpg
  - villas/ctv/alta/2H5A2124.jpg
  - villas/2H5A2863_9_Int.jpg
  - villas/ctv/alta/2H5A2131.jpg
  - villas/ctv/alta/2H5A2135.jpg
  - villas/ctv/alta/2H5A2147.jpg
  - villas/ctv/alta/IMG-8999-ABR20nov14-h3.jpg
  - villas/ctv/alta/IMG-9002-ABR20nov14-h3.jpg
  - villas/ctv/alta/IMG-9028-ABR20nov14-h3.jpg
number_of_beds: 3
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1732301830
number_of_rooms: 3
number_of_guests: 6
number_of_baths: 5
rooms:
  - b780e193-7262-49bd-a9bc-786c0be62534
  - 73237ba5-92d4-4802-9db1-7fce7d7811fb
  - d2a73c93-7f01-4b79-a9dd-4821d2e29d11
---
