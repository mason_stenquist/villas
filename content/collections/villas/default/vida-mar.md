---
id: 84efceba-4d71-4b3d-a0dd-9a50439ff953
title: 'Vida Mar'
villa_type: ctv
number_of_beds: 5
short_description: 'This beautiful, 5,000 sq. ft. (464 sq. meter) beachfront 4 Bedroom villa is positioned on a quiet beach accented by swaying palms, warm sand and gentle surf. This open-air 3-level villa fuses modern comfort with colonial Mexican charm. Add a refreshing private pool, Jacuzzi, and you have found the perfect private beachfront hideaway.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'This beautiful, 5,000 sq. ft. (464 sq. meter) beachfront 4 Bedroom villa is positioned on a quiet beach accented by swaying palms, warm sand and gentle surf. This open-air 3-level villa fuses modern comfort with colonial Mexican charm. Add a refreshing private pool, Jacuzzi, and you have found the perfect private beachfront hideaway.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vida Mar can be combined with Vida Sol for a 7-bedroom villa or with Vida Sol and Vida Alta to accommodate up to 24 guests making it the perfect location for family reunions, retreats, birthday celebrations or weddings.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe width="1255" height="706" src="https://www.youtube.com/embed/BXxupJ2XvME" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1732301746
villa_photos:
  - villas/ctv/mar/IMG-4298-CR07octt3f.jpg
  - villas/ctv/mar/IMG_6605.jpg
  - villas/ctv/mar/2H5A6676.jpg
  - villas/ctv/mar/IMG-4330-CR07octt3f.jpg
  - villas/ctv/mar/2H5A2165.jpg
  - villas/ctv/mar/IMG-4318-CR07octt4f.jpg
  - villas/ctv/mar/IMG_6637.jpg
  - villas/ctv/mar/IMG-4338-CR07octt3f.jpg
  - villas/ctv/mar/2H5A2183.jpg
  - villas/ctv/mar/IMG-8953-ABR24sep15-e3-ctvpano4.jpg
  - villas/ctv/mar/IMG_8917.jpg
  - villas/ctv/mar/2H5A6657.jpg
  - villas/ctv/mar/IMG_8941.jpg
  - villas/ctv/mar/IMG_8948.jpg
  - villas/ctv/mar/IMG_8980.jpg
number_of_rooms: 4
number_of_guests: 8
number_of_baths: '4'
rooms:
  - bb827caf-dd2a-4c6c-939b-c87a77ab271e
  - 49c4c59b-8bfd-4827-bada-6cae418b75f1
  - a5d1b547-0236-46ca-aed2-899b01479661
  - 71380529-85d9-4b6c-966d-2684d1444b82
amenities:
  - full-kitchen
  - heated-jacuzzi
  - living-room
  - pool
  - private-beach-access
  - sun-terrace
  - wifi-internet
---
