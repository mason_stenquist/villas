---
id: a7815b8c-3298-410e-87fe-0fa95351e1de
title: Maximiliano
villa_type: qmc
number_of_beds: 2
short_description: 'This spacious and luxurious suite has two individually designed bedrooms: one king-size bed is open to the fresh ocean air while the other room features a queen-size bed in a self-contained and air-conditioned adjoining bedroom. The sitting area, dining room and kitchen are all in one large open air room decorated with antiques and original artwork. The large green tiled bathroom features a sunken shower/tub and overlooks the beaches toward the southern portion of the Bay of Banderas.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'This spacious and luxurious suite has two individually designed bedrooms: one king-size bed is open to the fresh ocean air while the other room features a queen-size bed in a self-contained and air-conditioned adjoining bedroom.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The sitting area, dining room and kitchen are all in one large open air room decorated with antiques and original artwork. The large green tiled bathroom features a sunken shower/tub and overlooks the beaches toward the southern portion of the Bay of Banderas.'
villa_photos:
  - villas/2H5A8503_5_N.jpg
  - villas/2H5A8496.jpg
  - villas/qmc/maxi/IMG-6056-CR18t-P160x200.jpg
  - villas/2H5A8509.jpg
  - villas/qmc/maxi/QMC-Max--4377a.jpg
  - villas/2H5A8499.jpg
  - villas/qmc/maxi/Maxi-bedroom-2.jpg
  - villas/2H5A8478.jpg
  - villas/2H5A8488.jpg
  - villas/qmc/maxi/IMG-6072-CR18t.jpg
  - villas/2H5A8483.jpg
  - villas/2H5A8476.jpg
  - villas/qmc/maxi/QMC-Max-4118a.jpg
  - villas/rooftop-oceanview-Medium-1.jpg
amenities:
  - 2-balconies
  - 2-bedrooms
  - kitchenette
  - living-room
  - phone
  - room-safe
  - sunken-shower-tub
  - wifi-internet
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1623711123
number_of_rooms: 2
number_of_guests: 4
number_of_baths: 1
---
