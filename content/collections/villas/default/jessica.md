---
id: 94d994ae-10c7-43d3-bd01-1b4ac737682b
title: Jessica
villa_type: qmc
number_of_beds: 1
short_description: 'Jessica has an entrance just steps away from the main entrance of QMC. A queen size bed sits in a loft above the kitchenette, bathroom and sitting area. The sunken tub/shower is adjacent to the living area on the main level. Jessica is the smallest and most affordable room with a peek-a-boo view of the ocean from the day bed.'
amenities:
  - air-conditioning
  - coffee-maker
  - phone
  - room-safe
  - wifi-internet
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1689872728
villa_photos:
  - villas/2H5A2581.jpg
  - villas/2H5A2595.jpg
  - villas/qmc/jessica/QMC-Ent-4164a.jpg
  - villas/2H5A2593.jpg
  - villas/2H5A2599-ATR02dec22-e2-ctvpano2.jpg
  - villas/2H5A2606_8_Int2.jpg
  - villas/2H5A2619.jpg
  - villas/2H5A2626.jpg
  - villas/2H5A2656.jpg
  - villas/2H5A2630.jpg
  - villas/2H5A2634.jpg
  - villas/2H5A2642.jpg
  - villas/2H5A2662.jpg
  - villas/qmc/jessica/PICT4155a.jpg
  - villas/qmc/jessica/QMC-Jes-4151a.jpg
  - villas/2H5A2590.jpg
  - villas/2H5A2611_Bal.jpg
number_of_rooms: 1
number_of_guests: 2
number_of_baths: 1
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Jessica has an entrance just steps away from the main entrance of QMC. A queen size bed sits in a loft above the kitchenette, bathroom and sitting area. The sunken tub/shower is adjacent to the living area on the main level. Jessica is the smallest and most affordable room with a peek-a-boo view of the ocean from the day bed.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Please note - We have guests that book Jessica for the affordable price and enjoy the room. They take advantage and spend time in the QMC common areas like the Sala, Palapa, Roof Top Terrace, Pool and Beach. Others just book the lowest price room and are disappointed that it doesn''t have the view of the other rooms. '
---
