---
id: 3605061a-f60e-45d6-9e9c-f555df3d9600
title: Alejandro
villa_type: qmc
number_of_beds: 2
short_description: 'Alejandro has two distinct areas. The large bedroom with a king-size canopied bed and a sitting area open to the dramatic views of the ocean. The adjoining room includes a daybed along with full kitchen and eating area. The private terrace is a wonderful area to enjoy your morning coffee. Guests rave about the enormous bathroom where you can shower and watch the ocean waves below. Please note there is AC in the bedroom  area only. The rest of the suite is open to the sounds and breezes of the ocean.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1702600223
amenities:
  - king-size-bed
  - coffee-maker
  - day-bed
  - kitchenette
  - living-room
  - room-safe
  - tub-shower
  - wifi-internet
  - air-conditioning
villa_photos:
  - villas/2H5A8953_6_Int2.jpg
  - villas/2H5A8994.jpg
  - villas/2H5A8957_61_Int2.jpg
  - villas/2H5A8974.jpg
  - villas/2H5A8874_76_77_80_Int2.jpg
  - villas/2H5A8885_6_8_Int2.jpg
  - villas/2H5A8890_5_Int2.jpg
  - villas/2H5A8902_5_7_Int2.jpg
  - villas/2H5A9003.jpg
  - villas/2H5A8977_8_Int2.jpg
  - villas/2H5A8913.jpg
  - villas/2H5A8934.jpg
  - villas/2H5A8938.jpg
  - villas/2H5A8983.jpg
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Alejandro has two distinct areas. The large bedroom with a king-size canopied bed and a sitting area open to the dramatic views of the ocean. The adjoining room includes a daybed along with full kitchen and eating area. The private terrace is a wonderful area to enjoy your morning coffee. Guests rave about the enormous bathroom where you can shower and watch the ocean waves below. Please note there is AC in the bedroom  area only. The rest of the suite is open to the sounds of the ocean.'
number_of_rooms: 1
number_of_guests: 3
nuumber_of_baths: 1
number_of_baths: 1
---
