---
id: fdec8603-40e1-429a-b7e1-50d8cb15afa4
title: 'Casa Tres Vidas'
villa_type: ctv
number_of_beds: 13
short_description: 'This beautiful beachfront villa is situated on Playa Conchas Chinas, a quiet beach accented by swaying palm trees and offering some of the best views in Puerto Vallarta.  This open air 8 level villa with 10 bedrooms features a 18-ft high architectural dome design that bestows the charm of traditional colonial Mexico and brings the spectacular view right into the villa. The Casa Tres Vidas features 3 pools and 3 heated Jacuzzi along with 3 spacious living areas.  It offers the opportunity to enjoy a group dinner or function and also have privacy or a quiet moment. The villa includes breathtaking ocean views, Wi-Fi, and daily housekeeping.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Imagine awakening in your private luxurious and finely appointed Mexican villa to the calming sounds of the ocean just outside your window.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'After enjoying a leisurely cooked-to-order breakfast, the only decision you need to make for the day is whether to go shopping, lay by your private pool, take a walk along the beach, or enjoy one of the many adventurous tours available to you. This is your vacation — spend the days as you desire. Casa Tres Vidas allows travelers to experience Mexico as they wish while enjoying privacy in peaceful and plush surroundings.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Casa Tres Vidas, featured in Architectural Digest, offers the flexibility for families and groups of multiple sizes in a luxurious setting making for a unique choice for your vacation getaway.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Don’t just take our word for it — our many guest raves say it all;'
  -
    type: blockquote
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: '“This was an unbelievably terrific and unmatched vacation experience…” Vancouver BC, Canada'
  -
    type: blockquote
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: '“From the moment we walked in the door, we were treated like royalty.” Cedar Rapids, IA, USA'
  -
    type: blockquote
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: '“I was overwhelmed at how wonderful our stay was. From the beautiful villa with breathtaking views of the ocean to the gourmet meals… and roof top deck… Casa Tres Vidas is the place to stay with friends or family.” Seattle, WA, USA'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Villa Information:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Beachfront on Playa Conchas Chinas'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '10 air conditioned bedrooms with 11 king & 1 queen beds and private baths - sleeps 24'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '2 Bedrooms have 2 king beds each'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '3 Living rooms'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '3 Dining rooms'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '3 Kitchens'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Staff includes housekeeper'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '3 Pools & 3 Heated Jacuzzis (Beach level & upper level)'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Private Beach access – Beach furniture'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Cable TV / DVD / Sound System'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Access to computer/Internet and access to DSL line'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Casa Tres Vidas is the perfect location to host your next special celebration.  From weddings, family reunions or special birthdays, our unique architecture and beachfront setting provides you with the perfect backdrop for your memorable day.  In our enclave of 3 villas, Casa Tres Vidas can offer overnight accommodations for up to 24 people.  Combined with our sister property next door, the beachfront boutique hotel Quinta Maria Cortez, we can host an additional 18 guests making Casa Tres Vidas and Quinta Maria Cortez the perfect location to accommodate (up to 42 Guests) and host your special celebration from 20-80 people.  If your party needs overnight accommodations for  more than 42 people, we can offer advice and help accommodate them in nearby hotels and villas that we have relationships with.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe width="1255" height="706" src="https://www.youtube.com/embed/Gq5y3_yaaFg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
  -
    type: paragraph
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1732301199
villa_photos:
  - villas/ctv/alta/134.jpg
  - villas/ctv/sol/2H5A6691.jpg
  - villas/ctv/sol/2H5A6703.jpg
  - villas/ctv/sol/2H5A6548.jpg
  - villas/ctv/sol/2H5A6566.jpg
  - villas/ctv/sol/134/2H5A6569.jpg
  - villas/ctv/sol/135/IMG_3220.jpg
  - villas/ctv/sol/136/IMG_3280.jpg
  - villas/ctv/alta/2H5A2124.jpg
  - villas/ctv/alta/2H5A2131.jpg
  - villas/ctv/alta/2H5A2135.jpg
  - villas/ctv/alta/IMG-9143-ABR20nov14-h3.jpg
  - villas/ctv/alta/IMG-9002-ABR20nov14-h3.jpg
  - villas/ctv/alta/2H5A2045.jpg
  - villas/ctv/alta/IMG-9028-ABR20nov14-h3.jpg
  - villas/ctv/alta/121/IMG-9090-ABR20nov14-h3.jpg
  - villas/ctv/alta/122/IMG-9059-ABR20nov14-h3.jpg
  - villas/ctv/alta/123/IMG-6835-ABR21novt-h13.jpg
  - villas/ctv/mar/IMG-4298-CR07octt3f.jpg
  - villas/ctv/mar/2H5A6676.jpg
  - villas/ctv/mar/IMG-4330-CR07octt3f.jpg
  - villas/ctv/mar/IMG-8953-ABR24sep15-e3-ctvpano4.jpg
  - villas/ctv/mar/IMG_6555.jpg
  - villas/ctv/mar/IMG_8917.jpg
  - villas/ctv/mar/IMG_8948.jpg
  - villas/ctv/mar/IMG_8941.jpg
amenities:
  - air-conditioning
  - beach-access
  - full-kitchen
  - heated-jacuzzi
  - living-room
  - roof-top-terrace
  - sun-terrace
  - room-safe
  - wifi-internet
number_of_rooms: 10
number_of_guests: 24
number_of_baths: 11
rooms:
  - b780e193-7262-49bd-a9bc-786c0be62534
  - 73237ba5-92d4-4802-9db1-7fce7d7811fb
  - d2a73c93-7f01-4b79-a9dd-4821d2e29d11
  - bb827caf-dd2a-4c6c-939b-c87a77ab271e
  - 49c4c59b-8bfd-4827-bada-6cae418b75f1
  - a5d1b547-0236-46ca-aed2-899b01479661
  - 71380529-85d9-4b6c-966d-2684d1444b82
  - 2e329d58-8fc7-4ee7-ac50-f17ef9ce6920
  - 02ec060c-2736-4d45-8e1f-53551abce77d
  - df7188bf-7119-48d0-97b9-265745a128ca
---
