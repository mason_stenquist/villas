---
id: f6047bc0-c8c2-45a4-8098-08d9f22bfe7a
title: 'Vida Sol & Mar at CTV'
villa_type: ctv
number_of_rooms: 7
number_of_beds: 9
number_of_guests: 18
number_of_baths: 7
short_description: |-
  7 Bedroom Beach Villa on Playa Conchas Chinas the combination of Vida Mar & Sol cascades up from the Beach over 4 levels with 2 private Pools and 2 heated Jacuzzis with Wi-Fi thru out.

  Vida Mar & Sol beachfront villa is positioned on a quiet beach accented by swaying palms, warm sand and gentle surf.  This open-air 4-level villa fuses modern comfort with colonial Mexican charm.
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vida Sol with its towering 18-ft. - high domed spacious open-air living room & dining room.  Vida Mar & Sol reflects the charm of traditional colonial Mexico and blends the luxurious surroundings of the 4-level combined villa with the spectacular view of the Pacific. Watch playful dolphins and whales from your private terrace while enjoying your morning coffee or that perfect sunset cocktail.  '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vida Mar & Sol @ Casa Tres Vidas highlights:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Beachfront on Playa Conchas Chinas, a 10-minute beachfront walk to downtown Puerto Vallarta'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '7 air-conditioned bedrooms, 7.5 bathrooms '
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '(8 king-sized beds,  1 queen bed & 1 Single bed)'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '2 Living rooms'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Dining rooms & Terrace Dining'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '2 Kitchens'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Terrace on the Beach and Beachfront terrace of Vida Sol'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '2 Private Pools'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '2 Heated Jacuzzi'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Sun terrace'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Free Wi-Fi'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'TV''s in Common area and 4 out of 7 Rooms'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Direct-dial phone'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Private beach access '
  -
    type: paragraph
    content:
      -
        type: text
        text: '   *  Beach furniture (Chairs, Loungers, Tables & Umbrellas) to enjoy the quiet beach.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe width="1255" height="706" src="https://www.youtube.com/embed/fQE5lAjr23Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
  -
    type: paragraph
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1732301307
amenities:
  - air-conditioning
  - beach-access
  - entertainment
  - full-kitchen
  - private-terrace
  - room-safe
  - sun-terrace
  - wifi-internet
villa_photos:
  - villas/IMG_9009.jpg
  - villas/ctv/sol/2H5A6556.jpg
  - villas/ctv/sol/2H5A6546.jpg
  - villas/ctv/sol/2H5A2149.jpg
  - villas/ctv/sol/2H5A2194.jpg
  - villas/ctv/sol/2H5A6566.jpg
  - villas/ctv/sol/2H5A6687.jpg
  - villas/ctv/sol/2H5A6691.jpg
  - villas/ctv/sol/2H5A6703.jpg
  - villas/ctv/mar/IMG-4298-CR07octt3f.jpg
  - villas/ctv/mar/IMG-4318-CR07octt4f.jpg
  - villas/ctv/mar/2H5A6676.jpg
  - villas/ctv/mar/2H5A6657.jpg
  - villas/ctv/mar/IMG-4330-CR07octt3f.jpg
  - villas/ctv/mar/2H5A2183.jpg
  - villas/ctv/mar/2H5A2165.jpg
  - villas/ctv/mar/IMG-8953-ABR24sep15-e3-ctvpano4.jpg
  - villas/ctv/mar/IMG_6555.jpg
  - villas/ctv/mar/IMG_8917.jpg
  - villas/IMG_9014.jpg
rooms:
  - 2e329d58-8fc7-4ee7-ac50-f17ef9ce6920
  - 02ec060c-2736-4d45-8e1f-53551abce77d
  - df7188bf-7119-48d0-97b9-265745a128ca
  - bb827caf-dd2a-4c6c-939b-c87a77ab271e
  - 49c4c59b-8bfd-4827-bada-6cae418b75f1
  - a5d1b547-0236-46ca-aed2-899b01479661
  - 71380529-85d9-4b6c-966d-2684d1444b82
---
