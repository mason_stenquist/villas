---
title: Maria
villa_type: qmc
number_of_beds: 1
short_description: 'Originally the owner’s personal 1,200 sq. ft. apartment, the architecture of Maria is dramatic. The living room and full kitchen of this suite are completely open to the view of the ocean and nightly sunsets. Maria’s private king-size canopied bed with sitting area is adjacent to sunken shower/tub and separate water closet with bidet. Beautiful antique armoires serve as your closets.'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Originally the owner’s personal 1,200 sq. ft. apartment, the architecture of Maria is dramatic. The living room and full kitchen of this suite are completely open to the view of the ocean and nightly sunsets. Maria’s private king-size canopied bed with sitting area is adjacent to sunken shower/tub and separate water closet with bidet. Beautiful antique armoires serve as your closets.'
amenities:
  - coffee-maker
  - beach-access
  - air-conditioning
  - full-kitchen
  - king-size-bed
  - living-room
  - phone
  - room-safe
  - wifi-internet
villa_photos:
  - villas/qmc/maria/IMG-6010-CR18t.jpg
  - villas/qmc/maria/IMG-6016-CR18t-P160x200.jpg
  - villas/qmc/maria/IMG-6026-CR18t.jpg
  - villas/qmc/maria/IMG_2549.jpg
  - villas/qmc/maria/IMG-6038-CR18t.jpg
  - villas/qmc/maria/IMG_2566.jpg
  - villas/qmc/maria/IMG-4480-CR07octt3f.jpg
  - villas/qmc/maria/IMG-6042-CR18t.jpg
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1613503834
number_of_rooms: 1
number_of_guests: 2
number_of_baths: 1
id: e3f1f850-6bd3-4788-9c53-0b482ac56461
---
