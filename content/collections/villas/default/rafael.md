---
id: 53385dc5-20d4-422d-b7aa-dcb0a92ec7e0
title: Rafael
villa_type: qmc
number_of_beds: 2
short_description: 'Rafael, our split-level suite, features a queen-size bed on the entrance level as well as on the lower level of the suite. Mid-level is a striking black-&-white tile bathroom with sunken shower/tub. Just outside the entrance of Rafael is a private terrace that overlooks the pool and Pacific Ocean. Comfortable lounge chairs on the terrace are a great place to finish your favorite novel or to continue your favorite dream.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1676475160
amenities:
  - 2-queen-beds-on-2-levels
  - adjoining-patio-terrace
  - air-conditioning
  - coffee-maker
  - kitchenette
  - room-safe
  - sunken-shower-tub
  - wifi-internet
villa_photos:
  - villas/2H5A2872_6_Int2.jpg
  - villas/2H5A2881.jpg
  - villas/2H5A2893.jpg
  - villas/2H5A2896.jpg
  - villas/2H5A2902.jpg
  - villas/qmc/rafael/QMC-Raf-Kit-Bath-4441a.jpg
  - villas/qmc/rafael/QMC-Raf-Tub-4457a.jpg
  - villas/2H5A2935.jpg
  - villas/qmc/rafael/QMC-RAf-Ter-4458a.jpg
  - villas/qmc/rafael/Raf-Terrace-View.jpg
  - villas/qmc/rafael/IMG_1470.jpg
  - villas/2H5A2920.jpg
  - villas/2H5A2925.jpg
  - villas/2H5A2934.jpg
  - villas/qmc/rafael/IMG-8622-CR4t.jpg
  - villas/2H5A2941.jpg
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Rafael, our split-level suite, features a queen-size bed on the entrance level as well as on the lower level of the suite. Mid-level is a striking black-&-white tile bathroom with sunken shower/tub. Just outside the entrance of Rafael is a private terrace that overlooks the pool and Pacific Ocean. Comfortable lounge chairs on the terrace are a great place to finish your favorite novel or to continue your favorite dream.'
number_of_rooms: 2
number_of_guests: 4
number_of_baths: 1
---
