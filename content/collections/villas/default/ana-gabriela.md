---
title: 'Ana Gabriela'
villa_type: qmc
number_of_beds: 1
short_description: 'Deep red walls and a king-size canopied bed make “Gabby” a perfect romantic escape by the sea. Antique black tiles highlight the sunken tub/shower and adjoining bathroom with bidet. The sitting area on the spacious balcony is an ideal place to relax and watch the surf. Gabby features a mini-refrigerator and coffee station.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1613503719
amenities:
  - 2-balconies
  - air-conditioning
  - coffee-maker
  - room-safe
  - tub-shower
  - wifi-internet
villa_photos:
  - villas/qmc/ana_gab/IMG-8570-CR4t-P160x200.jpg
  - villas/qmc/ana_gab/IMG-8582-CR4t.jpg
  - villas/qmc/ana_gab/IMG-8590-CR4t.jpg
  - villas/qmc/ana_gab/IMG_2578.jpg
  - villas/qmc/ana_gab/IMG_2583.jpg
  - villas/qmc/ana_gab/IMG-8597-CR4t.jpg
  - villas/qmc/ana_gab/QMC-Ana-Bath-4400a.jpg
  - villas/qmc/ana_gab/QMC-Ana-4135a.jpg
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Deep red walls and a king-size canopied bed make “Gabby” a perfect romantic escape by the sea. Antique black tiles highlight the sunken tub/shower and adjoining bathroom with bidet. The sitting area on the spacious balcony is an ideal place to relax and watch the surf. Gabby features a mini-refrigerator and coffee station.'
number_of_rooms: 1
number_of_guests: 2
number_of_baths: 1
id: a0a96206-3a95-4978-8f33-6cb6249ad37c
---
