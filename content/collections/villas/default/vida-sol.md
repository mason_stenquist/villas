---
id: 5da81190-520f-4c7a-9d72-afb434567e1b
title: 'Vida Sol'
villa_type: ctv
short_description: 'With its towering 18-ft. – high domed spacious open-air living room, Vida Sol reflects the charm of traditional colonial Mexico and blends the luxurious surroundings of the 2-level, 3 Bedroom villa with the spectacular view of the Pacific. Watch playful dolphins and whales from your private terrace while enjoying your morning coffee or that perfect sunset cocktail. Vida Sol offers a dramatic private pool with heated Jacuzzi in a lush tropical-garden setting.'
amenities:
  - air-conditioning
  - entertainment
  - heated-jacuzzi
  - kitchenette
  - phone
  - pool
  - private-beach-access
  - sun-terrace
villa_photos:
  - villas/ctv/sol/2H5A6556.jpg
  - villas/ctv/sol/2H5A6546.jpg
  - villas/ctv/sol/2H5A6548.jpg
  - villas/ctv/sol/IMG_3434.jpg
  - villas/ctv/sol/2H5A6687.jpg
  - villas/ctv/sol/2H5A6693.jpg
  - villas/ctv/sol/2H5A6691.jpg
  - villas/ctv/sol/2H5A6703.jpg
  - villas/ctv/sol/2H5A2149.jpg
  - villas/ctv/sol/2H5A2152.jpg
  - villas/ctv/sol/2H5A2194.jpg
  - villas/ctv/sol/2H5A6566.jpg
  - villas/ctv/sol/2H5A2199.jpg
  - villas/ctv/sol/2H5A6711.jpg
  - villas/ctv/sol/2H5A6707.jpg
number_of_beds: 5
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'With its towering 18-ft. – high domed spacious open-air living room, Vida Sol reflects the charm of traditional colonial Mexico and blends the luxurious surroundings of the 2-level, 3 Bedroom villa with the spectacular view of the Pacific. Watch playful dolphins and whales from your private terrace while enjoying your morning coffee or that perfect sunset cocktail. Vida Sol offers a dramatic private pool with heated Jacuzzi in a lush tropical-garden setting.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vida Sol can be combined with Vida Alta for 6 bedrooms or Vida Mar for 7 bedrooms or both Vida Mar and Vida Alta for 10 bedrooms, accommodating up to 24 guests.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe width="1255" height="706" src="https://www.youtube.com/embed/PMmiuoyW5n4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1732301478
number_of_rooms: 3
number_of_guests: 10
number_of_baths: 3
rooms:
  - 2e329d58-8fc7-4ee7-ac50-f17ef9ce6920
  - 02ec060c-2736-4d45-8e1f-53551abce77d
  - df7188bf-7119-48d0-97b9-265745a128ca
---
