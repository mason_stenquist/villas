---
title: Guadalupe
villa_type: qmc
number_of_beds: 2
short_description: 'This gracious suite is located in our most privileged beachfront location. This split-level suite with king-size bed, single daybed, sitting area, galley bathroom and kitchenette is one large open suite with the areas defined by multiple levels. The Guadalupe Suite is ideal for those who want to spend a great deal of time at the beach, located just outside your suite door and for those who enjoy being directly on the water with all of the sights and sounds of the surf just outside their room.'
amenities:
  - adjoining-patio-terrace
  - air-conditioning
  - beach-access
  - coffee-maker
  - day-bed
  - kitchenette
  - king-size-bed
  - room-safe
  - tub-shower
  - wifi-internet
villa_photos:
  - villas/qmc/guadalupe/IMG_6464.jpg
  - villas/qmc/guadalupe/IMG_6523-ABR11nov16-e2-qmpano5.jpg
  - villas/qmc/guadalupe/IMG_6451.jpg
  - villas/qmc/guadalupe/IMG_6475.jpg
  - villas/qmc/guadalupe/IMG_6485.jpg
  - villas/qmc/guadalupe/IMG_6493.jpg
  - villas/qmc/guadalupe/IMG_6495.jpg
  - villas/qmc/guadalupe/IMG_6511.jpg
  - villas/qmc/guadalupe/IMG_6519.jpg
  - villas/qmc/guadalupe/IMG_6530.jpg
  - villas/qmc/guadalupe/QMC-Guad-4176a.jpg
  - villas/qmc/guadalupe/Guad-terrace.jpg
  - villas/qmc/guadalupe/QMC-B-Ter-W-4217a.jpg
  - villas/qmc/guadalupe/IMG-6774-ABR21novt-h5.jpg
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'This gracious suite is located in our most privileged beachfront location. This split-level suite with king-size bed, single daybed, sitting area, galley bathroom and kitchenette is one large open suite with the areas defined by multiple levels. The Guadalupe Suite is ideal for those who want to spend a great deal of time at the beach, located just outside your suite door and for those who enjoy being directly on the water with all of the sights and sounds of the surf just outside their room.'
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1613503806
number_of_rooms: 1
number_of_guests: 3
number_of_baths: 1
id: e425ab4b-3c93-426a-9a6e-7b9f43e332d6
---
