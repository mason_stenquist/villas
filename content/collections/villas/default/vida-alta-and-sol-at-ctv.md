---
id: 11332874-67c1-4ff5-8e3f-a2b516d87234
title: 'Vida Alta & Sol at CTV'
villa_type: ctv
number_of_rooms: 6
number_of_beds: 8
number_of_guests: 16
number_of_baths: 7
short_description: 'This spectacular beachfront villa offers some of the most amazing views in Puerto Vallarta, including sweeping views of the bay and vivid sunsets. Vida Alta & Vida Sol combined over 5 levels offers 6 bedrooms, (8 king beds) with 6 private bathrooms & 3 half baths along with 2 Private pools & 2 heated Jacuzzi'
bard:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vida Alta the penthouse villa has 3 levels with 3 spacious bedrooms, 5 baths, plus a rooftop terrace with a private pool and bar. The open-air main living room/dining room area includes a heated Jacuzzi framed by hand-carved cantera columns and arches. Other amenities include air-conditioned bedrooms, daily maid service and 24-hour security.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vida Sol with its towering 18-ft. - high domed spacious open-air living room, Vida Sol reflects the charm of traditional colonial Mexico and blends the luxurious surroundings of the 2-level villa with the spectacular view of the Pacific. Watch playful dolphins and whales from your private terrace while enjoying your morning coffee or that perfect sunset cocktail. Vida Sol offers a dramatic private pool with heated Jacuzzi in a lush tropical-garden setting. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Maid Service included.'
  -
    type: set
    attrs:
      values:
        type: embed_html
        html: '<iframe width="1255" height="706" src="https://www.youtube.com/embed/7ek90lv0iao" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
amenities:
  - air-conditioning
  - beach-access
  - entertainment
  - full-kitchen
  - heated-jacuzzi
  - living-room
  - roof-top-terrace
  - sun-terrace
  - room-safe
  - wifi-internet
updated_by: f8e3fb80-7c03-47e0-94d2-c23ba5734dbd
updated_at: 1732301407
villa_photos:
  - villas/ctv/alta/134.jpg
  - villas/ctv/alta/2H5A2045.jpg
  - villas/ctv/alta/2H5A2124.jpg
  - villas/ctv/alta/2H5A2131.jpg
  - villas/ctv/alta/2H5A2135.jpg
  - villas/ctv/alta/2H5A2147.jpg
  - villas/ctv/alta/IMG-8999-ABR20nov14-h3.jpg
  - villas/ctv/alta/IMG-9002-ABR20nov14-h3.jpg
  - villas/ctv/alta/IMG-9028-ABR20nov14-h3.jpg
  - villas/ctv/sol/2H5A2149.jpg
  - villas/ctv/sol/2H5A2194.jpg
  - villas/ctv/sol/2H5A6546.jpg
  - villas/ctv/sol/2H5A6556.jpg
  - villas/ctv/sol/2H5A6566.jpg
  - villas/ctv/sol/2H5A6687.jpg
  - villas/ctv/sol/2H5A6691.jpg
  - villas/ctv/sol/2H5A6703.jpg
rooms:
  - b780e193-7262-49bd-a9bc-786c0be62534
  - 73237ba5-92d4-4802-9db1-7fce7d7811fb
  - d2a73c93-7f01-4b79-a9dd-4821d2e29d11
  - 2e329d58-8fc7-4ee7-ac50-f17ef9ce6920
  - 02ec060c-2736-4d45-8e1f-53551abce77d
  - df7188bf-7119-48d0-97b9-265745a128ca
---
