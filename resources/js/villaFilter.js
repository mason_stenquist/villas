function villaFilter() {
  return {
    amenities: [],
    villas:[],
    filter_type: '',
    filter_people: 0,
    get filteredAmenities(){
      var filteredArray =  this.amenities.filter(item => item.filterBy)
      return filteredArray.map(item => item.slug)
    },
    init(amenities, villas) {

      //Set filter_type by url
      if(window.location.pathname === "/villas/ctv"){
        this.filter_type = "ctv"
      }
      if(window.location.pathname === "/villas/qmc"){
        this.filter_type = "qmc"
      }
      
      //Format amenities
      var amenitiesArray = JSON.parse(decodeURIComponent(amenities));
      amenitiesArray = amenitiesArray.map(item => {
        let title = item.title.replaceAll("+", " ");
        return{
          slug: item.slug, 
          title,
          filterBy: false
        }
      })
      this.amenities = amenitiesArray

      //Format villas
      var villasArray = JSON.parse(decodeURIComponent(villas));
      villasArray = villasArray.map(item => {
        let amenities = item.amenities.map(amenity => amenity.slug)
        return{
          slug: item.slug, 
          amenities,
          villa_type: item.villa_type.value,
          number_of_guests: item.number_of_guests
        }
      })
      this.villas = villasArray
      this.filter()

    },
    filter(){
      //Loop through villas
      this.villas.forEach(villa=>{
        var is_visible = false

        //Determine if any of the villas amenties are in the filtered amenities array
        if( this.filteredAmenities.length == 0){
          is_visible = true
        }else{        
          villa.amenities.forEach(amenity=>{   
            let index = this.filteredAmenities.indexOf(amenity)
            if(index >= 0){            
              is_visible = true
            }
          })
        }

        //Check villa type
        if(this.filter_type && this.filter_type !== villa.villa_type){
          is_visible = false
        }

        //Check number of guests
        if(villa.number_of_guests < this.filter_people){
          is_visible = false
        }

        //show or hide the villa
        var villaEl = document.getElementById(villa.slug);
        if(is_visible){
          villaEl.classList.remove("hidden")
        }else{
          villaEl.classList.add("hidden")
        }

      })
    }
  }
}
