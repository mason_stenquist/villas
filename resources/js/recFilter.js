function recommendationFilter() {
  return {
    filterTypes:[],
    recommendations:[],
    init(recommendations) {      

      //Format recommendations
      var recommendationsArray = JSON.parse(decodeURIComponent(recommendations));
      recommendationsArray = recommendationsArray.map(item => {
        let types = item.recommendation_types.map(type => type.slug)
        return{
          slug: item.slug, 
          types,
          number_of_beds: item.number_of_beds
        }
      })
      this.recommendations = recommendationsArray

    },
    filter(filterValue){
      //Add or remove filtervalue from filterTypes array
      let itemIndex = this.filterTypes.indexOf(filterValue)
      if( itemIndex < 0){
        this.filterTypes.push(filterValue)
      }else{
        this.filterTypes.splice(itemIndex, 1)
      }

      //Loop through recommendations
      this.recommendations.forEach(recommendation=>{
        var is_visible = false

        //Determine if any of the recommendations amenties are in the filtered types array
        if( this.filterTypes.length === 0){
          is_visible = true
        }else{        
          recommendation.types.forEach(type=>{   
            let index = this.filterTypes.indexOf(type)
            if(index >= 0){            
              is_visible = true
            }
          })
        }

        //show or hide the recommendation
        var recommendationEl = document.getElementById(recommendation.slug);
        if(is_visible){
          recommendationEl.classList.remove("hidden")
        }else{
          recommendationEl.classList.add("hidden")
        }

      })
    }
  }
}
