<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'accommodations' => 'Alojamientos',
    'amenities' => 'Comodidades',
    'home' => 'Hogar',
    'location' => 'Ubicación',
    'more_info' => 'Más información',
    'special_offer' => 'Oferta Especiales',
    'contact_us' => 'Contacta Con Nosotros'

];
