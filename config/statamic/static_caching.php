<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Active Static Caching Strategy
    |--------------------------------------------------------------------------
    |
    | To enable Static Caching, you should choose a strategy from the ones
    | you have defined below. Leave this null to disable static caching.
    |
    */

    'strategy' => env('CACHE_STATEGY'),

    /*
    |--------------------------------------------------------------------------
    | Caching Strategies
    |--------------------------------------------------------------------------
    |
    | Here you may define all of the static caching strategies for your
    | application as well as their drivers.
    |
    | Supported drivers: "application", "file"
    |
    */

    'strategies' => [

        'half' => [
            'driver' => 'application',
            'expiry' => null,
        ],

        'full' => [
            'driver' => 'file',
            'path' => public_path('static'),
            'lock_hold_length' => 0,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Exclusions
    |--------------------------------------------------------------------------
    |
    | Here you may define a list of URLs to be excluded from static
    | caching. You may want to exclude URLs containing dynamic
    | elements like contact forms, or shopping carts.
    |
    */

    'exclude' => [
        '/contact-us'
    ],

    /*
    |--------------------------------------------------------------------------
    | Invalidation Rules
    |--------------------------------------------------------------------------
    |
    | Here you may define the rules that trigger when and how content would be
    | flushed from the static cache. See the documentation for more details.
    | If a custom class is not defined, the default invalidator is used.
    |
    | https://docs.statamic.com/static-caching
    |
    */

    'invalidation' => [
        'rules' => [
            'collections' => [
                'pages' => [
                    'urls' => [
                        '/'
                    ]
                ],
                'villas' => [
                    'urls' => [
                        '/villas',
                        '/villas/casa-tres-vidas',
                        '/villas/quinta-maria-cortez'
                    ]
                ],
                'recommendations' => [
                    'urls' => [
                        '/recommendations',
                    ]
                ],
                'faq' => [
                    'urls' => [
                        '/about/f-a-q',
                    ]
                ],
                'posts' => [
                    'urls' => [
                        '/about/blog',
                    ]
                ],
                'staff' => [
                    'urls' => [
                        '/about/staff',
                    ]
                ]
            ],
            'taxonomies' => [
                'amenities' => [
                    'urls' => [
                        '/villas/*'
                    ]
                ],
                'blog_categories' => [
                    'urls' => [
                        '/about/blog',
                        '/about/blog/*'
                    ]
                ],
                'blog_tags' => [
                    'urls' => [
                        '/about/blog',
                        '/about/blog/*'
                    ]
                ],
                'recommendations_type' => [
                    'urls' => [
                        '/recommendations',
                        '/recommendations/*'
                    ]
                ]
            ],
            'globals' => [
                'company' => [
                    'urls' => [
                        '/*'
                    ]
                ],
                'footer' => [
                    'urls' => [
                        '/*'
                    ]
                ],
                'site' => [
                    'urls' => [
                        '/*'
                    ]
                ]
            ],
            'navigation' => [
                'main_nav' => [
                    'urls' => [
                        '/*'
                    ]
                ],
                'wedding_nav' => [
                    'urls' => [
                        '/special-events/*'
                    ]
                ]
            ]
        ],
    ],
];
